
public class WodConverter {

	public static void main(String[] args) {
		// deadliftToMachoMan(142, 30, 60);
		
		deadliftToMachoMan(102, 45, 60);
	}
	
	public static void deadliftToMachoMan(int deadliftWeight, int repsNumber, int machomanWeight) {
		
		double deadliftToPushJerkRatio = ( ((deadliftWeight * (repsNumber / 3)) * 0.8) * 0.72);
		double pushJerkTotalRep = (deadliftToPushJerkRatio / machomanWeight); 
		System.out.println("Push Jerk Total Round for 3 rep: " + (pushJerkTotalRep / 3));
		
		double deadliftToPowerCleanRatio = ( ((deadliftWeight * (repsNumber / 3)) * 0.8) * 0.68);
		double powerCleanTotalRep = (deadliftToPowerCleanRatio / machomanWeight); 
		System.out.println("Power Clean Total Round for 3 rep: " + (powerCleanTotalRep / 3));
		
		
		double deadliftToFrontSquatRatio = ( ((deadliftWeight * (repsNumber / 3)) * 0.8) * 0.85);
		double frontSquatTotalRep = (deadliftToFrontSquatRatio / machomanWeight); 
		System.out.println("Front Squat Total Round for 3 rep: " + (frontSquatTotalRep /3) );
		
	}

}
