package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Manager slot DTO */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ManagerSlotRequestDTO {

  /** Start time of manager slot in secs */
  @NotNull private int startTime;

  /** End time of manager slot in secs */
  @NotNull private int endTime;

  /** Slot duration */
  @NotNull private int slotDuration;

  /** Manager Id */
  private long managerId;

  /** Manager Email */
  private String managerEmail;
}
