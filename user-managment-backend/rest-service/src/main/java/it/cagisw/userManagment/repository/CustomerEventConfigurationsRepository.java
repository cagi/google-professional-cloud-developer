package it.cagisw.userManagment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.cagisw.userManagment.dto.CustomerEventConfiguration;

/** Repository for storing customer event definitions. */
public interface CustomerEventConfigurationsRepository
    extends JpaRepository<CustomerEventConfiguration, Long> {

  /**
   * Retrieve all event configurations for a customer
   *
   * @param customerId the unique customer id
   * @return list of event configuration
   */
  List<CustomerEventConfiguration> findAllByCustomerUniqueId(String customerId);
}
