package it.cagisw.userManagment.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "managers")
public class Managers {
	
	/** Id. */
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	/** Manager Name */
	@Column(name = "manager_name", nullable = false)
	private String managerName;

	/** Associated managerslot of manager. */
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "manager_name", referencedColumnName = "manager_name", nullable = false, insertable = false, updatable = false)
	private List<ManagerSlot> managerSlots = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public List<ManagerSlot> getManagerSlots() {
		return managerSlots;
	}

	public void setManagerSlots(List<ManagerSlot> managerSlots) {
		this.managerSlots = managerSlots;
	}
}
