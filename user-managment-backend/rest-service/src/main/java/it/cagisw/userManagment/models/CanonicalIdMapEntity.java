package it.cagisw.userManagment.models;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Entity for canonical id mapping. */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CanonicalIdMapEntity {

  /** The id of the customer party */
  private List<Integer> partyId;

  /** Customer relationship agreement id. */
  private List<Integer> customerRelationshipAgreementId;

  /** The unique identifier for the customer */
  private List<String> customerUniqueIdentifier;

  /** The CIF id */
  private List<String> cifId;

  /** The OLB user id */
  private List<String> olbUserId;

  /** The EQ customer numbers */
  private List<String> eqCustomerNumbers;
}
