package it.cagisw.userManagment.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import it.cagisw.userManagment.models.Appointments;

/** Repository for Appointments table */
public interface AppointmentsRepository extends JpaRepository<Appointments, Long> {

  /**
   * Find appointments for the given customerUniqueId and status different from passed status
   *
   * @param customerUniqueId the customer Unique Id
   * @param status the status
   * @param pageable pagination attributes
   * @return Found list of appointments
   */
  Page<Appointments> findByCustomerUniqueIdAndStatusNot(
      String customerUniqueId, String status, Pageable pageable);

  /**
   * Find appointment s by a given event id
   *
   * @param microsoftGraphEventId the event id related to the appointment
   * @return found appointment for the given event id
   */
  Optional<Appointments> findByMicrosoftGraphEventId(String microsoftGraphEventId);
}
