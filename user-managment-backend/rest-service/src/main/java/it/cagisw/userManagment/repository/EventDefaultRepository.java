package it.cagisw.userManagment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.cagisw.userManagment.dto.Event;
import it.cagisw.userManagment.models.EventDefault;

/** Repository for storing EventDefault. */
public interface EventDefaultRepository extends JpaRepository<EventDefault, Long> {

  /**
   * Retrieve the default configuration of an event
   *
   * @param event event to search.
   * @return optional might contain EventDefault model.
   */
  List<EventDefault> findByEvent(Event event);
}
