package it.cagisw.userManagment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.requests.AppointmentRequest;
import it.cagisw.userManagment.service.AppointmentsService;

@RestController
@RequestMapping("/appointments")
public class AppointmentsController {

	@Autowired
	private AppointmentsService appointmentsService;

	@PostMapping
	public void createAppointment(@RequestBody AppointmentRequest request) {
		appointmentsService.createAppointment(request);
	}

	@PutMapping
	public void updateAppointment() {
	}

	@DeleteMapping
	public void deleteAppointment() {

	}

	@GetMapping
	public void isManagerAvailable() {

	}
}
