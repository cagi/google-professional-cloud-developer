package it.cagisw.userManagment.dto;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** An event configuration for a customer. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerEventConfiguration {

  /** Unique configuration identifier. */
  private long eventConfId;

  /** Unique customer identifier. */
  private String customerUniqueId;

  /** Unique account identifier. */
  private String accountId;

  /** Type of the account. */
  private String accountType;

  /** Threshold structure. */
  private UserThresholdValue userThresholdValue;

  /** Configured channels for the event definition. */
  private List<EventConfigChannelMap> channels = new ArrayList<>();

  /** The master event definition. */
  private Event event;

  /** If event is active for customer. */
  private boolean active;

  /** When event configuration is created for customer. */
  private ZonedDateTime createdOn;

  /** When event configuration is updated for customer. */
  private ZonedDateTime updatedOn;
}
