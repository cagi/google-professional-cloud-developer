package it.cagisw.userManagment.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Object carrying appointment create/update appointment details */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentRequestDTO {

  /** Appointment subject. */
  @NotNull private String subject;

  /** Appointment description. */
  private String description;

  /** Appointment date. */
  @NotNull private ZonedDateTime appointmentDate;

  /** Appointment end date. */
  @NotNull private ZonedDateTime appointmentEndDate;

  /** Appointment mode. */
  @NotNull private String mode;

  /** Appointment address. */
  private String address;
}
