package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;

import it.cagisw.userManagment.models.Country;

public interface CountryRepository extends CrudRepository<Country, Integer> {
}