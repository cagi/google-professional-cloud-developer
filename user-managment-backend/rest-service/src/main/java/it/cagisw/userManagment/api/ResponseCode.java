package it.cagisw.userManagment.api;

/** Enumeration for status of the http requests */
public enum ResponseCode {
  /** Successful request * */
  SUCCESS,

  /** Failure request * */
  FAILURE
}
