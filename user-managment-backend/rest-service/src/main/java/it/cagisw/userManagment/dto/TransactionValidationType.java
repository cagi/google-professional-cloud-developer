package it.cagisw.userManagment.dto;

/** TransactionValidation type */
public enum TransactionValidationType {

  /** validate */
  VALIDATE,

  /** generate code image */
  GENERATE_CODE_IMAGE,
}
