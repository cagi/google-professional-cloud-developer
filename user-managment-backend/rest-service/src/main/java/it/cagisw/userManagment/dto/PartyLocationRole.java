package it.cagisw.userManagment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/** Party location roles */
public enum PartyLocationRole {

  /** Residence */
  @JsonProperty("residence")
  RESIDENCE,

  /** Domicile */
  @JsonProperty("domicile")
  DOMICILE,

  /** Office address */
  @JsonProperty("officeaddress")
  OFFICE_ADDRESS
}
