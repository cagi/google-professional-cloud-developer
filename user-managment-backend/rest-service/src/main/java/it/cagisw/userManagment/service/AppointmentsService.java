package it.cagisw.userManagment.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import it.cagisw.userManagment.exceptions.BaseServiceException;
import it.cagisw.userManagment.models.Appointment;
import it.cagisw.userManagment.repository.RepositoryAppointment;
import it.cagisw.userManagment.requests.AppointmentRequest;

@Service
public class AppointmentsService {

	@Autowired
	private RepositoryAppointment appointmentRep;

	public void isManagerAvailable(int appointmentId) {

		appointmentRep.findById(appointmentId).filter(a -> a.getAppointmentDate() != null).ifPresent( a -> { System.out.println();});
		
	}

	public void createManagerAvailability() {

	}

	public void listAppointmentsByUser(int userId) {
		List<String> collect = appointmentRep.findById(userId).stream().map(appointment -> appointment.getManagerName())
				.collect(Collectors.toList());

	}

	public void createAppointment(AppointmentRequest req) {
		// BeanUtils.copyProperties(null, req);

	}

	private Appointment getAppointmentById(int id) {
		Appointment appointment = appointmentRep.findById(id)
				.orElseThrow(() -> new BaseServiceException("application not found", HttpStatus.NOT_FOUND));
		return appointment;
	}

	
}
