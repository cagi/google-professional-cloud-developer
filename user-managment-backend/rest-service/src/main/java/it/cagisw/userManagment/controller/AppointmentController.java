package it.cagisw.userManagment.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.dto.AppointmentCancelRequestDTO;
import it.cagisw.userManagment.dto.AppointmentDTO;
import it.cagisw.userManagment.dto.AppointmentRequestDTO;
import it.cagisw.userManagment.dto.PaginatedList;
import it.cagisw.userManagment.service.interfaces.AppointmentService;

/** Appointments controller. */
@RequestMapping("/appointments")
@RestController
public class AppointmentController {

  /** Auto injected AppointmentService instance */
  @Autowired AppointmentService service;

  /**
   * Creates an Appointment
   *
   * @param request object carrying appointment data
   * @return created Appointment
   */
  @PostMapping("/{customerUniqueId}")
  public GenericResponse<AppointmentDTO> createAppointment(
		  @PathVariable("customerUniqueId") String customerUniqueId,
      @Valid @RequestBody AppointmentRequestDTO request) {
    return new GenericResponse<>(service.createAppointment(request, customerUniqueId));
  }

  /**
   * Get all appointments for the logged-in user
   *
   * @param customerUniqueId logged in user
   * @param skip number of rows to skip
   * @param limit number of rows to return
   * @return list of found manager ratings
   */
  @GetMapping("/{customerUniqueId}")
  public GenericResponse<PaginatedList> getAllAppointments(
		  @PathVariable("customerUniqueId") String customerUniqueId,
      @RequestParam int skip,
      @RequestParam int limit) {
    return new GenericResponse<>(service.findAll(customerUniqueId, skip, limit));
  }

  /**
   * Get appointment by its ID
   *
   * @param customerUniqueId id of the logged-in user
   * @param appointmentId ID of the appointment
   * @return found appointment
   */
  @GetMapping("/{appointmentId}/{customerUniqueId}")
  public GenericResponse<AppointmentDTO> getAppointmentById(
		  @PathVariable("customerUniqueId") String customerUniqueId,
      @PathVariable("appointmentId") long appointmentId) {
    return new GenericResponse<>(service.findById(appointmentId, customerUniqueId));
  }

  /**
   * Updates an Appointment
   *
   * @param appointmentId ID of the appointment
   * @param request object carrying updated ratings properties
   * @return updated appointment
   */
  @PutMapping("/{appointmentId}/{customerUniqueId}")
  public GenericResponse<AppointmentDTO> updateAppointment(
		  @PathVariable("customerUniqueId") String customerUniqueId,
      @PathVariable("appointmentId") long appointmentId,
      @Valid @RequestBody AppointmentRequestDTO request) {
    return new GenericResponse<>(
        service.updateAppointment(appointmentId, request, customerUniqueId));
  }

  /**
   * Canel Appointment
   *
   * @param appointmentId ID of the appointment
   * @return updated appointment
   */
  @PutMapping("/{appointmentId}/cancel/{customerUniqueId}")
  public GenericResponse<AppointmentDTO> cancelAppointment(
		  @PathVariable("customerUniqueId") String customerUniqueId,
      @PathVariable("appointmentId") long appointmentId,
      @Valid @RequestBody AppointmentCancelRequestDTO request) {
    return new GenericResponse<>(
        service.cancelAppointment(appointmentId, request, customerUniqueId));
  }

  /**
   * Accept new date in Appointment
   *
   * @param appointmentId ID of the appointment
   * @return updated appointment
   */
  @PutMapping("/{appointmentId}/accept/{customerUniqueId}")
  public GenericResponse<AppointmentDTO> acceptNewAppointmentDate(
		  @PathVariable("customerUniqueId") String customerUniqueId,
      @PathVariable("appointmentId") long appointmentId) {
    return new GenericResponse<>(service.acceptNewAppointmentDate(appointmentId, customerUniqueId));
  }

  /**
   * Delete an Appointment by ID
   *
   * @param customerUniqueId id of the logged-in user
   * @param appointmentId ID of the appointment to be deleted
   */
  @DeleteMapping("/{appointmentId}/{customerUniqueId}")
  public GenericResponse deleteAppointment(
		  @PathVariable("customerUniqueId") String customerUniqueId,
      @PathVariable("appointmentId") long appointmentId) {
    service.deleteAppointment(appointmentId, customerUniqueId);
    return new GenericResponse();
  }
}
