package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/** Base class for the location. */
@Data
@NoArgsConstructor
@SuperBuilder
public abstract class BaseLocation {

  /** Status of the location */
  @NotNull private String locationStatus;

  /** Type of the location */
  @NotNull private String locationType;

  /** Value of the asset */
  @NotNull private String locationValue;
}
