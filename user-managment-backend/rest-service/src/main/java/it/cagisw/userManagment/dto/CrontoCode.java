package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Structure to represent a cronto code. */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CrontoCode {

  /** Base64 encoded image for cronto code. */
  private String image;
}
