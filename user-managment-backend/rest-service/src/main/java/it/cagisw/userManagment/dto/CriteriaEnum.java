package it.cagisw.userManagment.dto;

/** Enumeration for query criteria. */
public enum CriteriaEnum {

  /** Like comparison for Strings. */
  LIKE,

  /** Equality comparison for all type of values. */
  IS
}
