package it.cagisw.userManagment.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** The response with the profile. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProfileResponse {

  /** Details with name of the customer */
  @NotNull private CustomerName customerName;

  /** Date of birth */
  @NotNull private String dob;

  private String nationality;

  private List<ContactPoint> contactPoints;

  private List<PartyLocation> partyLocations;

  /** Indicates if the customer is an UK citizen */
  @NotNull private Boolean ukCitizen;
}
