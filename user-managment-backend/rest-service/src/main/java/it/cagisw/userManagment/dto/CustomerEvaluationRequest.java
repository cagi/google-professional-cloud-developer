package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Request to evaluate the profile of a customer. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerEvaluationRequest {

  /** Unique identifier of a customer */
  @NotBlank private String customerUniqueIdentifier;

  /** Name details of the customer */
  private CustomerName customerName;

  /** Date of birth */
  @NotBlank private String dateOfBirth;

  /** Indicates if the customer is UK citizen */
  private Boolean ukCitizen;

  /** Post code */
  private String postCode;

  /** Channel */
  private String channel;
}
