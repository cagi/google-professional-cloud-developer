package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;

import it.cagisw.userManagment.models.AppointmentAttachments;

public interface RepositoryAppointmentAttachments extends CrudRepository<AppointmentAttachments, Integer>{

}
