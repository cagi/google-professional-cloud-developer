package it.cagisw.userManagment.models;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/** A Pojo representing a user registration operation. */
@Entity
@Data
@Table(name = "REGISTERED_USERS")
public class RegisteredUser {

  /** Primary key. */
  @Column(name = "REG_ID")
  @Id
  @GeneratedValue
  private long regId;

  /** Customer identifier of the registered user. */
  @Column(name = "CUSTOMERUNIQUEIDENTIFIER")
  private String customerUniqueIdentifier;

  /** New username for the user. */
  @Column(name = "USERNAME")
  private String username;

  /** Onespan domain of the user. */
  @Column(name = "DOMAIN")
  private String domain;

  /** Authenticator serial key that was assigned by onespan. */
  @Column(name = "SERIAL_KEY")
  private String serialKey;

  /** Mfa type for login. */
  @Column(name = "MFA_TYPE")
  @Enumerated(EnumType.STRING)
  private MfaType mfaType;

  /** Status of the onespan registration. */
  @Column(name = "REGISTRATION_STATUS")
  @Enumerated(EnumType.STRING)
  private RegistrationStatus registrationStatus;

  /** The internal status of the user. */
  @Column(name = "ACTIVE")
  private boolean active;

  /** Operating system of the device. */
  @Column(name = "DEVICE_OS")
  private String deviceOs;

  /** Identifier of the device. */
  @Column(name = "DEVICE_ID")
  private String deviceIdentifier;

  /** Binding id of the registration flow. */
  @Column(name = "BINDING_ID")
  private String bindingId;

  /** Created on audit field. */
  @Column(name = "CREATED_ON")
  private ZonedDateTime createdOn;

  /** Updated on audit field. */
  @Column(name = "UPDATED_ON")
  private ZonedDateTime updatedOn;
}
