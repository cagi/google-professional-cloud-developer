package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Response structure for login session check. */
@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class LoginSessionResponse {

  /** Session status of the login request. */
  private SessionStatus sessionStatus;

  /** Authentication details */
  private LoginResponse loginResponse;
}
