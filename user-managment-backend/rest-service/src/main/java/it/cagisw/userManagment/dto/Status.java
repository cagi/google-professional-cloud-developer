package it.cagisw.userManagment.dto;

/** Enumeration for the execution status if an operation. */
public enum Status {

  /** Operation is successful */
  SUCCESS,

  /** Operation has failed */
  FAIL
}
