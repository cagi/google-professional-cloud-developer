package it.cagisw.userManagment.api;

import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Data structure to represent a session. */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserSession {

  /** Device unique id, only for mobile session */
  private String deviceIdentifier;

  /** Device operating system, only for mobile session */
  private String deviceOs;

  /** Session Type */
  private SessionType sessionType;

  /** Access token */
  private String token;

  /** Refresh token */
  private String refreshToken;

  /** Session expiry time. */
  private ZonedDateTime expiry;
}
