package it.cagisw.userManagment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/** Types of a contact point. */
public enum ContactPoint {
  /** Email contact. */
  @JsonProperty("electronicAddress")
  ELECTRONIC_ADDRESS,

  /** Phone address. */
  @JsonProperty("phoneAddress")
  PHONE_ADDRESS,

  /** Mobile address. */
  @JsonProperty("cellPhoneAddress")
  MOBILE_ADDRESS,

  /** Social network address */
  @JsonProperty("socialNetworkAddress")
  SOCIAL_NETWORK_ADDRESS,

  /** Postal address. */
  @JsonProperty("postalAddress")
  POSTAL_ADDRESS,

  /** Trading address. */
  @JsonProperty("tradingAddress")
  TRADING_ADDRESS,
}
