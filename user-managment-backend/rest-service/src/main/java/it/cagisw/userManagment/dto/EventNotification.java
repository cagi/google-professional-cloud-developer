package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Event notification structure for internal services to send a message. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventNotification {

  /** The customer that message will be communicated. */
  @NotNull private String customerUniqueIdentifier;

  /** The event to be evaluated. */
  @NotNull private String event;

  /** The optional account number. */
  private String accountNumber;

  /** Value to check against threshold of event. */
  @NotNull private int eventTriggerValue;
}
