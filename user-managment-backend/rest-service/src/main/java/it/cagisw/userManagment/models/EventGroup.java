package it.cagisw.userManagment.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/** POJO representing a event group master record. */
@Data
@Entity
@Table(name = "EVENT_GROUP")
public class EventGroup {

  /** Primary key */
  @Id
  @GeneratedValue
  @Column(name = "EVENT_GROUP_ID")
  private long eventGroupId;

  /** Name of the event group definition */
  @Column(name = "GROUP_NAME")
  private String groupName;

  /** Foreign key to event group type */
  @ManyToOne
  @JoinColumn(name = "EVENT_GROUP_TYPE_ID")
  private EventGroupType eventGroupType;

  /** Description of the event group */
  @Column(name = "DESCRIPTION")
  private String description;
}
