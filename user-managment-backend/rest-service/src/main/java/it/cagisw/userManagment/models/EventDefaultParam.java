package it.cagisw.userManagment.models;

/** Enumeration for event type. */
public enum EventDefaultParam {
  /** Event default status. */
  DEFAULT_SETTING,

  /** Default threshold of the event.. */
  DEFAULT_THRESHOLD
}
