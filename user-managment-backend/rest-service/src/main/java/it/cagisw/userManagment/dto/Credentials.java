package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Structure for credentials. */
@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class Credentials {

  /** Pass key. */
  private PassKey passKey;

  /** Authenticator instance. */
  private Authenticator authenticator;
}
