package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Request a new transaction validation to onespan api. */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionValidationRequest {

  /** Input payload object type. */
  @NotBlank private TransactionInputEnum objectType;

  /** Validation timeout. */
  @NotBlank private int timeout;

  /** mfa event type, default is push */
  private TMfaType mfaEvent = TMfaType.T_PUSH;

  /** action type */
  private TransactionValidationType actionType = TransactionValidationType.VALIDATE;

  /** Data field to be used in validation. */
  @NotNull private TransactionValidationData data;
}
