package it.cagisw.userManagment.service.interfaces;

import java.util.Optional;

import it.cagisw.userManagment.models.CustomerProfile;

/** Service to perform operations with cache server. */
public interface CacheService {

  /**
   * Retrieve the customer profile from cache.
   *
   * @param cui customer unique identifier
   * @return an optional customer profile
   */
  Optional<CustomerProfile> getCustomerProfile(String cui);

  /**
   * Save the profile for a customer.
   *
   * @param customerProfile customer profile
   */
  void saveCustomerProfile(CustomerProfile customerProfile);
}
