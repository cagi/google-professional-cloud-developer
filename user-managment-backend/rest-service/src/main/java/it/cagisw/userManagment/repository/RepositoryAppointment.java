package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;

import it.cagisw.userManagment.models.Appointment;

public interface RepositoryAppointment extends CrudRepository<Appointment, Integer>{

}
