package it.cagisw.userManagment.repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import it.cagisw.userManagment.dto.CriteriaEnum;
import it.cagisw.userManagment.dto.Filter;
import it.cagisw.userManagment.models.CustomerEventLogHistory;
import it.cagisw.userManagment.models.RequestType;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class HistorySpecification implements Specification<CustomerEventLogHistory> {

  /** Serial id. */
  private static final long serialVersionUID = 1L;

  /** Filter instance to build a specification */
  private transient Filter filter;

  /**
   * Construct a predicate out of filter.
   *
   * @param root the root predicate.
   * @param query the jpa criteria query.
   * @param builder criteria builder instance.
   * @return a constructed predicate.
   */
  @Override
  public Predicate toPredicate(
      Root<CustomerEventLogHistory> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

    if (CriteriaEnum.IS == filter.getFilterCriteriaEnum()) {
      if (root.get(filter.getParam()).getJavaType() == boolean.class) {
        return builder.equal(
            root.<Boolean>get(filter.getParam()), Boolean.valueOf(filter.getValue()));
      } else if (root.get(filter.getParam()).getJavaType() == RequestType.class) {
        return builder.equal(
            root.<Enum>get(filter.getParam()), RequestType.valueOf(filter.getValue()));
      } else if (root.get(filter.getParam()).getJavaType() == long.class) {
        return builder.equal(root.<Long>get(filter.getParam()), Long.valueOf(filter.getValue()));
      }
      return builder.equal(root.<String>get(filter.getParam()), filter.getValue());
    } else if (CriteriaEnum.LIKE == filter.getFilterCriteriaEnum()) {
      if (root.get(filter.getParam()).getJavaType() == String.class) {
        return builder.like(root.get(filter.getParam()), "%" + filter.getValue() + "%");
      } else {
        return builder.equal(root.get(filter.getParam()), filter.getValue());
      }
    }
    return null;
  }
}
