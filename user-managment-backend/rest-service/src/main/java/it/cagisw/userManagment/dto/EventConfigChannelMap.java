package it.cagisw.userManagment.dto;

import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** The channels for an event. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventConfigChannelMap {

  /** Unique identifier for mapping. */
  private long channelMapId;

  /** Details of associated channel. */
  private Channel channel;

  /** Default value of activation of channel. */
  private boolean defaultValue;

  /** Current value of activation of channel. */
  private boolean active;

  /** When the channel is created. */
  private ZonedDateTime createdOn;

  /** When the channel is updated. */
  private ZonedDateTime updatedOn;
}
