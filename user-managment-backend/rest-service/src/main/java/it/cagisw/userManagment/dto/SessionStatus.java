package it.cagisw.userManagment.dto;

/** Session status enum for a login operation. */
public enum SessionStatus {

  /** unkonw */
  UNKNOWN,

  /** pending */
  PENDING,

  /** accepted */
  ACCEPTED,

  /** refused */
  REFUSED,

  /** timeout */
  TIMEOUT,

  /** failed */
  FAILED,

  /** rebinding */
  REBINDING,
}
