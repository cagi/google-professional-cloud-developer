package it.cagisw.userManagment.service;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import it.cagisw.userManagment.dto.CanonicalIdMapIdentifierRequest;
import it.cagisw.userManagment.dto.CanonicalIdMapIdentifierResponse;
import it.cagisw.userManagment.dto.CanonicalIdMapRequest;
import it.cagisw.userManagment.dto.CanonicalIdMapResponse;
import it.cagisw.userManagment.dto.ContactPoint;
import it.cagisw.userManagment.dto.CustomerEvaluationRequest;
import it.cagisw.userManagment.dto.CustomerName;
import it.cagisw.userManagment.dto.IdentifierType;
import it.cagisw.userManagment.dto.LocationPhoneAddress;
import it.cagisw.userManagment.dto.ProfileNameRequest;
import it.cagisw.userManagment.dto.ProfileNameResponse;
import it.cagisw.userManagment.dto.ProfileRequest;
import it.cagisw.userManagment.dto.ProfileResponse;
import it.cagisw.userManagment.dto.ProfileStatusRequest;
import it.cagisw.userManagment.dto.ProfileStatusResponse;
import it.cagisw.userManagment.dto.ValidationResponse;
import it.cagisw.userManagment.exceptions.BaseServiceException;
import it.cagisw.userManagment.exceptions.SubError;
import it.cagisw.userManagment.models.CustomerProfile;
import it.cagisw.userManagment.service.interfaces.CustomerProfileService;
import it.cagisw.userManagment.service.interfaces.CustomerProfileStorageService;
import lombok.extern.slf4j.Slf4j;

/**
 * This service is responsible for managing customer profiles and performs validation against
 * existing records.
 */
@Service
@Slf4j
public class CustomerProfileServiceImpl implements CustomerProfileService {

  /** UK country code */
  private static final String UK_COUNTRY_CODE = "44";

  /** Model mapper */
  private final ModelMapper modelMapper = new ModelMapper();

  /** Service to perform storage operations of customer profile entity */
  private final CustomerProfileStorageService customerProfileStorageService;

  /**
   * Create new CustomerProfileServiceImpl instance
   *
   * @param service service to perform storage operations of customer profile entity
   */
  public CustomerProfileServiceImpl(final CustomerProfileStorageService service) {
    customerProfileStorageService = service;
  }

  /**
   * Retrieve canonical id map of customer.
   *
   * @param request payload
   * @return the canonical id mapping
   */
  @Override
  public CanonicalIdMapResponse canonicalMap(CanonicalIdMapRequest request) {
    var customerProfile =
        customerProfileStorageService.getCustomerProfile(request.getCustomerUniqueIdentifier());
    return CanonicalIdMapResponse.builder()
        .partyId(getPartyId(customerProfile))
        .customerUniqueIdentifier(request.getCustomerUniqueIdentifier())
        .ccdsPartyId(request.getCustomerUniqueIdentifier())
        .eqCustomerNumbers(getIdentifiers(IdentifierType.EQCUSTOMERNO, customerProfile))
        .cifId(
            customerProfile.getCanonicalIdMapEntity().getCifId().stream().findFirst().orElse(null))
        .olbUserId(
            customerProfile.getCanonicalIdMapEntity().getOlbUserId().stream()
                .findFirst()
                .orElse(null))
        .build();
  }

  /**
   * Retrieve canonical id map of customer by identifier.
   *
   * @param request payload
   * @return the canonical id mapping
   */
  public CanonicalIdMapIdentifierResponse canonicalMapByIdentifier(
      CanonicalIdMapIdentifierRequest request) {
    var customerProfile =
        customerProfileStorageService.getCustomerProfile(request.getCustomerUniqueIdentifier());
    return CanonicalIdMapIdentifierResponse.builder()
        .partyId(getPartyId(customerProfile))
        .identifierType(request.getIdentifierType())
        .identifiers(getIdentifiers(request.getIdentifierType(), customerProfile))
        .build();
  }

  /**
   * Retrieve party id.
   *
   * @param customerProfile customer profile
   * @return party id
   */
  private int getPartyId(final CustomerProfile customerProfile) {
    return customerProfile.getCanonicalIdMapEntity().getPartyId().stream()
        .findFirst()
        .orElseThrow(() -> new BaseServiceException("",new ArrayList<SubError>()));
  }

  /**
   * Get the identifiers based on type
   *
   * @param type identifier type
   * @param customerProfile customer profile
   * @return identifiers
   */
  private List<String> getIdentifiers(
      final IdentifierType type, final CustomerProfile customerProfile) {
    var canonicalMaps = customerProfile.getCanonicalIdMapEntity();
    /*
    switch (type) {
      case CIFID:
        return canonicalMaps.getCifId();
      case OLBUSERID:
        return canonicalMaps.getOlbUserId();
      case EQCUSTOMERNO:
        return canonicalMaps.getEqCustomerNumbers();
      case CUSTOMERRELATIONSHIPAGREEMENTID:
        return canonicalMaps.getCustomerRelationshipAgreementId().stream()
            .filter(Objects::nonNull)
            .map(String::valueOf)
            .collect(Collectors.toList());
      default:
        throw new BaseServiceException("CPR_ERR000012.name()", new ArrayList<SubError>);
    }
    */
    return null;
  }

  /**
   * Retrieve status of relationship for customer with the bank.
   *
   * @param request payload
   * @return the status of the relationship
   */
  @Override
  public ProfileStatusResponse relationshipStatus(final ProfileStatusRequest request) {
    return ProfileStatusResponse.builder()
        .status(
            customerProfileStorageService
                .getCustomerProfile(request.getCustomerUniqueIdentifier())
                .isPartyRelationshipStatus())
        .build();
  }

  /**
   * Retrieve the name of a given customer.
   *
   * @param request payload
   * @return DTO with name details
   */
  @Override
  public ProfileNameResponse name(final ProfileNameRequest request) {
    var customerProfile =
        customerProfileStorageService.getCustomerProfile(request.getCustomerUniqueIdentifier());
    return modelMapper.map(
        customerProfile.getProfileEntity().getCustomerName(), ProfileNameResponse.class);
  }

  /**
   * Validates for the existence of given customer profile details over the database.
   *
   * @param request payload with data to evaluate customer
   * @return the SUCCESS or FAIL response. Returns success if there is a match.
   */
  @Override
  public ValidationResponse evaluate(CustomerEvaluationRequest request) {
	  /*
    ValidationResponse result =
        ValidationResponse.builder()
            .customerUniqueIdentifier(request.getCustomerUniqueIdentifier())
            .build();
    try {
      ProfileRequest profileRequest = new ProfileRequest();
      profileRequest.setCustomerUniqueIdentifier(request.getCustomerUniqueIdentifier());
      ProfileResponse profile = retrieve(profileRequest);

      var eqPostalAddress =
          profile.getPartyLocations().stream()
              .filter(pl -> pl.getPartyLocationType().equals(ContactPoint.POSTAL_ADDRESS))
              .map(PartyLocation::getLocation)
              .filter(Objects::nonNull)
              .map(l -> ((LocationPostalAddress) l).getLocationAddress())
              .filter(Objects::nonNull)
              .map(PostalAddress::getLegacyEqPostalAddress)
              .filter(Objects::nonNull)
              .filter(LegacyEqPostalAddress::getActive)
              .collect(Collectors.toList());

      String postCode = !eqPostalAddress.isEmpty() ? eqPostalAddress.get(0).getPostCode() : null;

      if (equalsIgnoreNullOrEmpty(request.getPostCode(), postCode)
          && request.getDateOfBirth().equals(profile.getDob())
          && !(request.getCustomerName() != null
              && !isNameValid(request.getCustomerName(), profile.getCustomerName()))
          && !(request.getUkCitizen() != null
              && profile.getUkCitizen() != null
              && !request.getUkCitizen().equals(profile.getUkCitizen()))) {
        result.setResponse(Response.SUCCESS);
      } else {
        result.setResponse(Response.FAILURE);
      }
      return result;
    } catch (Exception e) {
      throw new BaseServiceException(CPR_ERR000001.toString(), Collections.emptyList(), e);
    }
    */
	  return null;
  }

  /**
   * Validates if the name of a customer is valid, will return valid if fist name and last name are
   * not passed in request.
   *
   * @param nameToValidate the name from request to validate
   * @param name the name from DB or party data to validate
   * @return true if it's valid
   */
  boolean isNameValid(final CustomerName nameToValidate, final CustomerName name) {
    if (!StringUtils.hasText(nameToValidate.getFirstName())
        && !StringUtils.hasText(nameToValidate.getLastName())) {
      return true;
    }
    return equalsIgnoreNullOrEmpty(nameToValidate.getFirstName(), name.getFirstName(), false)
        && equalsIgnoreNullOrEmpty(nameToValidate.getLastName(), name.getLastName(), false)
        && equalsIgnoreNullOrEmpty(nameToValidate.getMiddleName(), name.getMiddleName(), false)
        && equalsIgnoreNullOrEmpty(nameToValidate.getTitle(), name.getTitle(), false);
  }

  /**
   * Compares a string ignoring nulls or empty strings, if both are valid then it will compare them
   * case insensitive.
   *
   * @param source source string
   * @param target target string
   * @return true if both are valid and equal
   */
  boolean equalsIgnoreNullOrEmpty(final String source, final String target) {
    return equalsIgnoreNullOrEmpty(source, target, true);
  }

  /**
   * Compares a string ignoring nulls or empty strings, if both are valid then it will compare them
   * case insensitive.
   *
   * @param source source string
   * @param target target string
   * @param ignoreCase flag to indicate if should ignore case
   * @return true if both are valid and equal
   */
  boolean equalsIgnoreNullOrEmpty(
      final String source, final String target, final boolean ignoreCase) {
    if (source == null || target == null) {
      return true;
    }

    if (source.isBlank() || target.isBlank()) {
      return true;
    }
    if (ignoreCase) {
      return source.replace(" ", "").equalsIgnoreCase(target.replace(" ", ""));
    } else {
      return source.replace(" ", "").equals(target.replace(" ", ""));
    }
  }

  /**
   * Retrieve the profile of a given customer.
   *
   * @param request payload
   * @return profile of customer
   */
  @Override
  public ProfileResponse retrieve(ProfileRequest request) {
    var customerProfile =
        customerProfileStorageService.getCustomerProfile(request.getCustomerUniqueIdentifier());
    var response = modelMapper.map(customerProfile.getProfileEntity(), ProfileResponse.class);
    setCountryCodeIfUkCitizen(response);
    return response;
  }

  /**
   * Set the country code for the phone addresses if those are null and the customer is UK citizen.
   *
   * @param response profile response
   */
  private void setCountryCodeIfUkCitizen(final ProfileResponse response) {
    if (Boolean.TRUE.equals(response.getUkCitizen())
        && !response.getPartyLocations().isEmpty()
        && response.getPartyLocations().stream()
            .anyMatch(
                pl ->
                    pl.getPartyLocationType() == ContactPoint.MOBILE_ADDRESS
                        || pl.getPartyLocationType() == ContactPoint.PHONE_ADDRESS)) {
      response
          .getPartyLocations()
          .forEach(
              pl -> {
                if (pl.getPartyLocationType() == ContactPoint.MOBILE_ADDRESS
                    || pl.getPartyLocationType() == ContactPoint.PHONE_ADDRESS) {
                  var phone = (LocationPhoneAddress) pl.getLocation();
                  if (phone.getLocationAddress() != null
                      && !StringUtils.hasText(phone.getLocationAddress().getCountryCode())) {
                    phone.getLocationAddress().setCountryCode(UK_COUNTRY_CODE);
                  }
                }
              });
    }
  }
}
