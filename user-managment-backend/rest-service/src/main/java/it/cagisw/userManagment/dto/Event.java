package it.cagisw.userManagment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** The event structure. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Event {
  /** Unique event Identifier. */
  private Long eventId;

  /** Event's group details. */
  private EventGroup eventGroup;

  /** Name of the event, simplify eventName as event. */
  @JsonProperty("event")
  private String eventName;

  /** The description of the event. */
  private String eventDescription;

  /** The message template of event. */
  private String messageTemplate;

  /** Image Url of the event. */
  private String imageUrl;

  /** If event has a threshold value and editable, default is false. */
  private boolean valueEditable = false;

  /** If event is active. */
  private Boolean active;
}
