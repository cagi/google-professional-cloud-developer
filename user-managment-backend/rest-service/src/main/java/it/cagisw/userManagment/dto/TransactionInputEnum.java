package it.cagisw.userManagment.dto;

/** Session status enum for a transaction validation input. */
public enum TransactionInputEnum {
  /** valid input */
  TransactionValidationInput,

  /** transaction validationInput */
  AdaptiveTransactionValidationInput
}
