package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Location electronic address. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ElectronicAddress {

  /** Address */
  private String address;
}
