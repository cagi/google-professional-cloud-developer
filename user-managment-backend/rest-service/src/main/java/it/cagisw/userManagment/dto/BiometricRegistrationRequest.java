package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Response structure for biometric registration. */
@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class BiometricRegistrationRequest {

  /** Type of biometric. */
  private BiometricType biometric;

  /** Identifier for the user. */
  private String userId;
}
