package it.cagisw.userManagment.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Party Location POJO. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PartyLocation {

  /** Party location role */
  @NotNull private PartyLocationRole partyLocationRole;

  /** Party location type */
  @NotNull private ContactPoint partyLocationType;

  /** Location entity */
  @JsonTypeInfo(
      use = JsonTypeInfo.Id.NAME,
      include = JsonTypeInfo.As.EXTERNAL_PROPERTY,
      property = "partyLocationType")
  @JsonSubTypes({
    @JsonSubTypes.Type(value = LocationElectronicAddress.class, name = "electronicAddress"),
    @JsonSubTypes.Type(value = LocationElectronicAddress.class, name = "socialNetworkAddress"),
    @JsonSubTypes.Type(value = LocationPhoneAddress.class, name = "phoneAddress"),
    @JsonSubTypes.Type(value = LocationPhoneAddress.class, name = "cellPhoneAddress"),
    @JsonSubTypes.Type(value = LocationPostalAddress.class, name = "postalAddress"),
    @JsonSubTypes.Type(value = LocationPostalAddress.class, name = "tradingAddress")
  })
  @NotNull
  @Valid
  private LocationEntity location;
}
