package it.cagisw.userManagment.models;

import java.io.Serializable;
import lombok.Data;

/** Id structure of event default id */
@Data
public class EventDefaultId implements Serializable {

  /** Serial version of class. */
  private static final long serialVersionUID = 1179549695184872799L;

  /** Event name. */
  private long event;

  /** Channel name. */
  private long eventDefaultId;
}
