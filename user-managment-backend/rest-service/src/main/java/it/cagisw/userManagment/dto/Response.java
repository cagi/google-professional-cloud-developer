package it.cagisw.userManagment.dto;

/** Used to indicate the result of an operation. */
public enum Response {

  /** Successful operation */
  SUCCESS,

  /** Failed operation */
  FAILURE
}
