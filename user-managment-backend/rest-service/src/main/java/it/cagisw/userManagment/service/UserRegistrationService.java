package it.cagisw.userManagment.service;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.cagisw.userManagment.dto.UserDTO;
import it.cagisw.userManagment.models.User;
import it.cagisw.userManagment.repository.UserRepository;
import it.cagisw.userManagment.requests.ListUserRequest;
import it.cagisw.userManagment.requests.UserRequest;
import it.cagisw.userManagment.specification.UserSpecification;

@Service
public class UserRegistrationService {
	
	private static Logger log = LoggerFactory.getLogger(UserRegistrationService.class);

	@Autowired
	private UserRepository userRep;

	public List<UserDTO> getAllUsers(String name) {
		Specification<User> spec = (root, query, builder) -> {
			Predicate result = builder.or(builder.equal(root.get("username"), name));
			return result;
		};

		List<User> list = userRep.findAll(spec);
		List<UserDTO> result = list.stream().map(UserDTO::fromModel).collect(toList());
		return result;
	}
	
    public UserDTO getUser(int id) {
    	Specification<User> spec = (root, query, builder) -> {
			Predicate result = builder.or(builder.equal(root.get("id"), id));
			return result;
		};

		List<User> list = userRep.findAll(spec);
		List<UserDTO> result = list.stream().map(UserDTO::fromModel).collect(toList());
        return result.get(0);
    }

	@Transactional
	public List<UserDTO> createUsers(ListUserRequest listUserRequest) {

		List<User> collect = listUserRequest.getUserRequests().stream().map(dto -> {
			User user = new User();
			user.setFullName(dto.getFullName());
			user.setUsername(dto.getUsername());
			user.setEmailAddress(dto.getEmailAddress());
			return user;
		}).collect(toList());
		
		List<User> resultSave = userRep.saveAll(collect);
		List<UserDTO> result = resultSave.stream().map(UserDTO::fromModel).collect(toList());
		return result;
	}
	
	@Transactional
	public UserDTO updateUser(UserRequest userRequest) {
		Example<User> example = Example.of(User.from(Integer.parseInt(userRequest.getId()),userRequest.getUsername()));
		Optional<User> result = userRep.findOne(example);
		log.debug("[updateUser] entity", result);
		User toSave = result.get();
		BeanUtils.copyProperties(userRequest, toSave);
		User save = userRep.save(toSave);
		UserDTO dto = new UserDTO();
		BeanUtils.copyProperties(save, dto);
		return dto;
	}
	
	public void prova() {
		Specification<User> spec = new UserSpecification();
		//spec.
		userRep.findAll(spec);
		
	}

}
