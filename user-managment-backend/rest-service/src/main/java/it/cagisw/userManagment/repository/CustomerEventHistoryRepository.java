package it.cagisw.userManagment.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import it.cagisw.userManagment.models.CustomerEventLogHistory;

/** Repository for storing master event histories. */
public interface CustomerEventHistoryRepository
    extends PagingAndSortingRepository<CustomerEventLogHistory, Long>,
        JpaSpecificationExecutor<CustomerEventLogHistory> {

  /**
   * Retrieve history records by filtering by dates.
   *
   * @param customerId unique customer id.
   * @param dateTime filter to include newer records.
   * @param sort pageable instance for pagination.
   * @return list of event history logs.
   */
  List<CustomerEventLogHistory> findByCustomerUniqueIdAndCreatedOnAfterOrderByHistoryIdDesc(
      String customerId, ZonedDateTime dateTime, Pageable sort);

  /**
   * Find all records by applying the pagination.
   *
   * @param customerId unique customer id.
   * @param sort pagination details.
   * @return list of event histories.
   */
  List<CustomerEventLogHistory> findByCustomerUniqueId(String customerId, Pageable sort);
}
