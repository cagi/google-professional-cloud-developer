package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Response structure for binding id. */
@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class BindingIdResponse {

  /** Identifier for the user. */
  private String bindingId;
}
