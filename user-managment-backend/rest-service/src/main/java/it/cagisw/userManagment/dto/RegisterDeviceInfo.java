package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Request structure to register a new device. */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDeviceInfo {

  /** User id to register device. */
  @NotBlank private String userId;

  /** Device binding id. */
  @NotBlank private String bindingId;

  /** Device Identifier. */
  @NotBlank private String deviceIdentifier;

  /** Device operating system. */
  @NotBlank private String deviceOs;
}
