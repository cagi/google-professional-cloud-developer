package it.cagisw.userManagment.service;

import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import it.cagisw.userManagment.dto.ChannelTransmissionRequest;
import it.cagisw.userManagment.models.Channel;
import lombok.Data;

/**
 * The main service that is responsible for running acquiring communication history ids and routing
 * the requests into the corresponding services. This injects all the implementations of
 * CommunicationChannel interface and acts like a factory
 */
@Service
public class ChannelRouterServiceImpl implements ChannelRouter {


  /**
   * Routes the incoming communication channels.
   *
   * @param channelTransmissionRequest the incoming communication requests.
   * @param serviceEvent the originator cyn event
   * @return the return information containing temporary token.
   */
  @Override
  public Map<Object, Object> route(
      ChannelTransmissionRequest channelTransmissionRequest, String serviceEvent) {
	  
	  /*
    ChannelActivityLogResponse channelActivityLogResponse =
        channelActivityRecordHelper.recordChannelActivity(channelTransmissionRequest, serviceEvent);

    return channelTransmissionRequest.getChannels().stream()
        .map(
            channel ->
                transmitMessageToChannel(
                    channelTransmissionRequest, serviceEvent, channelActivityLogResponse, channel))
        .filter(Optional::isPresent)
        .collect(Collectors.toMap(tuple -> tuple.get().getKey(), tuple -> tuple.get().getValue()));
    */
	  return null;
  }

  /**
   * Utility method to run pre-checks and sending message to channel asynchronously
   *
   * @param channelTransmissionRequest the actual transmission request
   * @param serviceEvent the actual services event that triggered the communication
   * @param channelActivityLogResponse the activity log response for the parent activity in history
   *     service
   * @param channel channel to send the communication
   * @return channel receipt
   */
  private void transmitMessageToChannelAsync(
      ChannelTransmissionRequest channelTransmissionRequest,
      String serviceEvent,
      Channel channel) {
	  
	  /*
    Optional<CommunicationChannel> channelOptional = getCommunicationChannel(channel);
    if (channelOptional.isEmpty()) {
      return;
    }
    CommunicationChannel communicationChannel = channelOptional.get();
    ChannelActivityEventLogResponse channelActivityEventLogResponse =
        getChannelActivityEventLogResponse(
            channelTransmissionRequest, serviceEvent, channelActivityLogResponse, channel);

    communicationChannel.sendAsync(
        channelTransmissionRequest,
        channelActivityEventLogResponse.getActivityEventLogId(),
        channel,
        serviceEvent);
    */
    
  }

  /**
   * Route a transmission request using async path.
   *
   * @param channelTransmissionRequest structure to contain messages and channels
   */
  @Override
  public void routeAsync(ChannelTransmissionRequest channelTransmissionRequest) {
	  
	 /* 
    ChannelActivityLogResponse channelActivityLogResponse =
        channelActivityRecordHelper.recordChannelActivity(
            channelTransmissionRequest, channelTransmissionRequest.getEvent());

    channelTransmissionRequest.getChannels().stream()
        .forEach(
            channel ->
                transmitMessageToChannelAsync(
                    channelTransmissionRequest,
                    channelTransmissionRequest.getEvent(),
                    channelActivityLogResponse,
                    channel));
    */
    
  }

  /**
   * Internal helper method to trigger channel activity record.
   *
   * @param channelTransmissionRequest actual transmission request
   * @param serviceEvent cyn event.
   * @param channelActivityLogResponse response returned by activity service.
   * @param channel channel instance
   * @return ChannelActivityEventLogResponse instance
   */
  /*private ChannelActivityEventLogResponse getChannelActivityEventLogResponse(
      ChannelTransmissionRequest channelTransmissionRequest,
      String serviceEvent,
      ChannelActivityLogResponse channelActivityLogResponse,
      Channel channel) {
    return channelActivityRecordHelper.recordChannelActivityEvent(
        channelActivityLogResponse.getActivityLogId(),
        channel.getChannelId(),
        channelTransmissionRequest.getCustomerUniqueIdentifier(),
        channelTransmissionRequest.getMessage(),
        channelTransmissionRequest.getSubject(),
        channelTransmissionRequest.getParams(),
        serviceEvent);
  }*/

  /**
   * Utility method to run pre-checks and sending message to channel
   *
   * @param channelTransmissionRequest the actual transmission request
   * @param serviceEvent the actual services event that triggered the communication
   * @param channelActivityLogResponse the activity log response for the parent activity in history
   *     service
   * @param channel channel to send the communication
   * @return channel receipt
   */
  private Optional<Tuple<Long, String>> transmitMessageToChannel(
      ChannelTransmissionRequest channelTransmissionRequest,
      String serviceEvent,
      Channel channel) {
	  
	  /*
    Optional<CommunicationChannel> channelOptional = getCommunicationChannel(channel);
    if (channelOptional.isEmpty()) {
      return Optional.empty();
    }
    CommunicationChannel communicationChannel = channelOptional.get();
    ChannelActivityEventLogResponse channelActivityEventLogResponse =
        getChannelActivityEventLogResponse(
            channelTransmissionRequest, serviceEvent, channelActivityLogResponse, channel);

    String channelResponse =
        communicationChannel.send(
            channelTransmissionRequest,
            channelActivityEventLogResponse.getActivityEventLogId(),
            channel,
            serviceEvent);
    return Optional.of(new Tuple<>(channel.getChannelId(), channelResponse));
    */
    return null;
  }

  /**
   * Filters the correct communication channel based on given information
   *
   * @param channel the channel definition to be used in filtering
   * @return the channel implementation.
   */
//  public Optional<CommunicationChannel> getCommunicationChannel(Channel channel) {
//    return communicationServiceFactory.getCommunicationChannel(channel);
//  }

  /**
   * Internal data structure to be used for collecting responses from channels
   *
   * @param <K> first element of tuple
   * @param <V> second element of tuple
   */
  @Data
  private static final class Tuple<K, V> {
    private final K key;
    private V value;

    public Tuple(K key, V value) {
      this.key = key;
      this.value = value;
    }
  }
}
