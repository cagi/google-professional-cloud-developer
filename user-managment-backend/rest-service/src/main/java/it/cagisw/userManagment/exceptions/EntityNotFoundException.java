package it.cagisw.userManagment.exceptions;

import java.util.List;

/** Exception to be thrown when an entity not found on db. */
public class EntityNotFoundException extends BaseServiceException {

  /** Serial version of the class. */
  private static final long serialVersionUID = 971954008276236160L;

  /**
   * Constructor for the exception.
   *
   * @param errorCode unique error code related to error.
   * @param apiSubErrorList list of sub errors if exist.
   */
  public EntityNotFoundException(String errorCode, List<SubError> apiSubErrorList) {
    super(errorCode, apiSubErrorList);
  }
}
