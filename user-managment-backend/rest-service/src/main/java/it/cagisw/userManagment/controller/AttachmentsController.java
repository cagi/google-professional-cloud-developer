package it.cagisw.userManagment.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.dto.AttachmentDTO;
import it.cagisw.userManagment.dto.AttachmentRequestDTO;
import it.cagisw.userManagment.service.interfaces.AttachmentsService;

/** Appointments attachment controller. */
@RequestMapping("/attachments")
@RestController
public class AttachmentsController {

  /** Auto injected AttachmentsService instance */
  @Autowired AttachmentsService service;

  /**
   * Creates an appointment's attachment
   *
   * @param customerUniqueId logged in user
   * @param appointmentId ID of the appointment
   * @param requestDTO uploaded file
   * @return created Appointment
   */
  @PostMapping("/appointments/{appointmentId}/{customerUniqueId}")
  public GenericResponse<AttachmentDTO> createNewAttachment(
		  @PathVariable("customerUniqueId") String customerUniqueId,
      @PathVariable("appointmentId") long appointmentId,
      @Valid @RequestBody AttachmentRequestDTO requestDTO) {
    return new GenericResponse<>(
        service.createNewAttachment(appointmentId, requestDTO, customerUniqueId));
  }

  /**
   * Get appointment attachment by its ID
   *
   * @param customerUniqueId id of the logged-in user
   * @param attachmentId ID of the rating
   * @return found appointment attachment
   */
  @GetMapping("/{attachmentId}/{customerUniqueId}")
  public GenericResponse<AttachmentDTO> getAttachmentById(
      @PathVariable("customerUniqueId") String customerUniqueId,
      @PathVariable("attachmentId") long attachmentId) {
    return new GenericResponse<>(service.findById(attachmentId, customerUniqueId));
  }

  /**
   * Delete a appointment's attachment by id
   *
   * @param customerUniqueId id of the logged-in user
   * @param attachmentId ID of the attachment to be deleted
   */
  @DeleteMapping("/{attachmentId}/{customerUniqueId}")
  public GenericResponse deleteAttachmentById(
      @PathVariable("customerUniqueId") String customerUniqueId,
      @PathVariable("attachmentId") long attachmentId) {
    service.deleteAttachment(attachmentId, customerUniqueId);
    return new GenericResponse();
  }
}
