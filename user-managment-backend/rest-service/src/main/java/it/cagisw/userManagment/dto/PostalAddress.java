package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** The postal address of a customer. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostalAddress {

  /** Address first line */
  @NotBlank private String address1;

  /** Address second line */
  private String address2;

  /** Address third line */
  private String address3;

  /** Post code */
  @NotBlank private String postCode;
}
