package it.cagisw.userManagment.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.dto.CanonicalIdMapIdentifierRequest;
import it.cagisw.userManagment.dto.CanonicalIdMapIdentifierResponse;
import it.cagisw.userManagment.dto.CanonicalIdMapRequest;
import it.cagisw.userManagment.dto.CanonicalIdMapResponse;
import it.cagisw.userManagment.dto.CustomerEvaluationRequest;
import it.cagisw.userManagment.dto.ProfileNameRequest;
import it.cagisw.userManagment.dto.ProfileNameResponse;
import it.cagisw.userManagment.dto.ProfileRequest;
import it.cagisw.userManagment.dto.ProfileResponse;
import it.cagisw.userManagment.dto.ProfileStatusRequest;
import it.cagisw.userManagment.dto.ProfileStatusResponse;
import it.cagisw.userManagment.dto.ValidationResponse;
import it.cagisw.userManagment.service.interfaces.CustomerProfileService;

/**
 * The entry point for customer profile HTTP requests.All the endpoints should decrypt the incoming
 * data. The registration and management of client keys are managed by the commons package.
 */
@RestController
public class CustomerProfileController {

  /** Service responsible for managing customer profiles */
  private final CustomerProfileService customerProfileService;

  /**
   * Create a new CustomerProfileController instance.
   *
   * @param customerProfileService service responsible for managing customer profiles
   */
  public CustomerProfileController(CustomerProfileService customerProfileService) {
    this.customerProfileService = customerProfileService;
  }

  /**
   * Retrieve canonical id map of customer.
   *
   * @param request payload
   * @return the canonical id mapping
   */
  @PostMapping("/canonicalmap")
  public GenericResponse<CanonicalIdMapResponse> canonicalMap(
      @Valid @RequestBody final CanonicalIdMapRequest request) {
    return new GenericResponse<>(customerProfileService.canonicalMap(request));
  }

  /**
   * Retrieve canonical id map of customer by identifier.
   *
   * @param request payload
   * @return the canonical id mapping
   */
  @PostMapping("/canonicalmap/identifier/retrieve")
  public GenericResponse<CanonicalIdMapIdentifierResponse> canonicalMap(
      @Valid @RequestBody final CanonicalIdMapIdentifierRequest request) {
    return new GenericResponse<>(customerProfileService.canonicalMapByIdentifier(request));
  }

  /**
   * Retrieve status of relationship for customer with the bank.
   *
   * @param request payload
   * @return the status of the relationship
   */
  @PostMapping("/relationship/status")
  public GenericResponse<ProfileStatusResponse> relationshipStatus(
      @Valid @RequestBody final ProfileStatusRequest request) {
    return new GenericResponse<>(customerProfileService.relationshipStatus(request));
  }

  /**
   * Retrieve the name of a given customer.
   *
   * @param request payload
   * @return the name details of the customer
   */
  @PostMapping("/name")
  public GenericResponse<ProfileNameResponse> name(
      @Valid @RequestBody final ProfileNameRequest request) {
    return new GenericResponse<>(customerProfileService.name(request));
  }

  /**
   * Validates for the existence of given customer profile details over the database.
   *
   * @param request payload
   * @return if the profile is valid
   */
  @PostMapping("/evaluate")
  public GenericResponse<ValidationResponse> evaluate(
      @Valid @RequestBody final CustomerEvaluationRequest request) {
    return new GenericResponse<>(customerProfileService.evaluate(request));
  }

  /**
   * Retrieve the profile of a given customer.
   *
   * @param request payload
   * @return the profile of the customer
   */
  @PostMapping("/retrieve")
  public GenericResponse<ProfileResponse> retrieve(
      @Valid @RequestBody final ProfileRequest request) {
    return new GenericResponse<>(customerProfileService.retrieve(request));
  }
}
