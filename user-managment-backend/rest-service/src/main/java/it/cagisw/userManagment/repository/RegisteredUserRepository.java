package it.cagisw.userManagment.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.cagisw.userManagment.models.RegisteredUser;
import it.cagisw.userManagment.models.RegistrationStatus;

/** The spring data repository for storing registered user entity */
@Repository
public interface RegisteredUserRepository extends CrudRepository<RegisteredUser, Long> {

  /**
   * Find a registered user by username
   *
   * @param userId unique user id.
   * @return Optional structure might contain RegisteredUser model.
   */
  List<RegisteredUser> findByUsername(String userId);

  /**
   * Find a registered user by username and registration status.
   *
   * @param userId unique user id.
   * @param registrationStatus status to filter.
   * @return Optional structure might contain RegisteredUser model.
   */
  List<RegisteredUser> findByUsernameAndRegistrationStatus(
      String userId, RegistrationStatus registrationStatus);

  /**
   * Find a registered user by username and registration status in list.
   *
   * @param userId unique user id.
   * @param registrationStatus status to filter.
   * @return Optional structure might contain RegisteredUser model.
   */
  List<RegisteredUser> findByUsernameAndRegistrationStatusIn(
      String userId, List<RegistrationStatus> registrationStatus);

  /**
   * Find a registered user by username and registration status.
   *
   * @param userId unique user id.
   * @param registrationStatus status to filter.
   * @param bindingId unique binding id.
   * @return Optional structure might contain RegisteredUser model.
   */
  List<RegisteredUser> findByUsernameAndRegistrationStatusAndBindingId(
      String userId, RegistrationStatus registrationStatus, String bindingId);

  /**
   * Find a registered user by username and registration status.
   *
   * @param registrationStatus status to filter.
   * @param deviceIdentifier unique device identifier.
   * @return Optional structure might contain RegisteredUser model.
   */
  List<RegisteredUser> findByRegistrationStatusAndDeviceIdentifier(
      RegistrationStatus registrationStatus, String deviceIdentifier);

  /**
   * Find a registered user by binding id
   *
   * @param bindingId unique binding id.
   * @return Optional structure might contain RegisteredUser model.
   */
  Optional<RegisteredUser> findByBindingId(String bindingId);

  /**
   * Find registered user with user id and binding id.
   *
   * @param userId user unique id.
   * @param bindingId unique binding id.
   * @return Registered User optional.
   */
  Optional<RegisteredUser> findByUsernameAndBindingId(String userId, String bindingId);

  /**
   * Find user registration with device os and device identifier.
   *
   * @param userId unique user id.
   * @param deviceOs unique device os.
   * @param deviceIdentifier unique device identifier.
   * @return Registered User optional.
   */
  List<RegisteredUser> findByUsernameAndDeviceOsAndDeviceIdentifier(
      String userId, String deviceOs, String deviceIdentifier);
}
