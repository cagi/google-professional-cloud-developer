package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Response structure for registration check operation. */
@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class CheckActivationStatusResponse {

  /** Status of the activation operation. */
  private ActivationStatus activationStatus;
}
