package it.cagisw.userManagment.models;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/** POJO representing a log history. */
@Table(name = "CUSTOMER_EVENT_LOG_HISTORY")
@Entity
@Data
public class CustomerEventLogHistory {

  /** Primary key */
  @Id
  @GeneratedValue
  @Column(name = "HISTORY_ID")
  private long historyId;

  /** Customer unique identifier */
  @Column(name = "CUSTOMER_UNIQUE_ID")
  private String customerUniqueId;

  /** Foreign key to event configuration of customer */
  @ManyToOne
  @JoinColumn(name = "EVENT_CONF_ID")
  private CustomerEventConfiguration eventConf;

  /** Foreign key to event group */
  @ManyToOne
  @JoinColumn(name = "EVENT_GROUP_ID")
  private EventGroup eventGroup;

  /** Name of the event group */
  @Column(name = "EVENT_GROUP")
  private String eventGroupName;

  /** Foreign key to event */
  @ManyToOne
  @JoinColumn(name = "EVENT_ID")
  private Event event;

  /** Name of the event */
  @Column(name = "EVENT_NAME")
  private String eventName;

  /** Foreign key to event group type */
  @ManyToOne
  @JoinColumn(name = "EVENT_GROUP_TYPE_ID")
  private EventGroupType eventGroupType;

  /** Name of the event group type */
  @Column(name = "EVENT_GROUP_TYPE")
  private String eventGroupTypeName;

  /** List of channels communicated */
  @Column(name = "CHANNELS")
  private String channels;

  /** Account number related to alert. */
  @Column(name = "ACCOUNT_NUMBER")
  private String accountNumber;

  /** Request type enumeration. */
  @Column(name = "REQUEST_TYPE")
  @Enumerated(EnumType.STRING)
  private RequestType requestType;

  /** Filtered flag. */
  @Column(name = "FILTERED")
  private boolean filtered;

  /** Short message. */
  @Lob
  @Column(name = "SHORT_MESSAGE")
  private String shortMessage;

  /** Created on audit field. */
  @Column(name = "CREATED_ON")
  private ZonedDateTime createdOn;
}
