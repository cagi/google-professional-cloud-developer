package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;

import it.cagisw.userManagment.models.ManagerSlotHasAppointments;

public interface RepositoryManagerSlotHasAppointments extends CrudRepository<ManagerSlotHasAppointments, Integer>{

}
