package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Reponse of a transaction using digipass device. */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DigiPassTransactionSigningResponse {

  /** Unique request id. */
  private String requestID;

  /** Request message. */
  private String requestMessage;

  /** Cronto code containing provisioning information. */
  private CrontoCode crontoCodeResponse;
}
