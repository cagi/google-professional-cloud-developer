package it.cagisw.userManagment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import it.cagisw.userManagment.models.ManagerSlot;

/** Repository for ManagerSlots table */
public interface ManagerSlotsRepository extends PagingAndSortingRepository<ManagerSlot, Long> {

  /**
   * Get all Manager slots
   *
   * @param managerId the manager id
   * @return found list of manager slots
   */
  List<ManagerSlot> findByManagerId(Long managerId);

  /**
   * Get all Manager slots
   *
   * @param managerId the manager email
   * @return found list of manager slots
   */
  List<ManagerSlot> findByManagerEmail(String managerEmail);

  /**
   * Find all overlapping slots for a manager for the given start and end time
   *
   * @param managerId the manager id
   * @param startTime start time to check
   * @param endTime end time to check
   * @return found list of slots overlapping with passed start and end time
   */
  @Query(
      "SELECT ms FROM ManagerSlot ms WHERE ms.managerId = :managerId "
          + "AND (:startTime BETWEEN ms.startTime AND ms.endTime OR :endTime BETWEEN ms.startTime AND ms.endTime)")
  List<ManagerSlot> findSlotsOverlappigWith(
      @Param("managerId") long managerId,
      @Param("startTime") int startTime,
      @Param("endTime") int endTime);
}
