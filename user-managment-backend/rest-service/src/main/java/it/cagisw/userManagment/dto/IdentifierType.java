package it.cagisw.userManagment.dto;

/** Types of identifiers. */
public enum IdentifierType {

  /** CIF id */
  CIFID,

  /** OLB user id */
  OLBUSERID,

  /** EQ customer number */
  EQCUSTOMERNO,

  /** Customer relationship agreement id */
  CUSTOMERRELATIONSHIPAGREEMENTID
}
