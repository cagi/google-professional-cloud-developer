package it.cagisw.userManagment.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/** POJO representing a channel. */
@Entity
@Table(name = "CHANNEL")
@Data
public class Channel {

  /** Primary Key */
  @Id
  @GeneratedValue
  @Column(name = "CHANNEL_ID")
  private long channelId;

  /** Name of the channel */
  @Column(name = "CHANNEL_NAME")
  private String channelName;

  /** Channel short code */
  @Column(name = "CHANNEL_SHORT_CODE")
  private String channelShortCode;

  /** Active flag of the channel */
  @Column(name = "ACTIVE")
  private boolean active;
}
