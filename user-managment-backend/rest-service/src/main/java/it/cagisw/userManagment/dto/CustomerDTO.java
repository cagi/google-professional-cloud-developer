package it.cagisw.userManagment.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/** DTO carrying details of an attachment create request */
@NoArgsConstructor
@Data
public class CustomerDTO {

  /** Title */
  private String title;

  /** First name */
  private String firstName;

  /** Last name */
  private String lastName;

  /** Middle name */
  private String middleName;
}
