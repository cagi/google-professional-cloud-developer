package it.cagisw.userManagment.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.cagisw.userManagment.models.Country;
import it.cagisw.userManagment.service.ServiceLayer;

@RestController
@RequestMapping("/methods")
public class DemoController {
	
	@Autowired
    private ServiceLayer service;

	@GetMapping("/shipState")
	public Map<String, String> getShipState() {
		Map<String, String> response = new HashMap<String, String>();
		Iterable<Country> all = service.getAll();
		response.put("etst", all.iterator().hasNext() + "");
		return response;
	}
}
