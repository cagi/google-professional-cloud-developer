package it.cagisw.userManagment.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.dto.ChannelTransmissionRequest;
import it.cagisw.userManagment.service.ChannelRouter;

/** The entry point for routing requests. */
@RestController
public class ChannelRouterController {

  /** Router interface */
  private final ChannelRouter channelRouter;

  /**
   * The constructor for controller to be used by Spring Boot
   *
   * @param channelRouter an implementation of router
   */
  public ChannelRouterController(ChannelRouter channelRouter) {
    this.channelRouter = channelRouter;
  }

  /**
   * The http entrypoint for router API
   *
   * @param requestData request contains message and channels
   * @param serviceEvent originator service event
   * @return the result for each channel
   */
  @PostMapping("/routeMessage")
  public GenericResponse<Map<Object, Object>> routeMessage(
      @Valid @RequestBody final ChannelTransmissionRequest requestData,
      @RequestHeader("x-cyn-event") String serviceEvent) {
    return new GenericResponse<>(channelRouter.route(requestData, serviceEvent));
  }
}
