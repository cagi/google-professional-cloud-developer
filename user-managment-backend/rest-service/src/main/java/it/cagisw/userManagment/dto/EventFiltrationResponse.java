package it.cagisw.userManagment.dto;

import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Event notification response structure. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventFiltrationResponse {

  /** Indicates if message is allowed or not. */
  @NotNull private boolean allowed;

  /** List of allowed channels. */
  @NotNull private List<Channel> allowedChannels;
}
