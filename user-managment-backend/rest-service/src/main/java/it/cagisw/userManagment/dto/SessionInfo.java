package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** SessionInfo structure for generic events. */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SessionInfo {

  /** Access token. */
  private String accessToken;

  /** Refresh token */
  private Object refreshToken;
}
