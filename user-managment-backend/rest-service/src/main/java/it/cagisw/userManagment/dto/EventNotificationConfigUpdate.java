package it.cagisw.userManagment.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** The input for updating customer's event preferences. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventNotificationConfigUpdate {

  /** Unique configuration id. */
  private long eventConfId;

  /** Numeric threshold value if event supports. */
  private Integer thresholdValue;

  /** The channels of event configured for customer. */
  private List<Channel> channelConfig;

  /** Activation status of the event configuration. */
  private Boolean active;
}
