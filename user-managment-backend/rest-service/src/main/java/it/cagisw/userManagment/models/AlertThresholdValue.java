package it.cagisw.userManagment.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

/** Class for representing user threshold preference. */
@Data
@Entity
@Table(name = "ALERT_THRESHOLD_VALUE")
public class AlertThresholdValue {

  /** Primary key */
  @Id
  @GeneratedValue
  @Column(name = "ALERT_THRESHOLD_VALUE_ID")
  private long thresholdId;

  /** Foreign key to CustomerEventConfiguration entity. */
  @OneToOne
  @JoinColumn(name = "ALERT_CONF_ID")
  private EventMap eventMap;

  /** Threshold value of the event */
  @Column(name = "VALUE")
  private long thresholdValue;
}
