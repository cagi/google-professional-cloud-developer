package it.cagisw.userManagment.utils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import it.cagisw.userManagment.dto.GenericEvent;

/**
 * Event listener to process application GenericEvents. This listener publishes events into
 * pre-defined message queue for further processing.
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class GenericEventListener {

  /** GCP Pub/Sub template. */
  // private final PubSubTemplate pubSubTemplate;

  /** Topic id. */
  @Value("${generic.event.topic:CYN_MICROSERVICE_EVENT_FOR_OPERATIONAL_INSIGHT}")
  private String genericEventTopic;

  /**
   * Publish a message to generic event topic.
   *
   * @param message payload for a migrated user
   */
  private void publishEvent(GenericEvent message) {
    try {
      // pubSubTemplate.publish(genericEventTopic, asyncMessageAESUtils.encryptMessage(message));
    } catch (Exception e) {
      log.error("Couldn't publish generic event.", e);
    }
  }

  /**
   * Listen spring events for GenericEvents.
   *
   * @param event GenericEvent instance published by services.
   */
  @EventListener
  @Async
  public void listenEvent(GenericEvent event) {
    publishEvent(event);
  }
}
