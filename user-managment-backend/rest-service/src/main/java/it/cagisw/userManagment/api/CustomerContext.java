package it.cagisw.userManagment.api;

/** The customer's view context. */
public enum CustomerContext {
  /** Personal */
  PERSONAL,

  /** Business */
  BUSINESS
}
