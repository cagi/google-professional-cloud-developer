package it.cagisw.userManagment.service;

import static com.cyn.ccds.customer.profile.server.exception.ErrorCode.CPR_ERR000010;

import com.cyn.ccds.customer.data.mgmt.client.CustomerAgreementResponse;
import com.cyn.ccds.customer.profile.server.service.CustomerDataService;
import com.cyn.commons.api.GenericResponse;
import com.cyn.commons.exception.BaseServiceException;
import com.cyn.commons.property.CustomerDataMgmtProperties;
import com.cyn.commons.util.RemoteInvocationContext;
import com.cyn.commons.util.RemoteInvocationService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collections;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

/** Service to perform operations against customer data management server. */
@Service
public class CustomerDataServiceImpl implements CustomerDataService {

  /** Remote invocation service */
  private final RemoteInvocationService remoteInvocationService;

  /** Customer data management properties */
  private final CustomerDataMgmtProperties customerDataMgmtProperties;

  /** Jackson object mapper */
  private final ObjectMapper objectMapper;

  /**
   * Create new CustomerDataServiceImpl instance.
   *
   * @param remoteInvocationService Remote invocation service
   * @param customerDataMgmtProperties Customer data management properties
   * @param objectMapper Jackson object mapper
   */
  public CustomerDataServiceImpl(
      final RemoteInvocationService remoteInvocationService,
      final CustomerDataMgmtProperties customerDataMgmtProperties,
      final ObjectMapper objectMapper) {
    this.remoteInvocationService = remoteInvocationService;
    this.customerDataMgmtProperties = customerDataMgmtProperties;
    this.objectMapper = objectMapper;
  }

  /**
   * Get customer agreement from customer data management server
   *
   * @param agreementId agreement id
   * @return customer agreement response
   */
  private CustomerAgreementResponse getCustomerAgreement(int agreementId) {
    try {
      var response =
          remoteInvocationService.executeRemoteService(
              RemoteInvocationContext.builder()
                  .httpMethod(HttpMethod.GET)
                  .url(
                      UriComponentsBuilder.fromHttpUrl(
                              customerDataMgmtProperties.getCustomerAgreementUrl())
                          .queryParam(
                              customerDataMgmtProperties.getCustomerAgreementPathVariable(),
                              agreementId)
                          .toUriString())
                  .includeAccessToken(true)
                  .returnType(Object.class)
                  .build());
      return objectMapper
          .convertValue(
              response.getBody(),
              new TypeReference<GenericResponse<CustomerAgreementResponse>>() {})
          .getBody();
    } catch (Exception e) {
      throw new BaseServiceException(CPR_ERR000010.name(), Collections.emptyList(), e);
    }
  }

  /**
   * Determine if the relationship is active
   *
   * @param customerRelationshipAgreementId agreement id
   * @return true if it is active
   */
  @Override
  public boolean isCustomerRelationshipActive(int customerRelationshipAgreementId) {
    var customerAgreement = getCustomerAgreement(customerRelationshipAgreementId);
    return customerAgreement.getStatus();
  }
}
