package it.cagisw.userManagment.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/** Class for representing modality map. */
@Entity
@Table(name = "EVENT_DEFAULT")
@Getter
@Setter
@IdClass(EventDefaultId.class)
public class EventDefault {

  /** Primary Key */
  @Id
  @GeneratedValue
  @Column(name = "EVENT_DEFAULT_ID")
  private long eventDefaultId;

  /** Reference to event. */
  @Id
  @ManyToOne
  @JoinColumn(name = "EVENT_ID")
  private Event event;

  /** Event param value */
  @Column(name = "EVENT_PARAM_VALUE")
  private String eventParamValue;

  /** Event param value type */
  @Column(name = "EVENT_PARAM")
  @Enumerated(EnumType.STRING)
  private EventDefaultParam eventParam;
}
