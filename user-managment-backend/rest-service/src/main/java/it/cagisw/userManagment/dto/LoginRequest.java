package it.cagisw.userManagment.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Login request structure for onespan login call. */
@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class LoginRequest {

  /** Type of login request. */
  private String loginType;

  /** Login challenge. */
  private String challenge;

  /** Object type of login request. */
  private String objectType;

  /** Device Identifier. */
  private String deviceIdentifier;

  /** Delivery method of orchestration. */
  private List<String> orchestrationDelivery;

  /** Timeout for the login request. */
  private int timeout;

  /** Credentials structure for login operation. */
  private Credentials credentials;
}
