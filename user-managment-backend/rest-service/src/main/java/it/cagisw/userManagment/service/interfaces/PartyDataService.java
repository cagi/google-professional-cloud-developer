package it.cagisw.userManagment.service.interfaces;

import it.cagisw.userManagment.models.CustomerProfile;

/** Service to perform operations with party data management component service. */
public interface PartyDataService {

  /**
   * Retrieve the CustomerProfile entity by calling the different endpoints needed from party data
   * management component service.
   *
   * @param customerUniqueIdentifier unique identifier of customer
   * @return the CustomerProfile entity
   */
  CustomerProfile getCustomerProfileEntity(String customerUniqueIdentifier);
}
