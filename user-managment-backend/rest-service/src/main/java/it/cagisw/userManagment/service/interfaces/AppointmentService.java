package it.cagisw.userManagment.service.interfaces;

import it.cagisw.userManagment.dto.AppointmentCancelRequestDTO;
import it.cagisw.userManagment.dto.AppointmentDTO;
import it.cagisw.userManagment.dto.AppointmentRequestDTO;
import it.cagisw.userManagment.dto.PaginatedList;

/** Service interface for Appointment related operations */
public interface AppointmentService {

  /**
   * Find all appointments for the given user
   *
   * @param customerUniqueId customerUniqueId name
   * @return Found list of user appointments
   */
  PaginatedList findAll(String customerUniqueId, int skip, int limit);

  /**
   * Create a new appointment
   *
   * @param request DTO appointment details to be created
   * @param customerUniqueId logged in user name
   * @return ID of the created user appointment
   */
  AppointmentDTO createAppointment(AppointmentRequestDTO request, String customerUniqueId);

  /**
   * Find an appointment by its row ID
   *
   * @param id ID for which manager rating is to be found
   * @param customerUniqueId logged in user name
   * @return Found appointment
   */
  AppointmentDTO findById(Long id, String customerUniqueId);

  /**
   * Update an appointment
   *
   * @param id ID of the row that needs to be updated
   * @param request DTO carrying updated appointment details
   * @param customerUniqueId logged in user name
   * @return updated appointment
   */
  AppointmentDTO updateAppointment(Long id, AppointmentRequestDTO request, String customerUniqueId);

  /**
   * Canel an appointment
   *
   * @param id ID of the row that needs to be updated
   * @param request DTO carrying updated appointment details
   * @param customerUniqueId logged in user name
   * @return updated appointment
   */
  AppointmentDTO cancelAppointment(
      Long id, AppointmentCancelRequestDTO request, String customerUniqueId);

  /**
   * Accept new date in Appointment
   *
   * @param id ID for which manager slot is to be deleted
   * @param customerUniqueId logged in user name
   */
  AppointmentDTO acceptNewAppointmentDate(Long id, String customerUniqueId);

  /**
   * Delete an appointment by its row ID
   *
   * @param id ID for which manager slot is to be deleted
   * @param customerUniqueId logged in user name
   */
  void deleteAppointment(Long id, String customerUniqueId);
}
