package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Device details for session validation. */
@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class DeviceDetails {

  /** Device unique id. */
  @NotBlank private String deviceIdentifier;

  /** Unique registration binding id. */
  @NotBlank private String bindingId;

  /** Device operating system. */
  @NotBlank private String deviceOs;
}
