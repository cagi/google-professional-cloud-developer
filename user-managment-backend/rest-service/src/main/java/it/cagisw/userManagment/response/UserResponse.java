package it.cagisw.userManagment.response;

import it.cagisw.userManagment.dto.UserDTO;

public class UserResponse {

	private UserDTO user;

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}
}
