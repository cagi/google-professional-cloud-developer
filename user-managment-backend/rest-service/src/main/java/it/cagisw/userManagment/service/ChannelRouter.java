package it.cagisw.userManagment.service;

import java.util.Map;

import it.cagisw.userManagment.dto.ChannelTransmissionRequest;

/** Router interface for routing messages into different channels */
public interface ChannelRouter {

  /**
   * Route given message to given channels
   *
   * @param channelTransmissionRequest structure to contain messages and channels
   * @param serviceEvent incoming cyn event for routing
   * @return the result for each channel requested
   */
  Map<Object, Object> route(
      ChannelTransmissionRequest channelTransmissionRequest, String serviceEvent);

  /**
   * Route given message to given channels via messaging topics.
   *
   * @param channelTransmissionRequest structure to contain messages and channels
   * @return the result for each channel requested
   */
  void routeAsync(ChannelTransmissionRequest channelTransmissionRequest);
}
