package it.cagisw.userManagment.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.api.ResponseCode;
import it.cagisw.userManagment.api.Status;
import it.cagisw.userManagment.dto.UserDTO;
import it.cagisw.userManagment.requests.ListUserRequest;
import it.cagisw.userManagment.requests.UserRequest;
import it.cagisw.userManagment.response.UserResponse;
import it.cagisw.userManagment.service.ServiceLayer;
import it.cagisw.userManagment.service.UserRegistrationService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	private static Logger log = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private ServiceLayer service;
	
	@Autowired
	private UserRegistrationService userRegistrationService;

	@PostMapping("/")
	public Map<String,Object> createUser(@RequestBody UserRequest userRequest) {
		log.debug("[createUser]");
		Map<String,Object> result = new HashMap<String,Object>();
		log.debug("Create User", userRequest);
		int id = service.createUser(userRequest);
		result.put("user_id", id);
		return result;
	}
	
	@GetMapping("/getAllUsers/{name}")
	public List<UserDTO> getAllUsers(@PathVariable(value = "name") String name) {
		log.debug("[getAllUsers]");
		List<UserDTO> allUsers = userRegistrationService.getAllUsers(name);
		return allUsers;
	}

	@GetMapping("/getUser/{id}")
	public UserResponse getUser(@PathVariable(value = "id") String id) {
		log.debug("[getUser]");
		UserResponse resp = new UserResponse();
		UserDTO userDTO = userRegistrationService.getUser(Integer.parseInt(id));
		resp.setUser(userDTO);
		return resp;
	}

	@DeleteMapping("/getUser/{id}")
	public void deleteUser(@PathVariable(value = "id") String id) {
		log.debug("[deleteUser]");
		service.deleteUser(Integer.parseInt(id));
	}
	
	@PostMapping("/createUsers")
	public GenericResponse<Object> createUsers(@RequestBody ListUserRequest listUserRequest) {
		log.debug("[createUsers]");
		List<UserDTO> createUsers = userRegistrationService.createUsers(listUserRequest);
		GenericResponse<Object> genericResponse = new GenericResponse<Object>(createUsers);
		Status status = new Status();
		status.setCode(ResponseCode.SUCCESS);
		genericResponse.setStatus(status);
		return genericResponse;
	}
	
	@GetMapping("/createUsersDemo")
	public ListUserRequest createUsersDemo() {
		log.debug("[createUsersDemo]");
		ListUserRequest result = new ListUserRequest();
		List<UserRequest> listUser = new ArrayList<UserRequest>();
		UserRequest user1 = new UserRequest();
		user1.setFullName("name");
		listUser.add(user1);
		result.setUserRequests(listUser );
		return result;
	}

	@PutMapping("/")
	public GenericResponse<Object> updateUser(@RequestBody UserRequest userRequest) {
		log.debug("[updateUser]");
		UserDTO updateUser = userRegistrationService.updateUser(userRequest);
		GenericResponse<Object> genericResponse = new GenericResponse<Object>(updateUser);
		Status status = new Status();
		status.setCode(ResponseCode.SUCCESS);
		genericResponse.setStatus(status);
		return genericResponse;
	}

	public void importData() {

	}
}
