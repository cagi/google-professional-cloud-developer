package it.cagisw.userManagment.dto;

/** transaction MFA type */
public enum TMfaType {

  /** push */
  T_PUSH,

  /** otp */
  T_OTP,
}
