package it.cagisw.userManagment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.cagisw.userManagment.models.CustomerAccount;

/** Repository for storing EventConfigAccountIdMaps. */
public interface CustomerAccountRepository extends JpaRepository<CustomerAccount, Long> {

  /**
   * Retrieve all customer account configurations.
   *
   * @param accountNumber account number.
   * @return list of customer accounts.
   */
  List<CustomerAccount> findAllByAccountNumber(String accountNumber);
}
