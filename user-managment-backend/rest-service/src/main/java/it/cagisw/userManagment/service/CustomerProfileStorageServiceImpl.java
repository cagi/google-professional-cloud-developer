package it.cagisw.userManagment.service;

import com.cyn.ccds.customer.profile.server.model.CustomerProfile;
import com.cyn.ccds.customer.profile.server.service.CacheService;
import com.cyn.ccds.customer.profile.server.service.CustomerProfileStorageService;
import com.cyn.ccds.customer.profile.server.service.PartyDataService;
import java.util.Optional;
import org.springframework.stereotype.Service;

/** Service to perform storage operations for the profile of a customer. */
@Service
public class CustomerProfileStorageServiceImpl implements CustomerProfileStorageService {

  private final CacheService cacheService;

  /** Service to perform operations with party data component service */
  private final PartyDataService partyDataService;

  /**
   * Create new CustomerProfileStorageServiceImpl instance.
   *
   * @param cacheService Service to perform operations with cache server
   * @param partyDataService Service to perform operations with party data component cacheService
   */
  public CustomerProfileStorageServiceImpl(
      final CacheService cacheService, final PartyDataService partyDataService) {
    this.cacheService = cacheService;
    this.partyDataService = partyDataService;
  }

  /**
   * Retrieve the CustomerProfile entity from DB if not available will fetch from party data
   * component service, sensitive data will be encrypted for storage and decrypted at retrieval.
   *
   * @param customerUniqueIdentifier the unique identifier for a customer
   * @return the CustomerProfile entity
   */
  @Override
  public CustomerProfile getCustomerProfile(String customerUniqueIdentifier) {
    Optional<CustomerProfile> optional = cacheService.getCustomerProfile(customerUniqueIdentifier);
    if (optional.isPresent()) {
      return optional.get();
    }
    var customerProfile = partyDataService.getCustomerProfileEntity(customerUniqueIdentifier);

    // TODO use customer data to retrieve relationship

    customerProfile.setPartyRelationshipStatus(true);

    try {
      cacheService.saveCustomerProfile(customerProfile);
    } catch (Exception e) {
      // Service can operate without cache server
    }
    return customerProfile;
  }

  /**
   * Retrieve latest information from party data component service, encrypts sensitive data before
   * storing to DB.
   *
   * @param customerUniqueIdentifier the unique identifier for a customer
   */
  @Override
  public void createOrUpdateCustomerProfile(String customerUniqueIdentifier) {
    var customerProfile = partyDataService.getCustomerProfileEntity(customerUniqueIdentifier);

    // TODO use customer data to retrieve relationship

    customerProfile.setPartyRelationshipStatus(true);

    cacheService.saveCustomerProfile(customerProfile);
  }

  /**
   * Updates the relationship of the customer with the bank.
   *
   * @param customerUniqueIdentifier the unique identifier for a customer
   * @param state state to be saved for this customer relationship
   */
  @Override
  public void updateCustomerProfileRelationshipState(
      String customerUniqueIdentifier, boolean state) {
    cacheService
        .getCustomerProfile(customerUniqueIdentifier)
        .ifPresentOrElse(
            c -> {
              c.setPartyRelationshipStatus(state);
              cacheService.saveCustomerProfile(c);
            },
            () -> createOrUpdateCustomerProfile(customerUniqueIdentifier));
  }
}
