package it.cagisw.userManagment.dto;

/** Enumeration to represent type of biometrics that can be registered. */
public enum BiometricType {

  /** Fingerprints */
  FINGERPRINT,

  /** Face Id */
  FACEID,

  /** Face Id */
  NULL,
}
