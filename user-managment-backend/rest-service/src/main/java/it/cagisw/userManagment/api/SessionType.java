package it.cagisw.userManagment.api;

/** User session type */
public enum SessionType {

  /** Web session */
  WEB,

  /** Mobile session */
  MOBILE
}
