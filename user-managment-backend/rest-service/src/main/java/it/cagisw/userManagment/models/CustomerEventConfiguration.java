package it.cagisw.userManagment.models;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/** POJO representing event configuration for a specific customer. */
@Data
@Entity
@Table(name = "CUSTOMER_EVENT_CONFIGURATIONS")
public class CustomerEventConfiguration {

  /** Primary key */
  @Id
  @GeneratedValue
  @Column(name = "EVENT_CONF_ID")
  private long eventConfId;

  /** Customer unique identifier */
  @Column(name = "CUSTOMER_UNIQUE_ID")
  private String customerUniqueId;

  /** Created on audit field */
  @Column(name = "CREATED_ON")
  private ZonedDateTime createdOn;

  /** Updated on audit field */
  @Column(name = "UPDATED_ON")
  private ZonedDateTime updatedOn;
}
