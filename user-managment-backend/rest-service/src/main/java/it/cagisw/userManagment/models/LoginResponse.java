package it.cagisw.userManagment.models;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/** A Pojo representing a login response for a user. */
@Entity
@Data
@Table(name = "LOGIN_RESPONSE")
public class LoginResponse {

  /** Unique key. */
  @Column(name = "ID")
  @Id
  @GeneratedValue
  private long id;

  /** Login request reference. */
  @ManyToOne
  @JoinColumn(name = "LOGIN_ID")
  private LoginRequest loginRequest;

  /** Http response for login operation. */
  @Column(name = "HTTP_CODE")
  private String httpCode;

  /** The challenge presented. */
  @Column(name = "CHALLENGE")
  private String challenge;

  /** Request message detail. */
  @Column(name = "REQUEST_MESSAGE")
  @Lob
  private String requestMessage;

  /** Exception in json format when issue occurs. */
  @Column(name = "EXCEPTION_JSON")
  private String exceptionJson;

  /** Unique request id for the login. */
  @Column(name = "REQUESTUUID")
  private String requestUuid;

  /** Created on audit field. */
  @Column(name = "CREATED_ON")
  private ZonedDateTime createdOn;
}
