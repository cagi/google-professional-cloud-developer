package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Manager rating statistics DTO */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ManagerRatingStatisticsDTO {

  /** Rating Manager Id */
  private long managerId;

  /** average star rating */
  private float averageRating;

  /** total count of ratings found */
  private int totalRatingCount;

  /** ratings */
  @Builder.Default private int[] ratings = new int[5];
}
