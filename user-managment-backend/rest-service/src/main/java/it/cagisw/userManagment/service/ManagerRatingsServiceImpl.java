package it.cagisw.userManagment.service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.cagisw.userManagment.commons.exception.BaseBadRequestException;
import com.cagisw.userManagment.commons.exception.BaseNotFoundException;
import com.cagisw.userManagment.commons.exception.ErrorCode;

import it.cagisw.userManagment.dto.ManagerRatingDTO;
import it.cagisw.userManagment.dto.ManagerRatingStatisticsDTO;
import it.cagisw.userManagment.dto.PaginatedList;
import it.cagisw.userManagment.models.ManagerRatings;
import it.cagisw.userManagment.repository.ManagerRatingsRepository;
import it.cagisw.userManagment.service.interfaces.ManagerRatingsService;
import it.cagisw.userManagment.service.interfaces.UserDetailInvoker;
import lombok.extern.slf4j.Slf4j;

/** Implementation class for {@link ManagerRatingsService} */
@Slf4j
@Service
public class ManagerRatingsServiceImpl implements ManagerRatingsService {

  /** Auto injected ManagerRatingsRepository instance */
  @Autowired private ManagerRatingsRepository ratingsRepository;

  @Autowired private UserDetailInvoker userDetailInvoker;

  /**
   * Find all manager ratings for the given manager (optional)
   *
   * @param customerUniqueId logged in user id
   * @param rating
   * @param sortOrder
   * @return Found list of manager ratings
   */
  @Override
  public PaginatedList findAll(
      String customerUniqueId, int skip, int limit, List<Integer> rating, String sortOrder) {
    Sort order = Sort.by(ManagerRatings.DEFAULT_SORT_COLUMN);
    if (sortOrder != null && sortOrder.toLowerCase().equals("oldest")) order = order.ascending();
    else order = order.descending();
    Pageable pageRequest = PageRequest.of(skip / limit, limit, order);
    List<Integer> ratingList = validateAndCreateRatingsFilter(rating);
    long managerId = getManagerId(customerUniqueId);

    Page<ManagerRatings> results = null;
    if (ratingList.isEmpty()) results = ratingsRepository.findByManagerId(managerId, pageRequest);
    else results = ratingsRepository.findByManagerIdAndRatingIn(managerId, pageRequest, ratingList);
    Page<ManagerRatingDTO> pageDtos = results.map(rt -> getRatingsDto(rt));
    return new PaginatedList(pageDtos.getTotalElements(), pageDtos.getContent());
  }

  /**
   * validate ratings query and convert them to 1-10 based ratings
   *
   * @param rating
   * @return ratingsFilter to query db
   */
  private List<Integer> validateAndCreateRatingsFilter(List<Integer> rating) {
    // no rating filter applied
    if (rating == null || rating.isEmpty()) return new ArrayList<Integer>();
    List<Integer> distinctRatings = rating.stream().distinct().collect(Collectors.toList());
    if (distinctRatings.size() < rating.size()
        || Collections.min(distinctRatings) < 1
        || Collections.max(distinctRatings) > 5) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000028.name(), Collections.emptyList());
    }
    List<Integer> ratingList = new ArrayList<Integer>();
    // converting 1-5 based rating to 1-10 based rating for filtering from database
    for (int ratingFilter : distinctRatings) {
      ratingList.add(ratingFilter * 2 - 1);
      ratingList.add(ratingFilter * 2);
    }
    return ratingList;
  }

  /**
   * Create a new manager rating
   *
   * @param ratingDTO DTO carrying manager rating details to be created
   * @param customerUniqueId logged in user id
   * @return ID of the created manager rating
   */
  @Override
  public ManagerRatingDTO createManagerRating(ManagerRatingDTO ratingDTO, String customerUniqueId) {
    validateRating(ratingDTO);
    long managerId = getManagerId(customerUniqueId);
    if (!ratingsRepository
        .findByCustomerUniqueIdAndManagerId(customerUniqueId, managerId)
        .isEmpty()) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000027.name(), Collections.emptyList());
    }

    ManagerRatings ratings =
        ManagerRatings.builder()
            .managerId(managerId)
            .rating(ratingDTO.getRating())
            .feedback(ratingDTO.getFeedback())
            .customerUniqueId(customerUniqueId)
            .createdDate(ZonedDateTime.now())
            .build();
    ratingsRepository.save(ratings);
    return getRatingsDto(ratings);
  }

  /**
   * Get rating statistics : star rating with count of each star, average rating
   *
   * @param customerUniqueId logged in user id
   * @return statistics of manager rating
   */
  @Override
  public ManagerRatingStatisticsDTO getStatistics(String customerUniqueId) {
    long managerId = getManagerId(customerUniqueId);
    List<ManagerRatings> managerRatings = ratingsRepository.findAllByManagerId(managerId);
    if (managerRatings.isEmpty()) {
      throw new BaseNotFoundException(ErrorCode.RMA_ERR000024.name(), Collections.emptyList());
    }
    ManagerRatingStatisticsDTO managerRatingStatisticsDTO =
        ManagerRatingStatisticsDTO.builder()
            .managerId(managerId)
            .totalRatingCount(managerRatings.size())
            .build();

    long totalRating = 0;

    for (ManagerRatings managerRating : managerRatings) {
      int rating = (managerRating.getRating() + 1) / 2;
      managerRatingStatisticsDTO.getRatings()[rating - 1]++;
      totalRating += rating;
    }
    managerRatingStatisticsDTO.setAverageRating(
        Math.round(totalRating * 100f / managerRatings.size()) / 100f);
    return managerRatingStatisticsDTO;
  }

  /**
   * Find a manager rating by its row ID
   *
   * @param id ID for which manager rating is to be found
   * @return Found manager rating
   */
  @Override
  public ManagerRatingDTO findById(Long id, String customerUniqueId) {
    ManagerRatings managerRating = getManagerRating(id);
    return getRatingsDto(managerRating);
  }

  /**
   * Update manager rating
   *
   * @param id ID of the row that needs to be updated
   * @param ratingDTO DTO carrying updated manager rating details
   * @param customerUniqueId logged in user id
   * @return flag to indicate whether row was updated or not
   */
  @Override
  public ManagerRatingDTO updateManagerRating(
      Long id, ManagerRatingDTO ratingDTO, String customerUniqueId) {
    validateRating(ratingDTO);
    ManagerRatings managerRating = getManagerRating(id);
    checkRatingOwnership(managerRating, customerUniqueId);
    managerRating.setRating(ratingDTO.getRating());
    managerRating.setFeedback(ratingDTO.getFeedback());
    ratingsRepository.save(managerRating);
    return getRatingsDto(managerRating);
  }

  /**
   * Delete a manager rating by its row ID
   *
   * @param id ID for which manager slot is to be deleted
   * @param customerUniqueId logged in user id
   */
  @Override
  public void deleteManagerRating(Long id, String customerUniqueId) {
    ManagerRatings managerRating = getManagerRating(id);
    checkRatingOwnership(managerRating, customerUniqueId);
    ratingsRepository.delete(managerRating);
  }

  /**
   * Convert ManagerRatings to ManagerRatingDTO
   *
   * @param rt the db entity
   * @return the DTO
   */
  private ManagerRatingDTO getRatingsDto(ManagerRatings rt) {
    return ManagerRatingDTO.builder()
        .id(rt.getId())
        .managerId(rt.getManagerId())
        .rating(rt.getRating())
        .feedback(rt.getFeedback())
        .createdDate(rt.getCreatedDate())
        .customerUniqueId(rt.getCustomerUniqueId())
        .build();
  }

  /**
   * Find ManagerRatings by Id, and throw exception if not found
   *
   * @param id ID of the row to be found
   * @return Found ManagerRatings
   */
  private ManagerRatings getManagerRating(Long id) {
    Optional<ManagerRatings> found = ratingsRepository.findById(id);

    if (!found.isPresent()) {
      throw new BaseNotFoundException(ErrorCode.RMA_ERR000004.name(), Collections.emptyList());
    }
    return found.get();
  }

  /**
   * Validate if logged in user has ownership on the manager rating. Throw exception otherwise
   *
   * @param managerRating the manager rating entity
   * @param customerUniqueId the logged in user
   */
  private void checkRatingOwnership(ManagerRatings managerRating, String customerUniqueId) {
    if (!managerRating.getCustomerUniqueId().equalsIgnoreCase(customerUniqueId)) {
      throw new BaseNotFoundException(ErrorCode.RMA_ERR000005.name(), Collections.emptyList());
    }
  }

  /**
   * validate rating is between 1 and 10
   *
   * @param ratingDTO the rating request dto
   */
  private void validateRating(ManagerRatingDTO ratingDTO) {
    if (ratingDTO.getRating() < 1 || ratingDTO.getRating() > 10)
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000025.name(), Collections.emptyList());
  }

  /**
   * Get manager Id.
   *
   * @return the manager ID
   */
  private long getManagerId(String customerUniqueId) {
    try {
      return userDetailInvoker.getManager(customerUniqueId).getId();
    } catch (Exception e) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000023.name(), Collections.emptyList());
    }
  }
}
