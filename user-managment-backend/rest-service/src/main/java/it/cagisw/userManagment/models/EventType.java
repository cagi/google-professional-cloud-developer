package it.cagisw.userManagment.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/** Class for representing event type master record. */
@Data
@Entity
@Table(name = "EVENT_TYPE")
public class EventType {

  /** Primary key */
  @Id
  @GeneratedValue
  @Column(name = "EVENT_TYPE_ID")
  private long eventTypeId;

  /** Name of the event type */
  @Column(name = "EVENT_TYPE")
  private String eventTypeName;

  /** Description of the event type */
  @Column(name = "DESCRIPTION")
  private String description;
}
