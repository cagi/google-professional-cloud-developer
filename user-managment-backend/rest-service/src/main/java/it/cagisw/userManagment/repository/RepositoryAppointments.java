package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;

import it.cagisw.userManagment.models.Appointments;

public interface RepositoryAppointments extends CrudRepository<Appointments, Integer>{

}
