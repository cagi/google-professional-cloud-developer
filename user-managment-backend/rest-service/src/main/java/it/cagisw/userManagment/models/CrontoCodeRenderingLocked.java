package it.cagisw.userManagment.models;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/** A Pojo representing a locked cronto code refresh. */
@Entity
@Data
@Table(name = "CRONTO_CODE_RENDERING_LOCKED")
public class CrontoCodeRenderingLocked {

  /** Unique key of the entity */
  @Id
  @GeneratedValue
  @Column(name = "ID")
  private long id;

  /** Customer unique identifier that is banned from rendering */
  @Column(name = "CUSTOMERUNIQUEIDENTIFIER")
  private String customerUniqueIdentifier;

  /** Flag to indicate if rendering is locked */
  @Column(name = "LOCKED")
  private boolean locked;

  /** Unlocked by audit field. */
  @Column(name = "UNLOCKED_BY")
  private String unlockedBy;

  /** Created on audit field. */
  @Column(name = "CREATED_ON")
  private ZonedDateTime createdOn;

  /** Updated on audit field. */
  @Column(name = "UPDATED_ON")
  private ZonedDateTime updatedOn;
}
