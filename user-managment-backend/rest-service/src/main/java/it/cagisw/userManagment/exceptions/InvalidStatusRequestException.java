package it.cagisw.userManagment.exceptions;

import java.util.List;

/** Exception to be thrown when change request is invalid. */
public class InvalidStatusRequestException extends BaseServiceException {

  /** Serial version of the class. */
  private static final long serialVersionUID = 6482215877045777088L;

  /**
   * Constructor for the exception.
   *
   * @param errorCode unique error code related to error.
   * @param apiSubErrorList list of sub errors if exist.
   */
  public InvalidStatusRequestException(String errorCode, List<SubError> apiSubErrorList) {
    super(errorCode, apiSubErrorList);
  }
}
