package it.cagisw.userManagment.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Customer name entity. */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerNameEntity {

  /** Title of customer's name */
  private String title;

  /** First name */
  private String firstName;

  /** Last name */
  private String lastName;

  /** Middle name */
  private String middleName;
}
