package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Definition of a filter. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Filter {
  /** The operator of the filter. */
  private CriteriaEnum filterCriteriaEnum;

  /** Parameter of the filter. */
  private String param;

  /** Value of the filter. */
  private String value;
}
