package it.cagisw.userManagment.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

/** Location phone address. */
@Data
@Jacksonized
@SuperBuilder
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class LocationPhoneAddress extends BaseLocation implements LocationEntity {

  /** Location address */
  @NotNull @Valid private PhoneAddress locationAddress;
}
