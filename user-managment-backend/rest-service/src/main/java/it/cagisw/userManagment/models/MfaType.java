package it.cagisw.userManagment.models;

/** Types of multifactorial authentication that user can enable. */
public enum MfaType {

  /** Login using pincode */
  PIN,

  /** Login using fingerprint. */
  FINGERPRINT,

  /** Login using faceId */
  FACEID
}
