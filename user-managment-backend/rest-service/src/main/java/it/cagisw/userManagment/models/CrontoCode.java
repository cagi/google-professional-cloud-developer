package it.cagisw.userManagment.models;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/** A Pojo representing a cronto code. */
@Entity
@Data
@Table(name = "CRONTO_CODES")
public class CrontoCode {

  /** Unique key */
  @Id
  @GeneratedValue
  @Column(name = "ID")
  private long id;

  /** Reference to CrontoRenderRequest */
  @ManyToOne
  @JoinColumn(name = "CRONTO_REQUEST_ID")
  private CrontoRenderRequest crontoRenderRequest;

  /** Hex message that is put into cronto */
  @Column(name = "HEXMESSAGE")
  @Lob
  private String hexMessage;

  /** Format of the rendered image */
  @Column(name = "FORMAT")
  private String format;

  /** Size of the rendered image */
  @Column(name = "SIZE")
  private long size;

  /** Created time */
  @Column(name = "CREATED_ON")
  private ZonedDateTime createdOn;

  /** Updated time */
  @Column(name = "UPDATED_ON")
  private ZonedDateTime updatedOn;
}
