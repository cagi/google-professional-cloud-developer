package it.cagisw.userManagment.service;

import lombok.AllArgsConstructor;
import lombok.Getter;

/** Enum with indices for map arrays. */
@Getter
@AllArgsConstructor
public enum MapArrayIndices {

  /** Index of the key in a pair */
  KEY_INDEX(0),

  /** Index of the value in a pair */
  VALUE_INDEX(1);

  /** The value of this enum */
  private final int value;
}
