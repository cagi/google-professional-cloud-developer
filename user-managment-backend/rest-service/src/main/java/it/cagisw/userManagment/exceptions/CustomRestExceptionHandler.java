package it.cagisw.userManagment.exceptions;

import java.util.ResourceBundle;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.api.ResponseCode;
import it.cagisw.userManagment.api.Status;
import static it.cagisw.userManagment.exceptions.ExceptionConstants.BUNDLE_NAME;

@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler{

	/** Error message for unknown exceptions * */
	  private static final String UNKNOWN_ERROR = "UNKNOWN_ERROR";

	  /** Content type for client error messages * */
	  private static final String APPLICATION_JSON_CONTENT = "application/json";

	  /**
	   * Handle base service exception and return 500. Read messages from message bundle
	   *
	   * @param ex exception thrown
	   * @return response entity to be send to caller
	   */
	  @ExceptionHandler({BaseServiceException.class})
	  public ResponseEntity<Object> handleServiceException(BaseServiceException ex) {
	    ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);
	    
	    GenericResponse<Object> errorResponse = new GenericResponse<>();
	    
	    
//	    String exceptionMessage =
//	        rb.containsKey(ex.getErrorMessage())
//	            ? rb.getString(ex.getErrorMessage())
//	            : ex.getErrorMessage();
	    
	    
//	    errorResponse.setStatus(
//	        Status.builder()
//	            .code(ResponseCode.FAILURE)
//	            .errorCode(ex.getErrorMessage())
//	            .message(exceptionMessage)
//	            .internalMessage(exceptionMessage)
//	            .build());
	    
	    
	    return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	  }

	  /**
	   * Handle failure exception and return 200. Read messages from message bundle.
	   *
	   * @param ex exception thrown
	   * @return response entity to be send to caller
	   */
	  @ExceptionHandler({BaseFailureStatusException.class})
	  public ResponseEntity<Object> handleStatusFailureException(BaseServiceException ex) {
	    ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);
	    String exceptionMessage =
	        rb.containsKey(ex.getErrorMessage())
	            ? rb.getString(ex.getErrorMessage())
	            : ex.getErrorMessage();
	    
	    GenericResponse<Object> errorResponse = new GenericResponse<>();
	    
	    
//	    errorResponse.setStatus(
//	        Status.builder()
//	            .code(ResponseCode.SUCCESS)
//	            .errorCode(ex.getErrorMessage())
//	            .message(exceptionMessage)
//	            .internalMessage(exceptionMessage)
//	            .build());
	    
	    return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.OK);
	  }

	  /**
	   * Handle bad request exception and return 400. Read messages from message bundle.
	   *
	   * @param ex exception thrown
	   * @return response entity to be send to caller
	   */
	  @ExceptionHandler({BaseBadRequestException.class})
	  public ResponseEntity<Object> handleBadRequestException(BaseBadRequestException ex) {
	    ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);
	    String exceptionMessage =
	        rb.containsKey(ex.getErrorMessage())
	            ? rb.getString(ex.getErrorMessage())
	            : ex.getErrorMessage();
	    
	    GenericResponse<Object> errorResponse = new GenericResponse<>();
	    
	    
//	    errorResponse.setStatus(
//	        Status.builder()
//	            .code(ResponseCode.FAILURE)
//	            .errorCode(ex.getErrorMessage())
//	            .message(exceptionMessage)
//	            .internalMessage(exceptionMessage)
//	            .build());
	    
	    
	    return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	  }

	  /**
	   * Handle unauthorized exception and return 403. Read messages from message bundle.
	   *
	   * @param ex exception thrown
	   * @return response entity to be send to caller
	   */
	  @ExceptionHandler({BaseUnauthorizedException.class})
	  public ResponseEntity<Object> handleUnAuthorizedException(BaseUnauthorizedException ex) {
	    ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);
	    String exceptionMessage =
	        rb.containsKey(ex.getErrorMessage())
	            ? rb.getString(ex.getErrorMessage())
	            : ex.getErrorMessage();
	    
	    GenericResponse<Object> errorResponse = new GenericResponse<>();
	    
	    
//	    errorResponse.setStatus(
//	        Status.builder()
//	            .code(ResponseCode.FAILURE)
//	            .errorCode(ex.getErrorMessage())
//	            .message(exceptionMessage)
//	            .internalMessage(exceptionMessage)
//	            .build());
	    return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.FORBIDDEN);
	  }

	  /**
	   * Handle unauthenticated exception and return 401. Read messages from message bundle.
	   *
	   * @param ex exception thrown
	   * @return response entity to be send to caller
	   */
	  @ExceptionHandler({BaseUnauthenticatedException.class})
	  public ResponseEntity<Object> handleUnAuthenticatedException(BaseUnauthenticatedException ex) {
	    ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);
	    String exceptionMessage =
	        rb.containsKey(ex.getErrorMessage())
	            ? rb.getString(ex.getErrorMessage())
	            : ex.getErrorMessage();
	    GenericResponse<Object> errorResponse = new GenericResponse<>();
	    
//	    errorResponse.setStatus(
//	        Status.builder()
//	            .code(ResponseCode.FAILURE)
//	            .errorCode(ex.getErrorMessage())
//	            .message(exceptionMessage)
//	            .internalMessage(exceptionMessage)
//	            .build());
	    
	    return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
	  }

	  /**
	   * Handle http client error and pass error as is to upstream.
	   *
	   * @param ex exception thrown
	   * @return response entity to be send to caller
	   */
	  @ExceptionHandler({HttpClientErrorException.class})
	  public ResponseEntity<Object> handleHttpClientException(HttpClientErrorException ex) {
	    HttpHeaders headers = new HttpHeaders();
	    headers.add(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_CONTENT);
	    return new ResponseEntity<>(
	        ex.getResponseBodyAsString(), headers, HttpStatus.valueOf(ex.getRawStatusCode()));
	  }

	  /**
	   * Handle http server error and pass error as is to upstream.
	   *
	   * @param ex exception thrown
	   * @return response entity to be send to caller
	   */
	  @ExceptionHandler({HttpServerErrorException.class})
	  public ResponseEntity<Object> handleHttpClientException(HttpServerErrorException ex) {
	    HttpHeaders headers = new HttpHeaders();
	    headers.add(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_CONTENT);
	    return new ResponseEntity<>(
	        ex.getResponseBodyAsString(), headers, HttpStatus.valueOf(ex.getRawStatusCode()));
	  }

	  /**
	   * Handle not found error and return 404.
	   *
	   * @param ex exception thrown
	   * @return response entity to be send to caller
	   */
	  @ExceptionHandler({BaseNotFoundException.class})
	  public ResponseEntity<Object> handleNotFoundException(BaseNotFoundException ex) {
	    ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);
	    String exceptionMessage =
	        rb.containsKey(ex.getErrorMessage())
	            ? rb.getString(ex.getErrorMessage())
	            : ex.getErrorMessage();
	    GenericResponse<Object> errorResponse = new GenericResponse<>();
	    
	    /*errorResponse.setStatus(
	        Status.builder()
	            .code(ResponseCode.FAILURE)
	            .errorCode(ex.getErrorMessage())
	            .message(exceptionMessage)
	            .internalMessage(exceptionMessage)
	            .build());*/
	    return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.NOT_FOUND);
	  }

	  /**
	   * Handle general exceptions that are not caught by services.
	   *
	   * @param ex exception thrown
	   * @return response entity to be send to caller
	   */
	  @ExceptionHandler({Exception.class})
	  public ResponseEntity<Object> handleUnknownError(Exception ex) {
		  
	    GenericResponse<Object> errorResponse = new GenericResponse<>();
	    
	    /*errorResponse.setStatus(
	        Status.builder()
	            .code(ResponseCode.FAILURE)
	            .errorCode(ex.getMessage())
	            .message(UNKNOWN_ERROR)
	            .internalMessage(ex.getMessage())
	            .build());*/
	    
	    
	    return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	  }

}
