package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;

import it.cagisw.userManagment.models.Role;

public interface RoleRepository extends CrudRepository<Role, Integer>{

}
