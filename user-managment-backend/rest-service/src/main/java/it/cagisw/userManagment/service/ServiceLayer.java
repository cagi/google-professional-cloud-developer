package it.cagisw.userManagment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.cagisw.userManagment.models.Country;
import it.cagisw.userManagment.models.User;
import it.cagisw.userManagment.repository.CountryRepository;
import it.cagisw.userManagment.repository.UserRepository;
import it.cagisw.userManagment.requests.UserRequest;

@Service
public class ServiceLayer {

    @Autowired
    private CountryRepository rep;
    
    @Autowired
    private UserRepository userRep;

    @Transactional(readOnly = true)
    public Iterable<Country> getAll() {
        return rep.findAll();
    }
    
    @Transactional
    public int createUser(UserRequest req) {
    	User entity = new User();
    	entity.setFullName(req.getFullName());
    	entity.setEmailAddress(req.getEmailAddress());
    	entity.setUsername(req.getUsername());
    	
		User save = userRep.save(entity);
		int id = save.getId();
		return id;
    }

    @Transactional
    public void deleteUser(int id) {
    	userRep.deleteById(Long.parseLong("" + id));
    }
    
    

}