package it.cagisw.userManagment.service;

import static com.cyn.ccds.onespan.adapter.server.exception.ErrorCode.ONE_ERR000015;
import static com.cyn.ccds.onespan.adapter.server.exception.ErrorCode.ONE_ERR000016;
import static com.cyn.ccds.onespan.adapter.server.exception.ErrorCode.ONE_ERR000017;

import com.cyn.ccds.customer.profile.client.ProfileRequest;
import com.cyn.ccds.customer.profile.client.ProfileResponse;
import com.cyn.ccds.customer.profile.client.location.ContactPoint;
import com.cyn.ccds.customer.profile.client.location.LocationElectronicAddress;
import com.cyn.ccds.customer.profile.client.location.LocationPhoneAddress;
import com.cyn.ccds.event.channel.router.client.Channel;
import com.cyn.ccds.event.channel.router.client.ChannelTransmissionRequest;
import com.cyn.ccds.event.channel.router.client.Param;
import com.cyn.ccds.event.subs.filter.client.EventNotification;
import com.cyn.ccds.event.subs.filter.client.EventNotificationResponse;
import com.cyn.ccds.onespan.adapter.server.service.ChannelTransmitter;
import com.cyn.commons.api.GenericResponse;
import com.cyn.commons.exception.BaseServiceException;
import com.cyn.commons.property.ChannelRouterProperties;
import com.cyn.commons.property.CustomerProfileProperties;
import com.cyn.commons.property.EventFiltrationProperties;
import com.cyn.commons.util.JsonUtils;
import com.cyn.commons.util.RemoteInvocationContext;
import com.cyn.commons.util.RemoteInvocationService;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/** Cyn router service of message transmitter . */
@Service
public class ChannelTransmitterRouterService implements ChannelTransmitter {

  /** Firstname param. */
  private static final String FIRST_NAME = "firstName";

  /** The SMS channel. */
  private static final String TEXT_MESSAGE_CHANNEL = "TEXT MESSAGE";

  /** Regex for removing mobile number leading zeros. . */
  private static final String MOBILE_NUMBER_REMOVE_LEADING_ZEROS_REGEX = "^0+(?!$)";

  /** The email channel. */
  private static final String EMAIL_CHANNEL = "EMAIL";

  /** Represents an empty string. . */
  private static final String EMPTY_STRING = "";

  /** Return type of customer profile service. . */
  private static final TypeReference<GenericResponse<ProfileResponse>> GENERIC_PROFILE_TYPE =
      new TypeReference<>() {};

  /** Return type of customer profile service. . */
  private static final TypeReference<GenericResponse<EventNotificationResponse>>
      GENERIC_EVENT_RESPONSE_TYPE = new TypeReference<>() {};

  /** Mobile number format. . */
  private final String mobileNumberFormat;

  /** Remove Invocation Helper. . */
  private final RemoteInvocationService remoteInvocationService;

  /** Customer profile server properties */
  private final CustomerProfileProperties customerProfileProperties;

  /** Channel router server properties */
  private final ChannelRouterProperties channelRouterProperties;

  /** Event filtration server properties */
  private final EventFiltrationProperties eventFiltrationProperties;

  /**
   * Constructor to be called by Spring Boot.
   *
   * @param mobileNumberFormat Mobile number format
   * @param remoteInvocationService Remove Invocation Helper.
   * @param customerProfileProperties Customer profile server properties
   * @param channelRouterProperties Channel router server properties
   * @param eventFiltrationProperties Event filtration server properties
   */
  public ChannelTransmitterRouterService(
      @Value("${mobile.number.format}") final String mobileNumberFormat,
      RemoteInvocationService remoteInvocationService,
      final CustomerProfileProperties customerProfileProperties,
      final ChannelRouterProperties channelRouterProperties,
      final EventFiltrationProperties eventFiltrationProperties) {
    this.mobileNumberFormat = mobileNumberFormat;
    this.remoteInvocationService = remoteInvocationService;
    this.customerProfileProperties = customerProfileProperties;
    this.channelRouterProperties = channelRouterProperties;
    this.eventFiltrationProperties = eventFiltrationProperties;
  }

  /**
   * Transmit a given security message via router service.
   *
   * @param message the actual transmit request.
   * @param event event that is causing communication.
   * @param subject subject of the message.
   * @return channel communication value phone number or email masked.
   */
  @Override
  public void transmitRequestForSecurityEvent(
      String message, String customerId, String event, String subject) {
    EventNotification eventNotification =
        EventNotification.builder().customerUniqueIdentifier(customerId).event(event).build();

    EventNotificationResponse eventNotificationResponse =
        getAvailableChannels(eventNotification, event);

    if (eventNotificationResponse.getEventFiltrationResponse().getAllowedChannels() == null) {
      throw new BaseServiceException(ONE_ERR000015.toString());
    }

    ProfileResponse customerDetails = getCustomerDetails(customerId);
    var mobileAddressOptional =
        customerDetails.getPartyLocations().stream()
            .filter(pl -> pl.getPartyLocationType() == ContactPoint.MOBILE_ADDRESS)
            .map(pl -> (LocationPhoneAddress) pl.getLocation())
            .findFirst();
    var emailAddressOptional =
        customerDetails.getPartyLocations().stream()
            .filter(pl -> pl.getPartyLocationType() == ContactPoint.ELECTRONIC_ADDRESS)
            .map(pl -> (LocationElectronicAddress) pl.getLocation())
            .findFirst();
    List<ChannelTransmissionRequest> transmissionRequests =
        eventNotificationResponse.getEventFiltrationResponse().getAllowedChannels().stream()
            .map(
                channel -> {
                  if (channel.getChannelName().equals(TEXT_MESSAGE_CHANNEL)
                      && isValidMobileNumber(mobileAddressOptional)) {
                    var mobileAddress = mobileAddressOptional.get().getLocationAddress();
                    return Channel.builder()
                        .channelId(channel.getChannelId())
                        .channelName(channel.getChannelName())
                        .contactInfo(
                            getFormattedNumber(
                                mobileAddress.getCountryCode(),
                                mobileAddress.getAreaCode(),
                                mobileAddress.getPhoneNumber()))
                        .build();
                  } else if (channel.getChannelName().equals(EMAIL_CHANNEL)
                      && isValidEmailAddress(emailAddressOptional)) {
                    var emailAddress = emailAddressOptional.get().getLocationAddress();
                    return Channel.builder()
                        .channelId(channel.getChannelId())
                        .channelName(channel.getChannelName())
                        .contactInfo(emailAddress.getAddress())
                        .build();
                  } else {
                    return null;
                  }
                })
            .filter(Objects::nonNull)
            .map(
                channel ->
                    getChannelEventTransmissionRequest(
                        customerId,
                        List.of(channel),
                        message,
                        Map.of(FIRST_NAME, customerDetails.getCustomerName().getFirstName()),
                        event,
                        subject))
            .collect(Collectors.toList());
    if (transmissionRequests.isEmpty()) {
      throw new BaseServiceException(ONE_ERR000016.name(), Collections.emptyList());
    }
    for (ChannelTransmissionRequest transmissionRequest : transmissionRequests) {
      try {
        sendChannelTransmissionRequest(transmissionRequest, event);
      } catch (Exception e) {
        throw new BaseServiceException(ONE_ERR000017.name(), Collections.emptyList(), e);
      }
    }
  }

  /**
   * Validate if LocationPhoneAddress optional has a valid phone number.
   *
   * @param mobileAddressOptional location phone address optional
   * @return true if is valid
   */
  private boolean isValidMobileNumber(final Optional<LocationPhoneAddress> mobileAddressOptional) {
    if (mobileAddressOptional.isEmpty()) {
      return false;
    }
    if (mobileAddressOptional.get().getLocationAddress() == null) {
      return false;
    }
    var mobileAddress = mobileAddressOptional.get().getLocationAddress();
    return isNotNullOrEmpty(mobileAddress.getCountryCode())
        && isNotNullOrEmpty(mobileAddress.getAreaCode())
        && isNotNullOrEmpty(mobileAddress.getPhoneNumber());
  }

  /**
   * Validate if LocationElectronicAddress has a valid email.
   *
   * @param emailAddressOptional location electronic address optional
   * @return true if valid
   */
  private boolean isValidEmailAddress(
      final Optional<LocationElectronicAddress> emailAddressOptional) {
    if (emailAddressOptional.isEmpty()) {
      return false;
    }
    if (emailAddressOptional.get().getLocationAddress() == null) {
      return false;
    }
    var emailAddress = emailAddressOptional.get().getLocationAddress();
    return isNotNullOrEmpty(emailAddress.getAddress());
  }

  /**
   * Get the mobile number formatted according to configurable format and remove leading zeros from
   * number.
   *
   * @param countryCode country code of number
   * @param areaCode area code
   * @param mobileNumber mobile number
   * @return formatted number
   */
  private String getFormattedNumber(
      final String countryCode, final String areaCode, final String mobileNumber) {
    return String.format(
        mobileNumberFormat,
        countryCode,
        areaCode.replaceFirst(MOBILE_NUMBER_REMOVE_LEADING_ZEROS_REGEX, EMPTY_STRING),
        mobileNumber);
  }

  /**
   * Check that a string is not null or empty.
   *
   * @param s string to check
   * @return true if not null or empty
   */
  private boolean isNotNullOrEmpty(String s) {
    return s != null && !s.isBlank();
  }

  /**
   * Get customer details by customer id.
   *
   * @param customerId the customer id.
   * @return the profile response.
   */
  private ProfileResponse getCustomerDetails(String customerId) {
    ProfileRequest customerProfileRetrieveRequest = new ProfileRequest();
    customerProfileRetrieveRequest.setCustomerUniqueIdentifier(customerId);
    ResponseEntity<Object> responseEntity =
        remoteInvocationService.executeRemoteService(
            RemoteInvocationContext.builder()
                .returnType(Object.class)
                .requestBody(customerProfileRetrieveRequest)
                .httpMethod(HttpMethod.POST)
                .url(customerProfileProperties.getRetrieveUri())
                .cynEvent(customerProfileProperties.getRetrieveCynEvent())
                .includeAccessToken(true)
                .build());

    return JsonUtils.OBJECT_MAPPER
        .convertValue(responseEntity.getBody(), GENERIC_PROFILE_TYPE)
        .getBody();
  }

  /**
   * Get available channels.
   *
   * @param eventNotification the event notification.
   * @return the event notification response.
   */
  private EventNotificationResponse getAvailableChannels(
      EventNotification eventNotification, String event) {
    ResponseEntity<Object> responseEntity =
        remoteInvocationService.executeRemoteService(
            RemoteInvocationContext.builder()
                .returnType(Object.class)
                .requestBody(eventNotification)
                .httpMethod(HttpMethod.POST)
                .cynEvent(event)
                .url(eventFiltrationProperties.getSubscriptionEvaluateUri())
                .includeAccessToken(true)
                .build());
    return JsonUtils.OBJECT_MAPPER
        .convertValue(responseEntity.getBody(), GENERIC_EVENT_RESPONSE_TYPE)
        .getBody();
  }

  /**
   * Send the channelTransmissionRequest to router service.
   *
   * @param channelTransmissionRequest request to be sent to router service
   * @param event event of the channel communication.
   * @return the status of the request. .
   */
  private boolean sendChannelTransmissionRequest(
      ChannelTransmissionRequest channelTransmissionRequest, String event) {
    remoteInvocationService.executeRemoteService(
        RemoteInvocationContext.builder()
            .returnType(Object.class)
            .requestBody(channelTransmissionRequest)
            .httpMethod(HttpMethod.POST)
            .url(channelRouterProperties.getRouteUri())
            .includeAccessToken(true)
            .cynEvent(event)
            .build());
    return true;
  }

  /**
   * Generate a ChannelTransmissionRequest pojo for given communication details.
   *
   * @param customerId the customer to communicate.
   * @param channels list of channels to communicate.
   * @param message the message content.
   * @return the constructed request.
   */
  private ChannelTransmissionRequest getChannelEventTransmissionRequest(
      String customerId,
      List<Channel> channels,
      String message,
      Map<String, String> params,
      String event,
      String subject) {
    return ChannelTransmissionRequest.builder()
        .customerUniqueIdentifier(customerId)
        .message(message)
        .event(event)
        .params(Collections.emptyList())
        .params(
            params.entrySet().stream()
                .map(
                    entry ->
                        Param.builder()
                            .paramName(entry.getKey())
                            .paramValue(entry.getValue())
                            .build())
                .collect(Collectors.toList()))
        .templatized(true)
        .subject(subject)
        .channels(channels)
        .build();
  }
}
