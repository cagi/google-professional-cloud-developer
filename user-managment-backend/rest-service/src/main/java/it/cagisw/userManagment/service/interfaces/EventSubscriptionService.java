package it.cagisw.userManagment.service.interfaces;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import it.cagisw.userManagment.dto.Status;
import it.cagisw.userManagment.dto.CaptureNotification;
import it.cagisw.userManagment.dto.ConfigViewRequest;
import it.cagisw.userManagment.dto.ConfigureUser;
import it.cagisw.userManagment.dto.CustomerEventHistory;
import it.cagisw.userManagment.dto.EventNotification;
import it.cagisw.userManagment.dto.EventNotificationConfigUpdate;
import it.cagisw.userManagment.dto.EventNotificationResponse;
import it.cagisw.userManagment.dto.Pagination;

/** The main login that performs event subscription related logic.. */
public interface EventSubscriptionService {

  /**
   * Return history records based on given pagination info.
   *
   * @param requestData the pagination data
   * @return response containing the history records.
   */
  List<CustomerEventHistory> historyData(Pagination requestData, String valueOf);

  /**
   * Return the number of history records based on given pagination info.
   *
   * @param requestData the pagination data
   * @return response containing the count.
   */
  int historyCount(Pagination requestData, String valueOf);

  /**
   * Return the default available events.
   *
   * @return all of the events in the master tables.
   */
  Map<Object, Object> viewEvents(String customerId, ConfigViewRequest requestData);

  /**
   * Update the event and channel preferences of the user.
   *
   * @return empty generic response.
   */
  Status updateEvent(EventNotificationConfigUpdate requestData);

  /**
   * Configure events for a new user. This will copy all the master data from master source and move
   * them to corresponding place
   *
   * @param configureUser the unique customer id
   * @return empty generic response.
   */
  Status configureEvents(ConfigureUser configureUser);

  /**
   * Validates the given event type.
   *
   * @param requestData RequestData the evaluate request
   * @return return operation result success or fail..
   */
  EventNotificationResponse evaluate(@Valid EventNotification requestData);

  /**
   * Capture a notification that has been sent to customer.
   *
   * @param requestData structure containing notification details.
   * @return status of the capture operation..
   */
  Status capture(CaptureNotification requestData);
}
