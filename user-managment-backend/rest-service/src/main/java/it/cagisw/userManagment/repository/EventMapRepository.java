package it.cagisw.userManagment.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.cagisw.userManagment.dto.CustomerEventConfiguration;
import it.cagisw.userManagment.dto.Event;
import it.cagisw.userManagment.models.EventMap;

/** Repository for storing EventDefault. */
public interface EventMapRepository extends JpaRepository<EventMap, Long> {

  /**
   * Retrieve the default configuration of an event
   *
   * @param event event to search.
   * @return optional might contain EventDefault model.
   */
  List<EventMap> findByEvent(Event event);

  /**
   * Retrieve the default configuration of an event
   *
   * @param customerEventConfiguration eventConfig to search.
   * @return optional might contain EventMap model.
   */
  Optional<EventMap> findByEventConf(CustomerEventConfiguration customerEventConfiguration);
}
