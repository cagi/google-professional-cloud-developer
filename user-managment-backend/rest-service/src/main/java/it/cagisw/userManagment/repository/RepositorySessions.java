package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;

import it.cagisw.userManagment.models.Sessions;

public interface RepositorySessions extends CrudRepository<Sessions, Integer>{

}
