package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;

import it.cagisw.userManagment.models.ManagerSlot;

public interface RepositoryManagerSlot extends CrudRepository<ManagerSlot, Integer>{

}
