package it.cagisw.userManagment.service.interfaces;

import java.util.List;

import it.cagisw.userManagment.dto.ManagerRatingDTO;
import it.cagisw.userManagment.dto.ManagerRatingStatisticsDTO;
import it.cagisw.userManagment.dto.PaginatedList;

/** Service interface for the manager ratings related operations */
public interface ManagerRatingsService {

  /**
   * Find all manager ratings for the given manager (optional)
   *
   * @return Found list of manager ratings
   */
  PaginatedList findAll(
      String customerUniqueId, int skip, int limit, List<Integer> rating, String sortOrder);

  /**
   * Create a new manager rating
   *
   * @param ratingDTO DTO carrying manager rating details to be created
   * @return ID of the created manager rating
   */
  ManagerRatingDTO createManagerRating(ManagerRatingDTO ratingDTO, String customerUniqueId);

  /**
   * Find a manager rating by its row ID
   *
   * @param id ID for which manager rating is to be found
   * @param customerUniqueId logged in user id
   * @return Found manager rating
   */
  ManagerRatingDTO findById(Long id, String customerUniqueId);

  /**
   * Update manager rating
   *
   * @param id ID of the row that needs to be updated
   * @param ratingDTO DTO carrying updated manager rating details
   * @param customerUniqueId logged in user id
   * @return flag to indicate whether row was updated or not
   */
  ManagerRatingDTO updateManagerRating(
      Long id, ManagerRatingDTO ratingDTO, String customerUniqueId);

  /**
   * Delete a manager rating by its row ID
   *
   * @param id ID for which manager slot is to be deleted
   * @param customerUniqueId logged in user id
   */
  void deleteManagerRating(Long id, String customerUniqueId);

  /**
   * Get rating statistics : star rating with count of each star, average rating
   *
   * @param customerUniqueId unique identifier of requesting user
   * @return statistics of manager rating
   */
  ManagerRatingStatisticsDTO getStatistics(String customerUniqueId);
}
