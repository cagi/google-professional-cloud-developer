package it.cagisw.userManagment.service.interfaces;

import java.util.List;
import java.util.Optional;

import it.cagisw.userManagment.dto.ManagerSlotDTO;
import it.cagisw.userManagment.dto.ManagerSlotRequestDTO;
import it.cagisw.userManagment.dto.SlotAvailabilityDTO;

/** Service interface for the manager slot related operations */
public interface ManagerSlotService {

  /**
   * Find all manager slots for the given manager (optional)
   *
   * @param managerId manager id (optional)
   * @return Found manager slots wrapped in PaginatedList
   */
  List<ManagerSlotDTO> findAll(Optional<Long> managerId);

  /**
   * Create a new manager slot
   *
   * @param slotDTO DTO carrying manager slot details to be created
   * @return ID of the created manager slot
   */
  ManagerSlotDTO createManagerSlot(ManagerSlotRequestDTO slotDTO);

  /**
   * Find a manager slot by its row ID
   *
   * @param id ID for which manager slot is to be found
   * @return Found manager slot
   */
  ManagerSlotDTO findById(Long id);

  /**
   * Update manager slot
   *
   * @param id ID of the row that needs to be updated
   * @param slotDTO DTO carrying updated manager slot details
   * @return flag to indicate whether row was updated or not
   */
  ManagerSlotDTO updateManagerSlot(Long id, ManagerSlotRequestDTO slotDTO);

  /**
   * Delete a manager slot by its row ID
   *
   * @param id ID for which manager slot is to be deleted
   */
  void deleteManagerSlot(Long id);

  /**
   * @param managerId id of manager whose availability need to be found
   * @param managerEmail email of manager whose availability need to be found
   * @param day day for which the availability need to be found
   * @return found slots availability
   */
  SlotAvailabilityDTO findSlotsAvailability(Long managerId, String managerEmail, String day);
}
