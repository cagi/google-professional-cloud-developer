package it.cagisw.userManagment.service.interfaces;

import it.cagisw.userManagment.dto.CanonicalIdMapIdentifierRequest;
import it.cagisw.userManagment.dto.CanonicalIdMapIdentifierResponse;
import it.cagisw.userManagment.dto.CanonicalIdMapRequest;
import it.cagisw.userManagment.dto.CanonicalIdMapResponse;
import it.cagisw.userManagment.dto.CustomerEvaluationRequest;
import it.cagisw.userManagment.dto.ProfileNameRequest;
import it.cagisw.userManagment.dto.ProfileNameResponse;
import it.cagisw.userManagment.dto.ProfileRequest;
import it.cagisw.userManagment.dto.ProfileResponse;
import it.cagisw.userManagment.dto.ProfileStatusRequest;
import it.cagisw.userManagment.dto.ProfileStatusResponse;
import it.cagisw.userManagment.dto.ValidationResponse;

/**
 * This service is responsible for managing customer profiles and performs validation against
 * existing records.
 */
public interface CustomerProfileService {

  /**
   * Retrieve canonical id map of customer.
   *
   * @param request payload
   * @return the canonical id mapping
   */
  CanonicalIdMapResponse canonicalMap(CanonicalIdMapRequest request);

  /**
   * Retrieve canonical id map of customer by identifier.
   *
   * @param request payload
   * @return the canonical id mapping
   */
  CanonicalIdMapIdentifierResponse canonicalMapByIdentifier(
      CanonicalIdMapIdentifierRequest request);

  /**
   * Retrieve status of relationship for customer with the bank.
   *
   * @param request payload
   * @return the status of the relationship
   */
  ProfileStatusResponse relationshipStatus(ProfileStatusRequest request);

  /**
   * Retrieve the name of a given customer.
   *
   * @param request payload
   * @return DTO with name details
   */
  ProfileNameResponse name(ProfileNameRequest request);

  /**
   * Validates for the existence of given customer profile details over the database.
   *
   * @param request payload with data to evaluate customer
   * @return the SUCCESS or FAIL response. Returns success if there is a match.
   */
  ValidationResponse evaluate(CustomerEvaluationRequest request);

  /**
   * Retrieve the profile of a given customer.
   *
   * @param request payload
   * @return profile of customer
   */
  ProfileResponse retrieve(ProfileRequest request);
}
