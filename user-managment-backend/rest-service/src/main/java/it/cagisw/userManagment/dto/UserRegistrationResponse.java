package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Response structure for a successful user registration request. */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRegistrationResponse {

  /** Activation password returned by onespan. */
  private String activationPassword;

  /** Serial number returned by onespan. */
  private String serialNumber;

  /** Binding id for device registration. */
  private String bindingId;

  /** Cronto image structure. */
  private CrontoCode crontoCodeResponse;
}
