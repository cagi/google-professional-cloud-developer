package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Name details of the customer. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerName {

  /** Title */
  private String title;

  /** First name */
  private String firstName;

  /** Last name */
  private String lastName;

  /** Middle name */
  private String middleName;
}
