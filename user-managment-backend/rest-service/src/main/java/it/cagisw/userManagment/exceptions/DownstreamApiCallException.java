package it.cagisw.userManagment.exceptions;

import java.util.List;

/** Exception to be thrown when downstream api responds error. */
public class DownstreamApiCallException extends BaseServiceException {

  /** Serial version of the class. */
  private static final long serialVersionUID = 2413099751218813702L;

  /**
   * Constructor for the exception.
   *
   * @param errorCode unique error code related to error.
   * @param apiSubErrorList list of sub errors if exist.
   */
  public DownstreamApiCallException(String errorCode, List<SubError> apiSubErrorList) {
    super(errorCode, apiSubErrorList);
  }
}
