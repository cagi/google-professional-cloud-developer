package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;

import it.cagisw.userManagment.models.Managers;

public interface RepositoryManagers extends CrudRepository<Managers, Integer>{

}
