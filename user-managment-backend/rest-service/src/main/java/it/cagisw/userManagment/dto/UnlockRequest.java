package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Unlock request structure for a onespan user. */
@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class UnlockRequest {

  /** Type of login request. */
  private String userId;
}
