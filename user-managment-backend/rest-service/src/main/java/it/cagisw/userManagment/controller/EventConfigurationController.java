package it.cagisw.userManagment.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.api.Status;
import it.cagisw.userManagment.dto.ConfigViewRequest;
import it.cagisw.userManagment.dto.ConfigureUser;
import it.cagisw.userManagment.dto.CustomerEventHistory;
import it.cagisw.userManagment.dto.EventNotificationConfigUpdate;
import it.cagisw.userManagment.dto.Pagination;
import it.cagisw.userManagment.service.interfaces.EventSubscriptionService;

/** The entry point for event subscription requests.. */
@RestController
public class EventConfigurationController {

  /** Event subscription service to perform business logic.. */
  private final EventSubscriptionService eventSubscriptionService;

  /**
   * Constructor for event configuration controller.
   *
   * @param eventSubscriptionService Event subscription service logic injected by spring.
   */
  public EventConfigurationController(EventSubscriptionService eventSubscriptionService) {
    this.eventSubscriptionService = eventSubscriptionService;
  }

  /**
   * Configure events for the user.
   *
   * @param requestData request structure for configuring events
   * @return status of the operation.
   */
  @PostMapping("/config")
  public GenericResponse<Status> config(@Valid @RequestBody final ConfigureUser requestData) {
    return new GenericResponse<>(eventSubscriptionService.configureEvents(requestData));
  }

  /**
   * The get event history endpoint.
   *
   * @param requestData query paremeters
   * @return list of history.
   */
  @PostMapping("/history/data")
  public GenericResponse<List<CustomerEventHistory>> historyData(
      @Valid @RequestBody final Pagination requestData) {
	  /*
    return new GenericResponse<>(
        eventSubscriptionService.historyData(
            requestData,
            String.valueOf(SecurityContextHolder.getContext().getAuthentication().getPrincipal())));
    */
    return null;
  }

  /**
   * Number of entries in the history box.
   *
   * @param requestData the input query
   * @return count.
   */
  @PostMapping("/history/count")
  public GenericResponse<Integer> historyCount(@Valid @RequestBody final Pagination requestData) {
	  /*
    return new GenericResponse<>(
        eventSubscriptionService.historyCount(
            requestData,
            String.valueOf(SecurityContextHolder.getContext().getAuthentication().getPrincipal())));
    */
	  return null;
  }

  /**
   * View events of customer.
   *
   * @return the list of event definitions.
   */
  @PostMapping("/view")
  public GenericResponse<Map<Object, Object>> viewEvents(
      @Valid @RequestBody final ConfigViewRequest requestData) {
	  /*
    return new GenericResponse<>(
        eventSubscriptionService.viewEvents(
            String.valueOf(SecurityContextHolder.getContext().getAuthentication().getPrincipal()),
            requestData));
    */
	  return null;
  }

  /**
   * Update event config of user.
   *
   * @param requestData the new event config.
   * @return status of operation..
   */
  @PostMapping("/update")
  public GenericResponse<Status> updateEvent(
      @Valid @RequestBody final EventNotificationConfigUpdate requestData) {
    return new GenericResponse<>(eventSubscriptionService.updateEvent(requestData));
  }
}
