package it.cagisw.userManagment.response;

import org.springframework.beans.BeanUtils;

import it.cagisw.userManagment.models.User;

public class EntityDTO {

	private int id;

	public EntityDTO() {
	}

	public EntityDTO(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public static EntityDTO fromModel(User model) {
		if (model == null) {
			return null;
		}

		EntityDTO dto = new EntityDTO();
		BeanUtils.copyProperties(model, dto);
		return dto;
	}

}
