package it.cagisw.userManagment.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** The structure for events of an account. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountEvents {

  /** Account identifier. */
  private String accountId;

  /** Account type. */
  private String accountType;

  /** Customer identifier. */
  private String customerUniqueId;

  /** List of events for account and customer. */
  private List<CustomerEventConfiguration> events;
}
