package it.cagisw.userManagment.dto;

import java.util.List;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Message for transaction validation. */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionMessage {

  /** Transaction validation authentication method. */
  @NotBlank private TransactionValidationAuthenticationMethod authentMethod;

  /** Data fields for transaction validation */
  @NotBlank private List<TransactionDataField> dataFields;
}
