package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Location phone address. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PhoneAddress {

  /** Type of phone */
  private String phoneType;

  /** Type of phone address */
  private String phoneAddressType;

  /** Country code */
  private String countryCode;

  /** Area code */
  private String areaCode;

  /** Phone number */
  private String phoneNumber;
}
