package it.cagisw.userManagment.requests;

public class ManagerAvailabilityRequest {

	private String managerName;

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
}
