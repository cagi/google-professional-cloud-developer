package it.cagisw.userManagment.service.interfaces;

/** Service to perform operations against customer data management server. */
public interface CustomerDataService {

  /**
   * Determine if the relationship is active
   *
   * @param customerRelationshipAgreementId agreement id
   * @return true if it is active
   */
  boolean isCustomerRelationshipActive(int customerRelationshipAgreementId);
}
