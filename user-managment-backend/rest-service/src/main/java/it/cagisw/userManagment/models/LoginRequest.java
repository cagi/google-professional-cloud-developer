package it.cagisw.userManagment.models;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/** A Pojo representing a login request for a user. */
@Entity
@Data
@Table(name = "LOGIN_REQUESTS")
public class LoginRequest {

  /** Unique key. */
  @Column(name = "LOGIN_ID")
  @Id
  @GeneratedValue
  private long loginId;

  /** Registered user reference. */
  @ManyToOne
  @JoinColumn(name = "REG_ID")
  private RegisteredUser registeredUser;

  /** Customer unique identifier. */
  @Column(name = "CUSTOMERUNIQUEIDENTIFIER")
  private String customerUniqueIdentifier;

  /** Username that logs in */
  @Column(name = "USERNAME")
  private String username;

  /** Unique request id. */
  @Column(name = "REQUESTUUID")
  private String requestUuid;

  /** Created on field. */
  @Column(name = "CREATED_ON")
  private ZonedDateTime createdOn;
}
