package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Parameter structure for templated messaged. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Param {

  /** Name of the parameter. */
  @NotNull private String paramName;

  /** Value of the parameter. */
  @NotNull private String paramValue;
}
