package it.cagisw.userManagment.dto;

import java.time.ZonedDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Structure for pagination information. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Pagination {

  /** Filter to use in pagination. */
  private List<Filter> filter;

  /** Limit of pagination. */
  private int limit;

  /** Page number to fetch. */
  private int pageNo;

  /** Start fate for pagination. */
  private ZonedDateTime startDate;

  /** Sort field. */
  private String sortBy;
}
