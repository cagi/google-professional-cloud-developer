package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;

import it.cagisw.userManagment.models.UsageHistory;

public interface RepositoryUsageHistory extends CrudRepository<UsageHistory, Integer>{

}
