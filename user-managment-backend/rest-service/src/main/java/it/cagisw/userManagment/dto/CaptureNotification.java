package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Event capture structure for internal services. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CaptureNotification {

  /** The customer that message has communicated. */
  @NotNull private String customerUniqueIdentifier;

  /** The event to be stored. */
  @NotNull private String event;

  /** Name of the communication channel. */
  @NotNull private String channel;

  /** Optional account number. */
  private String accountNumber;

  /** Short message of the event. */
  @NotNull private String shortMessage;
}
