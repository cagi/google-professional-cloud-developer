package it.cagisw.userManagment.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

/** Input required for creating a new event for customer. */
@Data
@AllArgsConstructor
public class NewEventInput {

  /** A unique customer id. */
  String customerId;

  /** List of channels. */
  List<Channel> channels;

  /** Event to store. */
  Event event;

}
