package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Response structure for evaluate. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventNotificationResponse {

  /** Response with channels. */
  @NotNull private EventFiltrationResponse eventFiltrationResponse;
}
