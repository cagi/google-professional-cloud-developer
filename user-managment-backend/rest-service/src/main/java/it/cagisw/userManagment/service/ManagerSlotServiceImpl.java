package it.cagisw.userManagment.service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.cagisw.userManagment.commons.exception.BaseBadRequestException;
import com.cagisw.userManagment.commons.exception.BaseNotFoundException;
import com.cagisw.userManagment.commons.exception.ErrorCode;

import it.cagisw.userManagment.dto.ManagerSlotDTO;
import it.cagisw.userManagment.dto.ManagerSlotRequestDTO;
import it.cagisw.userManagment.dto.SlotAvailabilityDTO;
import it.cagisw.userManagment.exceptions.BaseServiceException;
import it.cagisw.userManagment.models.ManagerSlot;
import it.cagisw.userManagment.repository.ManagerSlotsRepository;
import it.cagisw.userManagment.service.interfaces.ManagerSlotService;
import lombok.extern.slf4j.Slf4j;

/** Implementation class for {@link ManagerSlotService} */
@Slf4j
@Service
public class ManagerSlotServiceImpl implements ManagerSlotService {

  /** DB Date format */
  private static final String DB_DATE_FORMAT = "yyyy-MM-dd";

  /** Graph event date format */
  private static final String GRAPH_API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS";

  /** Auto injected ManagerSlotRepository instance */
  @Autowired private ManagerSlotsRepository slotRepo;

  /**
   * Find all manager slots
   *
   * @param managerId Filter by manager id (optional)
   * @return List of found manager slots
   */
  @Override
  public List<ManagerSlotDTO> findAll(Optional<Long> managerId) {

    List<ManagerSlot> slots =
        managerId.isPresent()
            ? slotRepo.findByManagerId(managerId.get())
            : (List<ManagerSlot>) slotRepo.findAll();
    return slots.stream().map(slot -> getDtoFromSlot(slot)).collect(Collectors.toList());
  }

  /**
   * Create a new manager slot
   *
   * @param dto DTO carrying manager slot details to be created
   * @return ID of the created manager slot
   */
  @Transactional
  @Override
  public ManagerSlotDTO createManagerSlot(ManagerSlotRequestDTO dto) {
    validateSlotDetails(dto);
    validateOverlappingWithExistingSlots(
        0, dto.getManagerId(), dto.getStartTime(), dto.getEndTime());
    ManagerSlot slot =
        ManagerSlot.builder()
            .startTime(dto.getStartTime())
            .endTime(dto.getEndTime())
            .managerId(dto.getManagerId())
            .managerEmail(dto.getManagerEmail())
            .slotDuration(dto.getSlotDuration())
            .build();
    slotRepo.save(slot);
    return getDtoFromSlot(slot);
  }

  /**
   * Validate details of the slot
   *
   * @param dto slot details
   */
  private void validateSlotDetails(ManagerSlotRequestDTO dto) {
    if (dto.getManagerId() == 0 || StringUtils.isEmpty(dto.getManagerEmail())) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000015.name(), Collections.emptyList());
    }

    if (dto.getStartTime() < 0
        || dto.getStartTime() > 86400
        || dto.getEndTime() < 0
        || dto.getEndTime() > 86400) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000016.name(), Collections.emptyList());
    }

    if (dto.getEndTime() < dto.getStartTime()) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000017.name(), Collections.emptyList());
    }

    if (dto.getSlotDuration() <= 0 || dto.getSlotDuration() > 120) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000018.name(), Collections.emptyList());
    }

    if (dto.getEndTime() - dto.getStartTime() < dto.getSlotDuration() * 60) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000019.name(), Collections.emptyList());
    }
  }

  /**
   * Find a manager slot by its row ID
   *
   * @param id ID for which manager slot is to be found
   * @return Found manager slot
   */
  @Override
  public ManagerSlotDTO findById(Long id) {
    ManagerSlot slot = getManagerSlot(id);

    return getDtoFromSlot(slot);
  }

  /**
   * Update manager slot
   *
   * @param id ID of the row that needs to be updated
   * @param dto DTO carrying updated manager slot details
   * @return flag to indicate whether row was updated or not
   */
  @Override
  public ManagerSlotDTO updateManagerSlot(Long id, ManagerSlotRequestDTO dto) {
    ManagerSlot slot = getManagerSlot(id);
    dto.setManagerId(slot.getManagerId());
    dto.setManagerEmail(slot.getManagerEmail());
    validateSlotDetails(dto);
    validateOverlappingWithExistingSlots(
        id, dto.getManagerId(), dto.getStartTime(), dto.getEndTime());
    slot.setStartTime(dto.getStartTime());
    slot.setEndTime(dto.getEndTime());
    slot.setSlotDuration(dto.getSlotDuration());
    slotRepo.save(slot);
    return getDtoFromSlot(slot);
  }

  /**
   * Delete a manager slot by its row ID
   *
   * @param id ID for which manager slot is to be deleted
   */
  @Override
  public void deleteManagerSlot(Long id) {
    ManagerSlot slot = getManagerSlot(id);
    slotRepo.delete(slot);
  }

  /**
   * Find available ManagerSlot.
   *
   * @param managerId manager id
   * @param managerEmail email of manager whose availability need to be found
   * @param day the day of the month
   * @return Found ManagerSlot
   */
  @Override
  public SlotAvailabilityDTO findSlotsAvailability(
      Long managerId, String managerEmail, String day) {
    log.info("Getting available slots for manager with id {} on day {}", managerId, day);
    if (managerId == null && StringUtils.isEmpty(managerEmail)) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000026.name(), Collections.emptyList());
    }
    List<ManagerSlot> managerSlots;
    if (managerId != null) managerSlots = slotRepo.findByManagerId(managerId);
    else managerSlots = slotRepo.findByManagerEmail(managerEmail);
    if (managerSlots.size() == 0) {
      throw new BaseNotFoundException(ErrorCode.RMA_ERR000013.name(), Collections.emptyList());
    }
    managerEmail = managerSlots.get(0).getManagerEmail();
    try {
      // divide slots by durations. e.g. slot of 9AM-10AM with duration of 30 mins should be broken
      // down into 2 slots that is 9-9:30 and 9:30-10
      List<SlotAvailabilityDTO.SlotDTO> allSlotsByDuration = getAllSlotsByDuration(managerSlots);

      // get calendar events for the RM
      Date queryDay = new SimpleDateFormat(DB_DATE_FORMAT).parse(day);
      LocalDateTime localDateTime =
          LocalDateTime.ofInstant(queryDay.toInstant(), ZoneId.systemDefault());
      LocalDateTime startTime = localDateTime.toLocalDate().atStartOfDay();
      LocalDateTime endTime = startTime.plusDays(1).minusSeconds(1);

      return SlotAvailabilityDTO.builder().date(day).slots(allSlotsByDuration).build();
    } catch (Exception e) {
      throw new BaseServiceException(ErrorCode.RMA_ERR000020.name(), Collections.emptyList());
    }
  }

  /**
   * Get all the child slots for a slot divided equally by the slotDuration
   *
   * @param managerSlots list of slots
   * @return slots created out of original slots of length equalling slot duration
   */
  private List<SlotAvailabilityDTO.SlotDTO> getAllSlotsByDuration(List<ManagerSlot> managerSlots) {
    List<SlotAvailabilityDTO.SlotDTO> availabilityDTOS = new ArrayList<SlotAvailabilityDTO.SlotDTO>();
    for (ManagerSlot managerSlot : managerSlots) {
      int perSlotDurationInSecs = managerSlot.getSlotDuration() * 60;
      int interimSlotStart = managerSlot.getStartTime();
      int interimSlotEnd = managerSlot.getStartTime() + perSlotDurationInSecs;
      while (interimSlotEnd <= managerSlot.getEndTime()) {
        SlotAvailabilityDTO.SlotDTO dto =
            SlotAvailabilityDTO.SlotDTO.builder()
                .startTime(interimSlotStart)
                .endTime(interimSlotEnd)
                .build();
        availabilityDTOS.add(dto);
        interimSlotStart = interimSlotEnd;
        interimSlotEnd += perSlotDurationInSecs;
      }
    }
    return availabilityDTOS;
  }

  /**
   * Find ManagerSlot by Id, and throw exception if not found
   *
   * @param id ID of the row to be found
   * @return Found ManagerSlot
   */
  private ManagerSlot getManagerSlot(Long id) {
    Optional<ManagerSlot> managerSlot = slotRepo.findById(id);

    if (!managerSlot.isPresent()) {
      throw new BaseNotFoundException(ErrorCode.RMA_ERR000004.name(), Collections.emptyList());
    }
    return managerSlot.get();
  }

  /**
   * Validate whether there is an already existing slot with overlapping time with passed time range
   *
   * @param id ID of the slot being compared to. If greater than 0, this record has to be ignored
   *     from lis of found slots
   * @param managerId Id of the manager
   * @param startTime Start time
   * @param endTime End time
   */
  private void validateOverlappingWithExistingSlots(
      long id, long managerId, int startTime, int endTime) {
    List<ManagerSlot> slots = slotRepo.findSlotsOverlappigWith(managerId, startTime, endTime);

    if (id > 0) {
      // collect only records, that are different from current record
      slots = slots.stream().filter(s -> s.getId() != id).collect(Collectors.toList());
    }
    if (slots.size() > 0) {
      log.error("manager slot overlapping with existing slots");
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000003.name(), Collections.emptyList());
    }
  }

  /**
   * Create ManagerSlotDTO instance for response from ManagerSlot
   *
   * @param slot the model object
   * @return the DTO object
   */
  private ManagerSlotDTO getDtoFromSlot(ManagerSlot slot) {
    return ManagerSlotDTO.builder()
        .id(slot.getId())
        .managerId(slot.getManagerId())
        .managerEmail(slot.getManagerEmail())
        .startTime(slot.getStartTime())
        .endTime(slot.getEndTime())
        .slotDuration(slot.getSlotDuration())
        .build();
  }
}
