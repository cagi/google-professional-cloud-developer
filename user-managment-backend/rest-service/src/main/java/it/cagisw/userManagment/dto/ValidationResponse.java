package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Validation response */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ValidationResponse {

  /** The unique identifier for a customer */
  private String customerUniqueIdentifier;

  /** Indicate if the operation was successful */
  private Response response;
}
