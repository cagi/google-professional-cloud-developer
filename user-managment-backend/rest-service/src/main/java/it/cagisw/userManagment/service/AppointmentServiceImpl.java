package it.cagisw.userManagment.service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.cagisw.userManagment.commons.exception.BaseBadRequestException;
import com.cagisw.userManagment.commons.exception.BaseNotFoundException;
import com.cagisw.userManagment.commons.exception.ErrorCode;

import it.cagisw.userManagment.dto.AppointmentCancelRequestDTO;
import it.cagisw.userManagment.dto.AppointmentDTO;
import it.cagisw.userManagment.dto.AppointmentRequestDTO;
import it.cagisw.userManagment.dto.AttachmentDTO;
import it.cagisw.userManagment.dto.CustomerDTO;
import it.cagisw.userManagment.dto.PaginatedList;
import it.cagisw.userManagment.models.AppointmentMode;
import it.cagisw.userManagment.models.AppointmentStatus;
import it.cagisw.userManagment.models.Appointments;
import it.cagisw.userManagment.repository.AppointmentsRepository;
import it.cagisw.userManagment.service.interfaces.AppointmentService;
import it.cagisw.userManagment.service.interfaces.UserDetailInvoker;
import it.cagisw.userManagment.service.interfaces.VideoServiceInvoker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/** Implementation class for {@link AppointmentService} */
@RequiredArgsConstructor
@Slf4j
@Service
public class AppointmentServiceImpl implements AppointmentService {

  /** Max duration of an appointment is 2 hours */
  private static final long MAX_APPOINTMENT_DURATION = 2 * 60 * 60;

  /** CYN Service user name */
  private static final String CYN_SERVICE_NAME = "cyn_service";

  /** URL format for the meeting */
  private static final String MEETING_URL_FORMAT = "%s/session/%s";

  /** Auto injected AppointmentRepository instance */
  private final AppointmentsRepository appointmentsRepo;

  /** Auto injected VideoServiceInvoker instance */
  private final VideoServiceInvoker videoServiceInvoker;

  /** Auto injected userDetailInvoker instance */
  private final UserDetailInvoker userDetailInvoker;

  /**
   * Find all appointments for the given user
   *
   * @param customerUniqueId logged in user unique Id
   * @return Found list of user appointments
   */
  @Override
  public PaginatedList findAll(String customerUniqueId, int skip, int limit) {
    validatePaginationParams(skip, limit);
    Sort order = Sort.by(Sort.Order.desc(Appointments.DEFAULT_SORT_COLUMN));
    Pageable pageRequest = PageRequest.of(skip / limit, limit, order);
    Page<Appointments> results =
        appointmentsRepo.findByCustomerUniqueIdAndStatusNot(
            customerUniqueId, AppointmentStatus.DELETED.getStatus(), pageRequest);
    Page<AppointmentDTO> page =
        results.map(
            rt -> {
              try {
                String desc = null;
                desc = desc.replace("\n", "").replace("\r", "");
                rt.setResponseDescription(desc);
              } catch (Exception e) {
                log.error(
                    "Erro while trying to fetch desc. present in calendar for appointment id {} , error {}",
                    rt.getId(),
                    e.getMessage());
              }
              return getAppointmentDto(rt);
            });
    return new PaginatedList(page.getTotalElements(), page.getContent());
  }

  /**
   * Create a new appointment
   *
   * @param request DTO appointment details to be created
   * @param customerUniqueId logged in user id
   * @return ID of the created user appointment
   */
  @Override
  @Transactional
  public AppointmentDTO createAppointment(AppointmentRequestDTO request, String customerUniqueId) {
    if (request.getAppointmentDate().toEpochSecond()
        >= request.getAppointmentEndDate().toEpochSecond()) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000006.name(), Collections.emptyList());
    }

    // check total appointment duration
    long totalSlotDuration =
        request.getAppointmentEndDate().toEpochSecond()
            - request.getAppointmentDate().toEpochSecond();
    if (totalSlotDuration > MAX_APPOINTMENT_DURATION) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000007.name(), Collections.emptyList());
    }

    long managerId = getManagerId(customerUniqueId);
    Appointments app =
        Appointments.builder()
            .appointmentDate(request.getAppointmentDate())
            .appointmentEndDate(request.getAppointmentEndDate())
            .address(request.getAddress())
            .managerId(managerId)
            .description(request.getDescription())
            .mode(request.getMode())
            .status(AppointmentStatus.CONFIRMED.getStatus())
            .customerUniqueId(customerUniqueId)
            .subject(request.getSubject())
            .build();

    app = appointmentsRepo.save(app);

    // call video service to create a session
    if (request.getMode().toUpperCase().equals(AppointmentMode.VIDEO.toString())) {
      String sessionId =
          videoServiceInvoker.createSession(app.getId(), managerId, customerUniqueId);
      log.info("successfully created session with id: " + sessionId);

      String description = request.getDescription();
      String graphEventDescription =
          prepareGraphEventDescription(
              description, sessionId, false, customerUniqueId, request.getAppointmentDate());
      app.setDescription(graphEventDescription);
    }

    try {
      app.setResponseDescription(app.getDescription());
      app.setDescription(request.getDescription());
      appointmentsRepo.save(app);
    } catch (Exception e) {
      log.error("Failed to create calendar", e);
    }

    return getAppointmentDto(app);
  }

  /**
   * prepare graph event description
   *
   * @param description : Customer sent description
   * @param sessionId : sessionId
   * @param isUpdated : whether event is being updated
   * @param customerUniqueId : customerUniqueId
   * @param appointmentDate : appointmentDate
   * @return graphEventDescription
   */
  private String prepareGraphEventDescription(
      String description,
      String sessionId,
      Boolean isUpdated,
      String customerUniqueId,
      ZonedDateTime appointmentDate) {
    StringBuilder graphEventDescription = new StringBuilder();
    String scheduledText = "scheduled";
    if (isUpdated) scheduledText = "re-" + scheduledText;
    String url = String.format(MEETING_URL_FORMAT, "https://rm.odyssey-api-dev.com", sessionId);
    String meetingText = String.format("<p><b>Meeting link</b>: <a href=%s>%s</a></p>", url, url);

    CustomerDTO customerDetails = userDetailInvoker.getCustomerDetails(customerUniqueId);
    if (customerDetails != null) {
      graphEventDescription.append(
          String.format(
              "Greetings,<br/>Meeting %s with <b>%s %s</b> on <u>%s</u><br/>%s",
              scheduledText,
              customerDetails.getTitle(),
              customerDetails.getLastName(),
              appointmentDate.toLocalDate(),
              meetingText));
      graphEventDescription.append(
          String.format(
              "<p><b>   Customer Id : </b> %s<br/><b>   Customer Name : </b>  %s %s %s %s</p><p><b>  Customer Message : </b> %s</p>",
              customerUniqueId,
              customerDetails.getTitle(),
              customerDetails.getFirstName(),
              customerDetails.getMiddleName(),
              customerDetails.getLastName(),
              description));
    } else {
      graphEventDescription.append(
          String.format(
              "Greetings,<br/>Meeting %s on <u>%s</u><br/>%s",
              scheduledText, appointmentDate.toLocalDate(), meetingText));
      graphEventDescription.append(
          String.format(
              "<p><b>   Customer Id : </b> %s<br/><b>   Customer Name : </b><i>(Couldn't be feched)</i> </p><p><b>  Customer Message : </b> %s</p>",
              customerUniqueId, description));
    }
    return graphEventDescription.toString();
  }

  /**
   * Find an appointment by its row ID
   *
   * @param id ID for which manager rating is to be found
   * @param customerUniqueId logged in user id
   * @return Found appointment
   */
  @Override
  public AppointmentDTO findById(Long id, String customerUniqueId) {
    Appointments appt = validateAndGetAppointment(id, customerUniqueId);
    try {
      String desc = null;
      desc = desc.replace("\n", "").replace("\r", "");
      appt.setResponseDescription(desc);
    } catch (Exception e) {
      log.error(
          "Erro while trying to fetch desc. present in calendar for appointment id {} , error {}",
          appt.getId(),
          e.getMessage());
    }

    return getAppointmentDto(appt);
  }

  /**
   * Update an appointment
   *
   * @param id ID of the row that needs to be updated
   * @param request DTO carrying updated appointment details
   * @param customerUniqueId logged in user id
   * @return updated appointment
   */
  @Override
  @Transactional
  public AppointmentDTO updateAppointment(
      Long id, AppointmentRequestDTO request, String customerUniqueId) {
    Appointments appointment = validateAndGetAppointment(id, customerUniqueId);

    if (request.getAppointmentDate().toEpochSecond()
        >= request.getAppointmentEndDate().toEpochSecond()) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000006.name(), Collections.emptyList());
    }

    // check total appointment duration
    long totalSlotDuration =
        request.getAppointmentEndDate().toEpochSecond()
            - request.getAppointmentDate().toEpochSecond();
    if (totalSlotDuration > MAX_APPOINTMENT_DURATION) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000007.name(), Collections.emptyList());
    }

    // Manager id, status can't be changed
    appointment.setAppointmentDate(request.getAppointmentDate());
    appointment.setAppointmentEndDate(request.getAppointmentEndDate());
    appointment.setAddress(request.getAddress());
    appointment.setDescription(request.getDescription());
    appointment.setMode(request.getMode());
    appointment.setSubject(request.getSubject());
    appointmentsRepo.save(appointment);

    if (appointment.getMicrosoftGraphEventId() != null) {
      if (appointment.getMode().toUpperCase().equals(AppointmentMode.VIDEO.toString())) {
        String sessionId =
            videoServiceInvoker.getSessionFromAppointment(appointment.getId(), customerUniqueId);
        log.info("successfully created session with id: " + sessionId);

        String description = appointment.getDescription();
        String graphEventDescription =
            prepareGraphEventDescription(
                description, sessionId, true, customerUniqueId, request.getAppointmentDate());
        appointment.setDescription(graphEventDescription);
      }

      try {
        appointment.setResponseDescription(appointment.getDescription());
        appointment.setDescription(request.getDescription());
        log.info("Updated microsoft graph event: {}", appointment.getMicrosoftGraphEventId());
      } catch (Exception e) {
        log.error("Failed to update calendar", e);
      }
    }

    return getAppointmentDto(appointment);
  }

  /**
   * Cancel an appointment
   *
   * @param id ID of the row that needs to be updated
   * @param request DTO carrying updated appointment details
   * @param customerUniqueId logged in user Id
   * @return updated appointment
   */
  @Override
  @Transactional
  public AppointmentDTO cancelAppointment(
      Long id, AppointmentCancelRequestDTO request, String customerUniqueId) {
    Appointments appointment = validateAndGetAppointment(id, customerUniqueId);

    appointment.setCancelationReason(request.getCancelationReason());
    appointment.setStatus(AppointmentStatus.CANCELED.getStatus());
    appointmentsRepo.save(appointment);

    return getAppointmentDto(appointment);
  }

  /**
   * Accept new date in Appointment
   *
   * @param id ID for which manager slot is to be deleted
   * @param customerUniqueId logged in user id
   */
  @Override
  @Transactional
  public AppointmentDTO acceptNewAppointmentDate(Long id, String customerUniqueId) {
    Appointments appointment = validateAndGetAppointment(id, customerUniqueId);

    String status = appointment.getStatus();
    if (appointment.getNewDate() == null
        || appointment.getNewEndDate() == null
        || (!status.equals(AppointmentStatus.PENDING.getStatus())
            && !status.equals(AppointmentStatus.CANCELED.getStatus()))) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000021.name(), Collections.emptyList());
    }

    String existingDescription = appointment.getDescription();

    appointment.setAppointmentDate(appointment.getNewDate());
    appointment.setAppointmentEndDate(appointment.getNewEndDate());
    appointment.setNewDate(null);
    appointment.setNewEndDate(null);
    appointment.setStatus(AppointmentStatus.CONFIRMED.getStatus());
    appointmentsRepo.save(appointment);

    if (appointment.getMicrosoftGraphEventId() != null) {
      try {
        if (appointment.getMode().toUpperCase().equals(AppointmentMode.VIDEO.toString())) {
          String sessionId =
              videoServiceInvoker.getSessionFromAppointment(appointment.getId(), customerUniqueId);
          log.info("successfully created session with id: " + sessionId);

          String description = appointment.getDescription();
          String graphEventDescription =
              prepareGraphEventDescription(
                  description, sessionId, true, customerUniqueId, appointment.getAppointmentDate());
          appointment.setDescription(graphEventDescription);
        }

        appointment.setResponseDescription(appointment.getDescription());
        appointment.setDescription(existingDescription);
        log.info("Updated microsoft graph event: {}", appointment.getMicrosoftGraphEventId());
      } catch (Exception e) {
        log.error("Failed to update calendar", e);
      }
    }

    return getAppointmentDto(appointment);
  }

  /**
   * Soft deletes an appointment by its row ID
   *
   * @param id ID for which manager slot is to be deleted
   * @param customerUniqueId logged in user id
   */
  @Override
  @Transactional
  public void deleteAppointment(Long id, String customerUniqueId) {
    Appointments appointment = validateAndGetAppointment(id, customerUniqueId);

    if (appointment.getMicrosoftGraphEventId() != null) {
      try {
        log.info("Deleted microsoft graph event: {}", appointment.getMicrosoftGraphEventId());
      } catch (Exception e) {
        log.error("Failed to delete calendar", e);
      }
    }

    appointment.setMicrosoftGraphEventId(null);
    appointment.setStatus(AppointmentStatus.DELETED.getStatus());
    appointmentsRepo.save(appointment);
  }

  /**
   * Find Appointments by Id, and throw exception if not found or not authorised
   *
   * @param id ID of the row to be found
   * @param customerUniqueId logged in user id
   * @return Found Appointments
   */
  private Appointments validateAndGetAppointment(Long id, String customerUniqueId) {
    Optional<Appointments> found = appointmentsRepo.findById(id);

    if (!found.isPresent()) {
      throw new BaseNotFoundException(ErrorCode.RMA_ERR000004.name(), Collections.emptyList());
    }
    Appointments appointments = found.get();

    // video service will call by creator or manager of appointments
    boolean isManagerOrService =
        checkManagerRole(customerUniqueId) || CYN_SERVICE_NAME.equals(customerUniqueId);

    if (!isManagerOrService
        && !appointments.getCustomerUniqueId().equalsIgnoreCase(customerUniqueId)) {
      throw new BaseNotFoundException(ErrorCode.RMA_ERR000009.name(), Collections.emptyList());
    }

    return appointments;
  }

  /**
   * Convert an Appointment into an AppointmentDTO
   *
   * @param app the appointment
   * @return the equivalent DTO
   */
  private AppointmentDTO getAppointmentDto(Appointments app) {
    AppointmentDTO appDto =
        AppointmentDTO.builder()
            .id(app.getId())
            .managerId(app.getManagerId())
            .customerUniqueId(app.getCustomerUniqueId())
            .appointmentDate(app.getAppointmentDate())
            .appointmentEndDate(app.getAppointmentEndDate())
            .address(app.getAddress())
            .cancelationReason(app.getCancelationReason())
            .description(
                app.getResponseDescription() == null
                    ? app.getDescription()
                    : app.getResponseDescription())
            .mode(app.getMode())
            .newDate(app.getNewDate())
            .newEndDate(app.getNewEndDate())
            .status(app.getStatus())
            .subject(app.getSubject())
            .build();

    List<AttachmentDTO> attachmentDtos =
        app.getAttachments() != null
            ? app.getAttachments().stream()
                .map(
                    att ->
                        AttachmentDTO.builder()
                            .id(att.getId())
                            .appointmentId(app.getId())
                            .url(att.getUrl())
                            .fileName(att.getFileName())
                            .cratedDate(att.getCratedDate())
                            .build())
                .collect(Collectors.toList())
            : new ArrayList();

    appDto.setAttachments(attachmentDtos);
    return appDto;
  }

  /**
   * Validate the pagination params
   *
   * @param skip number of rows to skip
   * @param limit number of rows to return
   */
  private void validatePaginationParams(int skip, int limit) {
    if (skip < 0 || limit <= 0) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000001.name(), Collections.emptyList());
    }
  }

  /**
   * Get manager Id.
   *
   * @return the email
   */
  private long getManagerId(String customerUniqueId) {
    try {
      return userDetailInvoker.getManager(customerUniqueId).getId();
    } catch (Exception e) {
      throw new BaseBadRequestException(ErrorCode.RMA_ERR000023.name(), Collections.emptyList());
    }
  }

  /**
   * Check user has manager role.
   *
   * @param customerUniqueId user unique id
   * @return true if user has manager role, false otherwise
   */
  private boolean checkManagerRole(String customerUniqueId) {
    /*Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      log.info(
          String.format(
              "User roles %d, %s",
              auth.getAuthorities().size(),
              auth.getAuthorities().stream()
                  .map(f -> f.getAuthority())
                  .collect(Collectors.joining(","))));
      return customerUniqueId.equals("CCDS0000000028");
    }*/
    return false;
  }
}
