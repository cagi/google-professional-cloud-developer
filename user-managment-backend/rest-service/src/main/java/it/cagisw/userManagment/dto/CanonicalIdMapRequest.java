package it.cagisw.userManagment.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/** Payload to request a canonical id mapping for a customer. */
@Data
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class CanonicalIdMapRequest extends BaseProfileRequest {}
