package it.cagisw.userManagment.models;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/** A Pojo representing a cronto code refresh counter. */
@Entity
@Data
@Table(name = "CRONTO_CODE_REFRESH_COUNTER")
public class CrontoCodeRefreshCounter {

  /** Unique key */
  @Id
  @GeneratedValue
  @Column(name = "REFRESH_ID")
  private long refreshId;

  /** Cronto code render request reference. */
  @ManyToOne
  @JoinColumn(name = "CRONTO_CODE_ID")
  private CrontoRenderRequest crontoRenderRequest;

  /** Retry counter. */
  @Column(name = "COUNTER")
  private int counter;

  /** Created timestamp. */
  @Column(name = "CREATED_ON")
  private ZonedDateTime createdOn;

  /** Updated timestamp. */
  @Column(name = "UPDATED_ON")
  private ZonedDateTime updatedOn;
}
