package it.cagisw.userManagment.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** transaction standard */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionStandard {

  /** data Fields */
  private List<String> dataFields;

  /** signature */
  private String signature;
}
