package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Request a new user registration to onespan api. */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationRequest {

  /** Internal customer unique identifier. */
  @NotBlank private String customerUniqueIdentifier;

  /** User id to register. */
  @NotBlank private String userId;
}
