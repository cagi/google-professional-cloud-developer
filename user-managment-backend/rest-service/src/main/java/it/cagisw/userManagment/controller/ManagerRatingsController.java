package it.cagisw.userManagment.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.dto.ManagerRatingDTO;
import it.cagisw.userManagment.dto.ManagerRatingStatisticsDTO;
import it.cagisw.userManagment.dto.PaginatedList;
import it.cagisw.userManagment.service.interfaces.ManagerRatingsService;

/** Manager slots controller. */
@RequestMapping("/manager-ratings")
@RestController
public class ManagerRatingsController {

	/** Auto injected ManagerRatingsService instance */
	@Autowired
	ManagerRatingsService service;

	/**
	 * Creates a manager slot
	 *
	 * @param ratingDTO object carrying ratings data
	 * @return created manager ratings
	 */
	@PostMapping
	public GenericResponse<ManagerRatingDTO> createManagerRating(@PathVariable String customerUniqueId,
			@Valid @RequestBody ManagerRatingDTO ratingDTO) {

		return new GenericResponse<>(service.createManagerRating(ratingDTO, customerUniqueId));
	}

	/**
	 * Get all manager ratings available in DB
	 *
	 * @param customerUniqueId unique identifier of the logged-in user
	 * @param skip             number of rows to skip
	 * @param limit            number of rows to return
	 * @param rating           list of manager rating to filter
	 * @param sortOrder        sort by rating created date oldest first/newest fisrt
	 * @return list of found manager ratings
	 */
	@GetMapping("/{customerUniqueId}")
	public GenericResponse<PaginatedList> getAllManagerRatings(@PathVariable("customerUniqueId") String customerUniqueId,
			@RequestParam int skip, @RequestParam int limit, @RequestParam(required = false) List<Integer> rating,
			@RequestParam(required = false) String sortOrder) {

		return new GenericResponse<>(service.findAll(customerUniqueId, skip, limit, rating, sortOrder));
	}

	/**
	 * Get manager's star rating overview : average rating, star count
	 *
	 * @param customerUniqueId unique identifier of the logged-in user
	 * @return the statistics of the manager's rating
	 */
	@GetMapping("/statistics/{customerUniqueId}")
	public GenericResponse<ManagerRatingStatisticsDTO> getManagerRatingStatistics(
			@RequestParam String customerUniqueId) {
		return new GenericResponse<>(service.getStatistics(customerUniqueId));
	}

	/**
	 * Get manager individual ratings by its ID
	 *
	 * @param customerUniqueId unique identifier of the logged-in user
	 * @param ratingId         ID of the rating
	 * @return found manager ratings
	 */
	@GetMapping("/{ratingId}/{customerUniqueId}")
	public GenericResponse<ManagerRatingDTO> getManagerRatingById(
			@PathVariable("customerUniqueId") String customerUniqueId, @PathVariable("ratingId") long ratingId) {

		return new GenericResponse<>(service.findById(ratingId, customerUniqueId));
	}

	/**
	 * Updates a manager's specific rating created by current user
	 *
	 * @param customerUniqueId unique identifier of the logged-in user
	 * @param ratingId         ID of the manager ratings
	 * @param ratingDTO        object carrying updated ratings data
	 * @return updated manager ratings
	 */
	@PutMapping("/{ratingId}")
	public GenericResponse<ManagerRatingDTO> updateManagerRatings(@PathVariable("customerUniqueId") String customerUniqueId,
			@PathVariable("ratingId") long ratingId, @Valid @RequestBody ManagerRatingDTO ratingDTO) {

		return new GenericResponse<>(service.updateManagerRating(ratingId, ratingDTO, customerUniqueId));
	}

	/**
	 * delete a specific rating of a manager created by current user
	 *
	 * @param customerUniqueId unique identifier of the logged-in user
	 * @param ratingId         ID of the manager rating to be deleted
	 */
	@DeleteMapping("/{ratingId}")
	public GenericResponse deleteManagerRatingsById(@PathVariable("customerUniqueId") String customerUniqueId,
			@PathVariable("ratingId") long ratingId) {

		service.deleteManagerRating(ratingId, customerUniqueId);
		return new GenericResponse();
	}
}
