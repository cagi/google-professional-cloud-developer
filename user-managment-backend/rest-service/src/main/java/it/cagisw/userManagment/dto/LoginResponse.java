package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/** Response structure for login. */
@AllArgsConstructor
@Data
@NoArgsConstructor
@SuperBuilder
public class LoginResponse {

	/** Session status of the login request. */
	private SessionStatus sessionStatus;

	/** Message for the login operation. */
	private String requestMessage;

	/** Onespan request id. */
	private String requestId;

	String accessToken;

	String refreshToken;

	long accessTokenExpiresIn;

}
