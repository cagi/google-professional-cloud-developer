package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.cagisw.userManagment.dto.LoginRequest;

/** The spring data repository for storing login requests. */
@Repository
public interface LoginRequestRepository extends CrudRepository<LoginRequest, Long> {}
