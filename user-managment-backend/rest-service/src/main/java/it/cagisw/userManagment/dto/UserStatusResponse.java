package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Response structure for user status. */
@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class UserStatusResponse {

  /** Flag indicates if user is locked. */
  private boolean locked;

  /** Flag indicates if user is enabled. */
  private boolean enabled;
}
