package it.cagisw.userManagment.exceptions;

/** Constants for exception handling */
public class ExceptionConstants {

  private ExceptionConstants() {}

  /** The name of property bundle * */
  public static final String BUNDLE_NAME = "messages";
}
