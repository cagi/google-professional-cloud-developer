package it.cagisw.userManagment.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.text.MessageFormat;

/** Authentication method enumeration for transaction validation. */
public enum TransactionValidationAuthenticationMethod {

  /** Login with fingerprint */
  FINGERPRINT("Fingerprint"),

  /** Login with faceid */
  FACEID("Face"),

  /** Login with pin */
  PIN("PIN"),

  /** Login with pin */
  NO_PIN("NoPIN");

  /** Actual enum value */
  private static final String ERROR_PATTERN = "{0} value is not correct for Authentication method";

  /** Actual enum value */
  private String value;

  /** Thread local formatters */
  private static ThreadLocal<MessageFormat> threadLocalMessageFormat =
      ThreadLocal.withInitial(() -> new MessageFormat(ERROR_PATTERN));

  /**
   * Constuctor for enum
   *
   * @param value enum value
   */
  TransactionValidationAuthenticationMethod(String value) {
    this.value = value;
  }

  /**
   * Get the actual value of the enum.
   *
   * @return
   */
  @JsonValue
  public String getValue() {
    return value;
  }

  /**
   * Convert enum to string
   *
   * @return string representation of enum
   */
  @Override
  public String toString() {
    return String.valueOf(value);
  }

  /**
   * Create enum from string value
   *
   * @param text string value
   * @return PassKey instance.
   */
  @JsonCreator
  public static TransactionValidationAuthenticationMethod fromValue(String text) {
    for (TransactionValidationAuthenticationMethod b :
        TransactionValidationAuthenticationMethod.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    throw new IllegalArgumentException(threadLocalMessageFormat.get().format(text));
  }
}
