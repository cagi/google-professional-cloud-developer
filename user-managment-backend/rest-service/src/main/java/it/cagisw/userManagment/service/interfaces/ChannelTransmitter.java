package it.cagisw.userManagment.service.interfaces;

/** Interface of message transmitter. */
public interface ChannelTransmitter {

  /**
   * Transmit a given security message via underlying implementation.
   *
   * @param message the actual transmit request.
   * @param event event to trigger.
   * @param subject subject of the message.
   * @return status of the operation.
   */
  void transmitRequestForSecurityEvent(
      String message, String customerId, String event, String subject);
}
