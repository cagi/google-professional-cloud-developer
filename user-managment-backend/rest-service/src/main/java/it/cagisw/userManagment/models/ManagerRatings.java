package it.cagisw.userManagment.models;

import java.time.ZonedDateTime;
import javax.persistence.*;
import lombok.*;

/** Manager ratings. */
@Entity
@Table(name = "manager_ratings")
@Data
@ToString(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ManagerRatings {

  /** Default sort column */
  public static final String DEFAULT_SORT_COLUMN = "createdDate";

  /** Id. */
  @Id
  @Column(name = "id", nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  /** Rating manager id. */
  @Column(name = "manager_id", nullable = false)
  private long managerId;

  /** Rating user unique id. */
  @Column(name = "customer_unique_id", nullable = false, columnDefinition = "VARCHAR(64)")
  private String customerUniqueId;

  /** Rating value */
  @Column(name = "rating", columnDefinition = "SMALLINT", nullable = false)
  private int rating;

  /** Rating feedback. */
  @Column(name = "feedback", columnDefinition = "TEXT", nullable = false)
  private String feedback;

  @Column(name = "created_date", nullable = false)
  private ZonedDateTime createdDate;
}
