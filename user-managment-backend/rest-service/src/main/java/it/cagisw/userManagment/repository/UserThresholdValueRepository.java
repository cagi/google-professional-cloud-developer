package it.cagisw.userManagment.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.cagisw.userManagment.models.AlertThresholdValue;
import it.cagisw.userManagment.models.EventMap;

/** Repository for storing master event thresholds. */
public interface UserThresholdValueRepository extends JpaRepository<AlertThresholdValue, Long> {

  /**
   * Get a threshold value for an event map.
   *
   * @param eventMap event map instance.
   * @return optional might contain threshold.
   */
  Optional<AlertThresholdValue> findOneByEventMap(EventMap eventMap);

  /**
   * Delete all thresholds.
   *
   * @param eventMap event map to correlate.
   */
  void deleteByEventMap(EventMap eventMap);
}
