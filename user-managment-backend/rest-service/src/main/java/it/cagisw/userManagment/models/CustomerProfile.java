package it.cagisw.userManagment.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Entity for customer profile. */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerProfile {

  /** Indicates the relationship state for the customer with the bank */
  private boolean partyRelationshipStatus;

  /** Profile of the customer */
  private ProfileEntity profileEntity;

  /** Canonical id mapping of the customer */
  private CanonicalIdMapEntity canonicalIdMapEntity;
}
