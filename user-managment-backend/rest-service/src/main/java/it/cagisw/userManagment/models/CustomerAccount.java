package it.cagisw.userManagment.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/** POJO representing a configuration for an account. */
@Entity
@Data
@Table(name = "CUSTOMER_ACCOUNT")
public class CustomerAccount {

  /** Account id of composite key. */
  @Column(name = "ACCOUNT_ID")
  @Id
  @GeneratedValue
  private int accountId;

  /** CustomerEventConfiguration field of composite key. */
  @ManyToOne
  @JoinColumn(name = "EVENT_CONF_ID")
  private CustomerEventConfiguration eventConf;

  /** Created on audit field. */
  @Column(name = "ACCOUNTNUMBER")
  private String accountNumber;
}
