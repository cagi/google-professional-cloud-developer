package it.cagisw.userManagment.service.interfaces;

import it.cagisw.userManagment.dto.CustomerDTO;
import it.cagisw.userManagment.dto.ManagerDTO;

/** Service interface for invoking rm-video server */
public interface UserDetailInvoker {

  /**
   * get email id of regular user from profile service
   *
   * @param userId : customer unique identifier
   * @return customer email id found
   */
  String getUserEmail(String userId);

  /**
   * get customer's details :
   *
   * <ul>
   *   <li>title
   *   <li>firstName
   *   <li>lastName
   *   <li>middleName
   * </ul>
   *
   * @param customerUniqueId : customer unique identifier
   * @return customer details found
   */
  CustomerDTO getCustomerDetails(String userId);

  /**
   * get manager details from contact service using customer id
   *
   * @param userId : manager user unique identifier
   * @return manager found
   */
  ManagerDTO getManager(String userId);
}
