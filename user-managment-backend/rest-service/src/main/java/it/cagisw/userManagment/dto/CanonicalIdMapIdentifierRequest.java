package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Payload to request a canonical id mapping by identifier for a customer. */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CanonicalIdMapIdentifierRequest {

  /** Customer unique identifier */
  private String customerUniqueIdentifier;

  /** Type of identifier */
  private IdentifierType identifierType;
}
