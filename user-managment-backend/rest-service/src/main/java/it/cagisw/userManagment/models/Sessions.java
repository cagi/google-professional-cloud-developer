package it.cagisw.userManagment.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name = "sessions")
@Entity
public class Sessions {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	/** RM video session id. */
	@Column(name = "session_id", columnDefinition = "VARCHAR(128)", nullable = false)
	private String sessionId;

	/** RM video appointment. */
	@OneToOne
	@JoinColumn(name = "appointments_id")
	private Appointments appointmentsId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Appointments getAppointmentsId() {
		return appointmentsId;
	}

	public void setAppointmentsId(Appointments appointmentsId) {
		this.appointmentsId = appointmentsId;
	}

}
