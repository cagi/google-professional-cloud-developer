package it.cagisw.userManagment.service.interfaces;

/** Service interface for invoking rm-video server */
public interface VideoServiceInvoker {

  /**
   * Create a session in rm-video server
   *
   * @param appointmentId the appointmentId
   * @param managerId the Id of the manager
   * @param customerUniqueId the logged-in unique id
   * @return the created sessionId
   */
  String createSession(Long appointmentId, Long managerId, String customerUniqueId);

  /**
   * Get session from appointment
   *
   * @param appointmentId the appointmentId
   * @param customerUniqueId the logged-in unique id
   * @return the created sessionId
   */
  String getSessionFromAppointment(long appointmentId, String customerUniqueId);
}