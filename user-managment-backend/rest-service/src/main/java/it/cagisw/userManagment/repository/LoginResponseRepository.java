package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.cagisw.userManagment.dto.LoginResponse;

/** The spring data repository for storing login responses. */
@Repository
public interface LoginResponseRepository extends CrudRepository<LoginResponse, Long> {}
