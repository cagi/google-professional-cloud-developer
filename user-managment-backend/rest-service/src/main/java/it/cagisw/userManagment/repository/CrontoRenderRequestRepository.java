package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.cagisw.userManagment.models.CrontoRenderRequest;

/** The spring data repository for storing the cronto render requests */
@Repository
public interface CrontoRenderRequestRepository extends CrudRepository<CrontoRenderRequest, Long> {}
