package it.cagisw.userManagment.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/** POJO representing a mapping between event and channel master record. */
@Data
@Entity
@Table(name = "DEFAULT_EVENTS_CHANNEL_MAP")
@IdClass(EventChannelMapId.class)
public class EventChannelMap {

  /** Primary key of entity. */
  @GeneratedValue private long mapId;

  /** Composite primary key event field */
  @Id
  @ManyToOne
  @JoinColumn(name = "EVENT_ID")
  private Event event;

  /** Composite primary key channel field */
  @Id
  @ManyToOne
  @JoinColumn(name = "CHANNEL_ID")
  private Channel channel;
}
