package it.cagisw.userManagment.models;

import java.util.List;

import it.cagisw.userManagment.dto.ContactPoint;
import it.cagisw.userManagment.dto.PartyLocation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Profile entity. */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfileEntity {

  /** The details with the name of the customer */
  private CustomerNameEntity customerName;

  /** Date of birth */
  private String dob;

  /** Indicates if the customer is an UK citizen */
  private Boolean ukCitizen;

  /** Nationality */
  private String nationality;

  /** Contact points */
  private List<ContactPoint> contactPoints;

  /** Party Locations */
  private List<PartyLocation> partyLocations;
}
