package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Key structure for data field. */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionValidationKey {

  /** Text of the data field key. */
  @NotBlank private String text;
}
