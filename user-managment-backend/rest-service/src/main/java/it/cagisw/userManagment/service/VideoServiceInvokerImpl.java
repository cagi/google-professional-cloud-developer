package it.cagisw.userManagment.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.dto.SessionDTO;
import it.cagisw.userManagment.service.interfaces.VideoServiceInvoker;
import lombok.extern.slf4j.Slf4j;

/** Implementation class for {@link VideoServiceInvoker} */
@Slf4j
@Service
public class VideoServiceInvokerImpl implements VideoServiceInvoker {

  /** Cyn event to be used in http request */
  public static final String CYN_EVENT = "SESSION_MESSAGE";

  /** rm-appointment server url. */
  private static final String VIDEO_RETRIEVE_URL =
      "http://%s/v1/conferencing/service/customer/rm/sessions";

  /** Return type of email channel */
  private static final TypeReference<GenericResponse<SessionDTO>> GENERIC_TYPE =
      new TypeReference<>() {};

  /** rm-video server host */
  @Value("${video.server.name}")
  private String videoServerName;

  /**
   * Create a session in rm-video server
   *
   * @param appointmentId the appointmentId
   * @param managerId the ID of the manager
   * @param customerUniqueId the id of the appointment creator
   * @return the created sessionId
   */
  public String createSession(Long appointmentId, Long managerId, String customerUniqueId) {
	 /* 
    Map<String, String> body = new HashMap<>();
    body.put("appointmentId", appointmentId.toString());
    body.put("managerId", managerId.toString());
    body.put("customerUniqueId", customerUniqueId);
    RemoteInvocationContext<?> context =
        RemoteInvocationContext.builder()
            .cynEvent(CYN_EVENT)
            .returnType(Object.class)
            .requestBody(body)
            .httpMethod(HttpMethod.POST)
            .url(String.format(VIDEO_RETRIEVE_URL, videoServerName))
            .includeAccessToken(Boolean.TRUE)
            .build();
    try {
      ResponseEntity<?> responseEntity = invocationService.executeRemoteService(context);
      return JsonUtils.OBJECT_MAPPER
          .convertValue(responseEntity.getBody(), GENERIC_TYPE)
          .getBody()
          .getSessionId();
    } catch (Exception e) {
      log.error(ErrorCode.RMA_ERR000012.name(), e);
      throw new BaseServiceException(ErrorCode.RMA_ERR000012.name(), Collections.emptyList());
    }
    */
	  return null;
  }

  /**
   * Get session from appointment
   *
   * @param appointmentId the appointmentId
   * @param customerUniqueId the logged-in unique id
   * @return the sessionId
   */
  public String getSessionFromAppointment(long appointmentId, String customerUniqueId) {
	  /*
    RemoteInvocationContext<?> context =
        RemoteInvocationContext.builder()
            .cynEvent(CYN_EVENT)
            .returnType(Object.class)
            .httpMethod(HttpMethod.GET)
            .url(
                String.format(
                    VIDEO_RETRIEVE_URL + "?appointmentId=%d", videoServerName, appointmentId))
            .includeAccessToken(Boolean.TRUE)
            .build();
    try {
      ResponseEntity<?> responseEntity = invocationService.executeRemoteService(context);
      return JsonUtils.OBJECT_MAPPER
          .convertValue(responseEntity.getBody(), GENERIC_TYPE)
          .getBody()
          .getSessionId();
    } catch (Exception e) {
      log.error(ErrorCode.RMA_ERR000012.name(), e);
      throw new BaseServiceException(ErrorCode.RMA_ERR000012.name(), Collections.emptyList());
    }*/
    return null;
  }
}
