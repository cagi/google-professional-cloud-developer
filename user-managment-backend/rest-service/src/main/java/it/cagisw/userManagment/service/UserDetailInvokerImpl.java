package it.cagisw.userManagment.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.cagisw.userManagment.commons.exception.ErrorCode;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.dto.CustomerDTO;
import it.cagisw.userManagment.dto.ManagerDTO;
import it.cagisw.userManagment.dto.RemoteInvocationContext;
import it.cagisw.userManagment.exceptions.BaseServiceException;
import it.cagisw.userManagment.service.interfaces.UserDetailInvoker;
import it.cagisw.userManagment.service.interfaces.VideoServiceInvoker;
import lombok.extern.slf4j.Slf4j;

/** Implementation class for {@link VideoServiceInvoker} */
@Slf4j
@Service
public class UserDetailInvokerImpl implements UserDetailInvoker {

  /** Cyn event to be used for customer in http request */
  public static final String CYN_EVENT_CUSTOMER_CANONICAL_MAP = "PROFILE_EVENT:GET_CANONICAL_MAP";

  /** Cyn event to be used for customer in http request */
  public static final String CYN_EVENT_CUSTOMER_RETRIEVE = "PROFILE_EVENT:RETRIEVE";

  /** Cyn event to be used for manager in http request */
  public static final String CYN_EVENT_MANAGER = "SECURITY_EVENT:ALERT_CONFIGURE";

  /** customer profile service url. */
  private static final String CUSTOMER_DETAIL_RETRIEVE_URL =
      "http://%s/cyn_customer_profile_service/v1/customer/services/customer/profile/canonicalmap";

  /** url to retrieve the profile of a given customer . */
  private static final String CUSTOMER_PROFILE_RETRIEVE_URL =
      "http://%s/cyn_customer_profile_service/v1/customer/services/customer/profile/retrieve";

  /** contact rm service url. */
  private static final String MANAGER_DETAIL_RETRIEVE_URL =
      "http://%s/cyn_contact_rm_service/v1/personal/customer/services/relationship/manager/details";

  /** Return type of email channel */
  private static final TypeReference<GenericResponse<ManagerDTO>> GENERIC_TYPE =
      new TypeReference<>() {};

  /** customer profile server host */
  @Value("${profile.server.name}")
  private String customerProfileServerName;

  /** rm contact server host */
  @Value("${rm.contact.server.name}")
  private String rmContactServerName;

  /**
   * get customer email
   *
   * @param userId : user Id
   * @return email found
   */
  @Override
  public String getUserEmail(String userId) {
	  
	  /*
    RemoteInvocationContext<?> context =
        RemoteInvocationContext.builder()
            .cynEvent(CYN_EVENT_CUSTOMER_RETRIEVE)
            .returnType(Object.class)
            .requestBody(getRequestBody(userId))
            .httpMethod(HttpMethod.POST)
            .url(String.format(CUSTOMER_DETAIL_RETRIEVE_URL, customerProfileServerName))
            .includeAccessToken(Boolean.TRUE)
            .build();
    // invoke customer detail endpoint
    ResponseEntity<?> responseEntity;
    try {
      responseEntity = invocationService.executeRemoteService(context);
    } catch (Exception e) {
      log.error(ErrorCode.RMA_ERR000022.name(), e);
      throw new BaseServiceException(ErrorCode.RMA_ERR000022.name(), Collections.emptyList());
    }

    // parse data
    try {
      String response = new Gson().toJson(responseEntity.getBody(), Map.class);
      return new ObjectMapper().readTree(response).path("body").path("email").asText();
    } catch (Exception e) {
      log.warn(ErrorCode.RMA_ERR000022.name(), e);
      return null;
    }
    */
	  return null;
  }

  /** {@inheritDoc} */
  @Override
  public CustomerDTO getCustomerDetails(String userId) {
	  
	  /*
    RemoteInvocationContext<?> context =
        RemoteInvocationContext.builder()
            .cynEvent(CYN_EVENT_CUSTOMER_RETRIEVE)
            .returnType(Object.class)
            .requestBody(getRequestBody(userId))
            .httpMethod(HttpMethod.POST)
            .url(String.format(CUSTOMER_PROFILE_RETRIEVE_URL, customerProfileServerName))
            .includeAccessToken(Boolean.TRUE)
            .build();
    ResponseEntity<?> responseEntity;
    try {
      responseEntity = invocationService.executeRemoteService(context);
    } catch (Exception e) {
      log.error(ErrorCode.RMA_ERR000029.name(), e);
      throw new BaseServiceException(ErrorCode.RMA_ERR000029.name(), Collections.emptyList());
    }

    // parse data
    try {
      String response = new Gson().toJson(responseEntity.getBody(), Map.class);
      ObjectMapper objectMapper = new ObjectMapper();
      JsonNode customerName = objectMapper.readTree(response).path("body").path("customerName");
      return objectMapper.convertValue(customerName, CustomerDTO.class);
    } catch (Exception e) {
      log.warn(ErrorCode.RMA_ERR000029.name(), e);
      return null;
    }
    */
    return null;
  }

  /**
   * get manager details from contact server
   *
   * @param userId : user Id
   * @return email found
   */
  @Override
  public ManagerDTO getManager(String userId) {
	  
	  /*
    RemoteInvocationContext<?> context =
        RemoteInvocationContext.builder()
            .cynEvent(CYN_EVENT_MANAGER)
            .returnType(Object.class)
            .requestBody(getRequestBody(userId))
            .httpMethod(HttpMethod.POST)
            .url(String.format(MANAGER_DETAIL_RETRIEVE_URL, rmContactServerName))
            .includeAccessToken(Boolean.TRUE)
            .build();
    try {
      ResponseEntity<?> responseEntity = invocationService.executeRemoteService(context);
      var managerBody =
          JsonUtils.OBJECT_MAPPER.convertValue(responseEntity.getBody(), GENERIC_TYPE);
      ManagerDTO manager = managerBody.getBody();
      if (manager == null || StringUtils.isEmpty(manager.getEmail())) {
        throw new BaseServiceException(ErrorCode.RMA_ERR000022.name(), Collections.emptyList());
      }
      return manager;
    } catch (Exception e) {
      log.error(ErrorCode.RMA_ERR000022.name(), e);
      throw new BaseServiceException(ErrorCode.RMA_ERR000022.name(), Collections.emptyList());
    }
    */
    return null;
  }

  /**
   * Creates service body.
   *
   * @param userId the user id
   * @return the body
   */
  private Map<String, String> getRequestBody(String userId) {
	  
	  /*
    Map<String, String> body = new HashMap<>();
    String customerUniqueIdentifier =
        SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
    if (StringUtils.isEmpty(customerUniqueIdentifier))
      throw new BaseServiceException(ErrorCode.RMA_ERR000022.name(), Collections.emptyList());
    body.put("customerUniqueIdentifier", customerUniqueIdentifier);
    return body;
    */
    return null;
  }
}
