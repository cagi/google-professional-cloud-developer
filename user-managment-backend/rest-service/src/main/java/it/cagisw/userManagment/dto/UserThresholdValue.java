package it.cagisw.userManagment.dto;

import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** User threshold value structure for an event. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserThresholdValue {

  /** Unique identifier of the threshold. */
  private long thresholdId;

  /** Currency definition of the threshold. */
  private Currency currency;

  /** Value of the threshold. */
  private long thresholdValue;

  /** When threshold is created. */
  private ZonedDateTime createdOn;

  /** When threshold is updated. */
  private ZonedDateTime updatedOn;
}
