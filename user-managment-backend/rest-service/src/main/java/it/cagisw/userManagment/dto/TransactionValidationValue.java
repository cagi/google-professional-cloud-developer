package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Value structure for data field. */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionValidationValue {

  /** Text of the data field value. */
  @NotBlank private String text;
}
