package it.cagisw.userManagment.dto;

/** Activation status enum for registration. */
public enum ActivationStatus {
  UNKNOWN,
  PENDING,
  ACTIVATED,
  TIMEOUT
}
