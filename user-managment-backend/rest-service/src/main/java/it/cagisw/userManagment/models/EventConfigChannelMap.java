package it.cagisw.userManagment.models;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/** POJO representing a channel preference for a customer. */
@Entity
@Data
@Table(name = "EVENT_CONFIG_CHANNEL_MAP")
public class EventConfigChannelMap {

  /** Primary key */
  @Id
  @GeneratedValue
  @Column(name = "CHANNEL_MAP_ID")
  private long channelMapId;

  /** Foreign key to CustomerEventConfiguration entity. */
  @ManyToOne
  @JoinColumn(name = "EVENT_CONF_ID")
  private CustomerEventConfiguration eventConf;

  /** Foreign key to Channel entity. */
  @ManyToOne
  @JoinColumn(name = "CHANNEL_ID")
  private Channel channel;

  /** Flag to indicate if mapping active. */
  @Column(name = "ACTIVE")
  private boolean active;

  /** Created on audit field. */
  @Column(name = "CREATED_ON")
  private ZonedDateTime createdOn;

  /** Updated on audit field. */
  @Column(name = "UPDATED_ON")
  private ZonedDateTime updatedOn;
}
