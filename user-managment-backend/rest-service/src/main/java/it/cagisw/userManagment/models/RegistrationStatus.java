package it.cagisw.userManagment.models;

/** Enumeration for registration status. */
public enum RegistrationStatus {

  /** User registration was initiated but not completed. */
  PENDING,

  /** User is registered and activated. */
  REGISTERED,

  /** User is unregistered from Onespan. */
  UNREGISTERED,

  /** User is de-registered from Onespan. */
  INVALIDATED,

  /** User is de-registered from Onespan and authenticator deleted from onespan. */
  EXPIRED,
}
