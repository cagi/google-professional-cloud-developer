package it.cagisw.userManagment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventGroup {

  /** A master event group id. */
  private long eventGroupId;

  /** A master event group name. */
  private String groupName;

  /** Event group that triggers event group. */
  @JsonProperty("eventGroup")
  private String eventGroupName;

  /** Description of the group. */
  private String description;
}
