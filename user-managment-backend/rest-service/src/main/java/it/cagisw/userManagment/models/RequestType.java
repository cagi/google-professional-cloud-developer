package it.cagisw.userManagment.models;

/** Enumeration for request types. */
public enum RequestType {
  /** Evaluate log record. */
  EVALUATE,

  /** Notification log record. */
  NOTIFICATION
}
