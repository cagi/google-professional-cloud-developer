package it.cagisw.userManagment.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.text.MessageFormat;

/** Enum for orchestration delivery. */
public enum OrchestrationDeliveryEnum {

  /** Login with fingerprint */
  PUSH_NOTIFICATION("pushNotification"),

  /** Login with faceid */
  REQUEST_MESSAGE("requestMessage");

  /** Actual enum value */
  private static final String ERROR_PATTERN = "{0} value is not correct for orchestration delivery";

  /** Actual enum value */
  private String value;

  /** Thread local formatters */
  private static ThreadLocal<MessageFormat> threadLocalMessageFormat =
      ThreadLocal.withInitial(() -> new MessageFormat(ERROR_PATTERN));

  /**
   * Constuctor for enum
   *
   * @param value enum value
   */
  OrchestrationDeliveryEnum(String value) {
    this.value = value;
  }

  /**
   * Get the actual value of the enum.
   *
   * @return
   */
  @JsonValue
  public String getValue() {
    return value;
  }

  /**
   * Convert enum to string
   *
   * @return string representation of enum
   */
  @Override
  public String toString() {
    return String.valueOf(value);
  }

  /**
   * Create enum from string value
   *
   * @param text string value
   * @return PassKey instance.
   */
  @JsonCreator
  public static OrchestrationDeliveryEnum fromValue(String text) {
    for (OrchestrationDeliveryEnum b : OrchestrationDeliveryEnum.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    throw new IllegalArgumentException(threadLocalMessageFormat.get().format(text));
  }
}
