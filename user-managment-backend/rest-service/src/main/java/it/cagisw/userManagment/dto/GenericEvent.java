package it.cagisw.userManagment.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.core.ResolvableType;
import org.springframework.core.ResolvableTypeProvider;

/** Generic event for async events. */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GenericEvent<T> implements ResolvableTypeProvider {

  /** Event source ex. service name */
  private String source;

  /** Event payload/message; */
  private T message;

  /** Session info of event */
  private SessionInfo sessionInfo;

  /** Device info in case user accessing via device. */
  private DeviceInfo deviceInfo;

  /** Timestamp of the event. */
  private Instant timestamp;

  /** Type of tje event. */
  private String type;

  /** Unique customer id of the event. */
  private String customerUniqueIdentifier;

  /** Success flag of the event. */
  private boolean success;

  /** Failure reason of the event if not successful. */
  private String failureReason;

  /** Ip of the user. */
  private String ipAddress;

  /** Channel that user accesses. */
  private String channel;

  /**
   * Return resolvable type of the event.
   *
   * @return ResolvableType instance with type information.
   */
  @Override
  @JsonIgnore
  public ResolvableType getResolvableType() {
    return ResolvableType.forClassWithGenerics(getClass(), ResolvableType.forInstance(message));
  }

  /**
   * Construct a new generic event
   *
   * @param message event message.
   */
  public GenericEvent(T message) {
    this.message = message;
  }
}
