package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.cagisw.userManagment.dto.CrontoCode;

/** The spring data repository for cronto codes. */
@Repository
public interface CrontoCodeRepository extends CrudRepository<CrontoCode, Long> {}
