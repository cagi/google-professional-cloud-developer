package it.cagisw.userManagment.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.cagisw.userManagment.dto.Event;
import it.cagisw.userManagment.models.Channel;
import it.cagisw.userManagment.models.EventChannelMap;

/** Repository for storing EventChannelMaps. */
public interface EventChannelMapRepository extends JpaRepository<EventChannelMap, Long> {

  /**
   * Find all channel mappings.
   *
   * @param channel channel to search.
   * @param event event to search.
   * @return optional event channel map.
   */
  Optional<EventChannelMap> findByChannelAndEvent(Channel channel, Event event);
}
