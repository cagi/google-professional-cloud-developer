package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Manager DTO */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ManagerDTO {

  /** Id of the row */
  private long id;

  /** email of RM */
  @NotNull private String email;
}
