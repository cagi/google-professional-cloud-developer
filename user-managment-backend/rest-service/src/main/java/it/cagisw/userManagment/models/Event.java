package it.cagisw.userManagment.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/** POJO representing an event master record. */
@Entity
@Table(name = "EVENTS")
@Data
public class Event {

  /** Primary Key */
  @Id
  @GeneratedValue
  @Column(name = "EVENT_ID")
  private long eventId;

  /** Foreign key to event group entity */
  @ManyToOne
  @JoinColumn(name = "EVENT_GROUP_ID")
  private EventGroup eventGroup;

  /** Foreign key to event type entity */
  @ManyToOne
  @JoinColumn(name = "EVENT_TYPE_ID")
  private EventType eventType;

  /** Name of the event */
  @Column(name = "EVENT")
  private String eventName;

  /** Description of the event */
  @Column(name = "EVENT_DESCRIPTION")
  private String eventDescription;

  /** Url of the event image */
  @Column(name = "IMAGE_URL")
  private String imageUrl;

  /** Active flag of the event. */
  @Column(name = "ACTIVE")
  private boolean active;
}
