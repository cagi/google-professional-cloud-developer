package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Object carrying appointment create/update appointment details */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentCancelRequestDTO {

  /** Appointment cancellation reason. */
  private String cancelationReason;
}
