package it.cagisw.userManagment.dto;

import org.springframework.beans.BeanUtils;

import it.cagisw.userManagment.models.User;

public class UserDTO {

	private int id;

	private String username;

	private String fullName;

	private String emailAddress;

	private String status;

	private String password;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public static UserDTO fromModel(User model) {
		if (model == null) {
			return null;
		}

		UserDTO dto = new UserDTO();
		BeanUtils.copyProperties(model, dto);
		return dto;
	}
}
