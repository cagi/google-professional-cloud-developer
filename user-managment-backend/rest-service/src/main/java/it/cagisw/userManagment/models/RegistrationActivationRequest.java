package it.cagisw.userManagment.models;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/** A Pojo representing a registration activation request for a user. */
@Entity
@Data
@Table(name = "REGISTRATION_ACTIVATION_REQUESTS")
public class RegistrationActivationRequest {

  /** Primary Key */
  @Column(name = "REG_REQ_ID")
  @Id
  @GeneratedValue
  private long regReqId;

  /** Customer unique identifier. */
  @Column(name = "CUSTOMERUNIQUEIDENTIFIER")
  private String customerUniqueIdentifier;

  /** Username that is being activated. */
  @Column(name = "USERNAME")
  private String username;

  /** Onespan domain that user is being activated. */
  @Column(name = "DOMAIN")
  private String domain;

  /** Channel of the activation request. */
  @Column(name = "CHANNELCODE")
  private String channelCode;

  /** Ip address of the client. */
  @Column(name = "CLIENT_IP")
  private String clientIp;

  /** Status of the activation code validation. */
  @Column(name = "ACTIVATION_CODE_VALIDATED")
  private boolean activationCodeValidated;

  /** Created on audit field. */
  @Column(name = "CREATED_ON")
  private ZonedDateTime createdOn;

  /** Updated on audit field. */
  @Column(name = "UPDATED_ON")
  private ZonedDateTime updatedOn;
}
