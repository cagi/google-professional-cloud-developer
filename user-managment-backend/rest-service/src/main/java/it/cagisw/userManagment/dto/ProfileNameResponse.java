package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Response with name details of the customer. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProfileNameResponse {

  /** Name details of the customer */
  @NotNull private CustomerName customerName;
}
