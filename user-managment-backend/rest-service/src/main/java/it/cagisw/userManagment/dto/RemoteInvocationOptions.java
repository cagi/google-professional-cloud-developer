package it.cagisw.userManagment.dto;

import lombok.Builder;
import lombok.Data;

/** Options for remote invocation */
@Data
@Builder
public class RemoteInvocationOptions {

  /** Enable rethrow of remote errors based on CYN exception handling rules */
  private boolean rethrowRemoteServiceError;
}
