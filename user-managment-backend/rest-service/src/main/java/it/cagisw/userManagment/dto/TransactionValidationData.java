package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Data holder for transaction validation. */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionValidationData {

  private TransactionStandard standard;

  /** Transaction validation message. */
  private TransactionMessage transactionMessage;
}
