package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Legacy EQ postal address POJO. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LegacyEqPostalAddress {

  /** EQ address 1 */
  private String address1;

  /** EQ address 2 */
  private String address2;

  /** EQ address 3 */
  private String address3;

  /** EQ address 4 */
  private String address4;

  /** EQ address 5 */
  private String address5;

  /** Post code */
  private String postCode;

  /** Indicates if it's active */
  private Boolean active = true;
}
