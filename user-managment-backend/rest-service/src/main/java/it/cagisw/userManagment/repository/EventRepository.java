package it.cagisw.userManagment.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.cagisw.userManagment.dto.Event;
import it.cagisw.userManagment.dto.EventGroup;

/** Repository for storing master event definitions. */
public interface EventRepository extends JpaRepository<Event, Long> {

  /**
   * Find an active event on master records.
   *
   * @param event event name.
   * @param active active or not
   * @return optional might contain event master record.
   */
  Optional<Event> findOneByEventNameAndActive(String event, boolean active);

  /**
   * Retrieve all events for an event group.
   *
   * @param eventGroup event group instance to use in query.
   * @return list of event master records.
   */
  List<Event> findAllByEventGroup(EventGroup eventGroup);
}
