package it.cagisw.userManagment.dto;

import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
/** The request api structure for transmission of a message into channels */
public class ChannelTransmissionRequest {

  /** Subject of the message */
  @NotNull private String subject;

  /** Customer identifier */
  @NotNull private String customerUniqueIdentifier;

  /** Flag to indicate if the channel is templated */
  @NotNull private boolean templatized;

  /** The actual message */
  @NotNull private String message;

  /** The event of the request. */
  private String event;

  /** List of channels to transmit message */
  @NotNull private List<Channel> channels;

  /** List of channels to transmit message */
  @NotNull private List<Param> params;
}
