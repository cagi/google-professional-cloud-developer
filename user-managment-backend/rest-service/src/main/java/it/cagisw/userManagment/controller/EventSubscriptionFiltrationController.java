package it.cagisw.userManagment.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.api.Status;
import it.cagisw.userManagment.dto.CaptureNotification;
import it.cagisw.userManagment.dto.EventNotification;
import it.cagisw.userManagment.dto.EventNotificationResponse;
import it.cagisw.userManagment.service.interfaces.EventSubscriptionService;

/** The entry point for event subscription requests.. */
@RestController
public class EventSubscriptionFiltrationController {

  /** Service responsible for business logic. */
  private final EventSubscriptionService eventSubscriptionService;

  /**
   * The constructor for controller to be created by Spring.
   *
   * @param eventSubscriptionService the service implementation to be injected by Spring..
   */
  public EventSubscriptionFiltrationController(EventSubscriptionService eventSubscriptionService) {
    this.eventSubscriptionService = eventSubscriptionService;
  }

  /**
   * Filter events and send message to channels.
   *
   * @param requestData the message data.
   * @param cynEvent the event triggered the message.
   * @return status of the operation..
   */
  @PostMapping("/subscription/evaluate")
  public GenericResponse<EventNotificationResponse> evaluate(
      @Valid @RequestBody final EventNotification requestData,
      @RequestHeader("x-cyn-event") String cynEvent) {
    return new GenericResponse<>(eventSubscriptionService.evaluate(requestData));
  }

  /**
   * Filter events and send message to channels.
   *
   * @param requestData the message data.
   * @param cynEvent the event triggered the message.
   * @return status of the operation..
   */
  @PostMapping("/history/capture")
  public GenericResponse<Status> capture(
      @Valid @RequestBody final CaptureNotification requestData,
      @RequestHeader("x-cyn-event") String cynEvent) {
    return new GenericResponse<>(eventSubscriptionService.capture(requestData));
  }
}
