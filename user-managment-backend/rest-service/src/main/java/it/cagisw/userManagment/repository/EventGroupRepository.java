package it.cagisw.userManagment.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.cagisw.userManagment.dto.EventGroup;

/** Repository for storing EventGroups. */
public interface EventGroupRepository extends JpaRepository<EventGroup, Long> {
  /**
   * Find event group model with a group name.
   *
   * @param groupName name of the group.
   * @return optional might contain event group.
   */
  Optional<EventGroup> findFirstByGroupName(String groupName);
}
