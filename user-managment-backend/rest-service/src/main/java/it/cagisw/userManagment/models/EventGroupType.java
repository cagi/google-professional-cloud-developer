package it.cagisw.userManagment.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/** POJO representing an event group master record. */
@Data
@Entity
@Table(name = "EVENT_GROUP_TYPE")
public class EventGroupType {

  /** Primary key */
  @Id
  @GeneratedValue
  @Column(name = "EVENT_GROUP_TYPE_ID")
  private long eventGroupTypeId;

  /** Name of the event group type */
  @Column(name = "GROUP_TYPE_NAME")
  private String groupTypeName;
}
