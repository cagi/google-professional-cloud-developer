package it.cagisw.userManagment.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.dto.ManagerSlotDTO;
import it.cagisw.userManagment.dto.ManagerSlotRequestDTO;
import it.cagisw.userManagment.dto.SlotAvailabilityDTO;
import it.cagisw.userManagment.service.interfaces.ManagerSlotService;

/** Manager slots controller. */
@RestController
@RequestMapping("/manager-slots")
public class ManagerSlotController {

  /** Auto injected ManagerSlotService instance */
  @Autowired ManagerSlotService slotService;

  /**
   * Creates a manager slot
   *
   * @param slotDTO object carrying slot properties
   * @return created manager slot
   */
  @PostMapping
  public GenericResponse<ManagerSlotDTO> createManagerSlot(
      @Valid @RequestBody ManagerSlotRequestDTO slotDTO) {

    return new GenericResponse<>(slotService.createManagerSlot(slotDTO));
  }

  /**
   * Get all manager slots available in DB
   *
   * @param managerId optional filter the manager id
   * @return list of found manager slots
   */
  @GetMapping
  public GenericResponse<List<ManagerSlotDTO>> getAllManagerSlots(
      @RequestParam Optional<Long> managerId) {

    return new GenericResponse<>(slotService.findAll(managerId));
  }

  /**
   * Get manager slot by its ID
   *
   * @param slotId ID of the manager slot
   * @return found manager slot
   */
  @GetMapping("/{slotId}")
  public GenericResponse<ManagerSlotDTO> getManagerSlotById(@PathVariable("slotId") long slotId) {

    return new GenericResponse<>(slotService.findById(slotId));
  }

  /**
   * Updates a manager slot
   *
   * @param slotId ID of the manager slot
   * @param slotDTO object carrying updated slot data
   * @return updated manager slot
   */
  @PutMapping("/{slotId}")
  public GenericResponse<ManagerSlotDTO> updateManagerSlot(
      @PathVariable("slotId") long slotId, @Valid @RequestBody ManagerSlotRequestDTO slotDTO) {

    return new GenericResponse<>(slotService.updateManagerSlot(slotId, slotDTO));
  }

  /**
   * Delete a manager slot by its ID
   *
   * @param slotId ID of the manager slot to be delted
   */
  @DeleteMapping("/{slotId}")
  public GenericResponse deleteManagerSlotById(@PathVariable("slotId") long slotId) {

    slotService.deleteManagerSlot(slotId);
    return new GenericResponse();
  }

  /**
   * Get a manager's slots availability info for a particular day
   *
   * @param managerId the manager id
   * @param day date of the day to query the availability (in yyyy-MM-dd format)
   * @return list of found manager slots
   */
  @GetMapping("/manager")
  public GenericResponse<SlotAvailabilityDTO> getManagerSlotsAvailability(
      @RequestParam String day,
      @RequestParam(required = false) Long managerId,
      @RequestParam(required = false) String managerEmail) {

    return new GenericResponse<>(slotService.findSlotsAvailability(managerId, managerEmail, day));
  }
}
