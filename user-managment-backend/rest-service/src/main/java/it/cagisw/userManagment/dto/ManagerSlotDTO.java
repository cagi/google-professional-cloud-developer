package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Manager slot DTO */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ManagerSlotDTO {

  /** Id of the row */
  private long id;

  /** Start time of manager slot in secs */
  private int startTime;

  /** End time of manager slot in secs */
  private int endTime;

  /** Slot duration */
  private int slotDuration;

  /** Manager Id */
  private long managerId;

  /** Manager Email */
  private String managerEmail;
}
