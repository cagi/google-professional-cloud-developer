package it.cagisw.userManagment.models;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/** A Pojo representing an cronto code render request. */
@Entity
@Data
@Table(name = "CRONTO_CODE_RENDER_REQUEST")
public class CrontoRenderRequest {

  /** Unique key */
  @Column(name = "CRONTO_CODE_ID")
  @Id
  @GeneratedValue
  private long crontoCodeId;

  /** Unique customer id. */
  @Column(name = "CUSTOMERUNIQUEIDENTIFIER")
  private String customerUniqueIdentifier;

  /** Cronto render request unique identifier. */
  @Column(name = "CCRREQUESTUUID")
  private String ccrRequestUuid;

  /** The channel that is rendering cronto. */
  @Column(name = "CHANNEL_CODE")
  private String channelCode;

  /** The invoking component */
  @Column(name = "INVOKING_COMPONENT")
  private String invokingComponent;

  /** Created audit field */
  @Column(name = "CREATED_ON")
  private ZonedDateTime createdOn;

  /** Updated audit field. */
  @Column(name = "UPDATED_ON")
  private ZonedDateTime updatedOn;
}
