package it.cagisw.userManagment.service;

import java.util.Base64;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cagisw.userManagment.exceptions.BaseServiceException;
import it.cagisw.userManagment.models.CustomerProfile;
import it.cagisw.userManagment.service.interfaces.CryptoService;

/** Service to perform encryption/decryption. */
@Service
public class CryptoServiceImpl implements CryptoService {

  /** Key to encrypt/decrypt data */
  private final String encryptionKey;

  /** Jackson object mapper */
  private final ObjectMapper objectMapper;

  /**
   * Create CryptoServiceImpl new instance.
   *
   * @param encryptionKey key to encrypt/decrypt data
   * @param objectMapper Jackson object mapper
   */
  public CryptoServiceImpl(
      @Value("${pii.enc.key}") final String encryptionKey, final ObjectMapper objectMapper) {
    this.encryptionKey = encryptionKey;
    this.objectMapper = objectMapper;
  }

  /**
   * Encrypts sensitive information for a CustomerProfile entity.
   *
   * @param customerProfile the entity
   * @return the encrypted entity
   */
  @Override
  public String encryptCustomerProfile(final CustomerProfile customerProfile) {
    try {
      var customerProfileJson = objectMapper.writeValueAsString(customerProfile);
      return encryptString(customerProfileJson);
    } catch (Exception e) {
      throw new BaseServiceException("CPR_ERR000006.name()", Collections.emptyList(), e);
    }
  }

  /**
   * Decrypts the sensitive information of an encrypted CustomerProfile entity.
   *
   * @param encryptedCustomerProfile encrypted JSON string base64
   * @return decrypted entity
   */
  @Override
  public CustomerProfile decryptCustomerProfile(final String encryptedCustomerProfile) {
    var customerProfileJson = decryptString(encryptedCustomerProfile);
    try {
      return objectMapper.readValue(customerProfileJson, CustomerProfile.class);
    } catch (JsonProcessingException e) {
      throw new BaseServiceException("CPR_ERR000007.name()", Collections.emptyList(), e);
    }
  }

  /**
   * Decrypts data of type string.
   *
   * @param data encrypted data to decrypt
   * @return decrypted data
   */
  public String decryptString(String data) {
    try {
      if (data == null) return null;
      return new String();
    } catch (Exception e) {
      throw new BaseServiceException("CPR_ERR000007.name()", Collections.emptyList(), e);
    }
  }

  /**
   * Encrypts data of type string.
   *
   * @param data data to encrypt
   * @return base64 encrypted string
   */
  public String encryptString(String data) {
    try {
      if (data == null) return null;
      return "";
    } catch (Exception e) {
      throw new BaseServiceException("CPR_ERR000006.name()", Collections.emptyList(), e);
    }
  }
}
