package it.cagisw.userManagment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.cagisw.userManagment.models.Channel;

/** Repository for storing master Channel definitions. */
public interface ChannelRepository extends JpaRepository<Channel, Long> {}
