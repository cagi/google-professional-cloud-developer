package it.cagisw.userManagment.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/** POJO representing event map for a specific customer. */
@Data
@Entity
@Table(name = "EVENT_MAP")
public class EventMap {

  /** Primary key */
  @Id
  @GeneratedValue
  @Column(name = "EVENT_MAP_ID")
  private long eventMapId;

  /** Foreign key to CustomerEventConfiguration entity. */
  @ManyToOne
  @JoinColumn(name = "EVENT_CONF_ID")
  private CustomerEventConfiguration eventConf;

  /** Event field */
  @ManyToOne
  @JoinColumn(name = "EVENT_ID")
  private Event event;

  /** Foreign key to CustomerEventConfiguration entity. */
  @ManyToOne
  @JoinColumn(name = "ACCOUNT_ID")
  private CustomerAccount customerAccount;

  /** Flag to indicate if mapping active. */
  @Column(name = "ACTIVE")
  private boolean active;
}
