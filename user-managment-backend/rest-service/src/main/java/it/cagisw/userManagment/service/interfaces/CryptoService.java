package it.cagisw.userManagment.service.interfaces;

import it.cagisw.userManagment.models.CustomerProfile;

/** Service to perform encryption/decryption. */
public interface CryptoService {

  /**
   * Encrypts sensitive information for a CustomerProfile entity.
   *
   * @param customerProfile the entity
   * @return the encrypted entity
   */
  String encryptCustomerProfile(CustomerProfile customerProfile);

  /**
   * Decrypts the sensitive information of an encrypted CustomerProfile entity.
   *
   * @param encryptedCustomerProfile encrypted JSON string base64
   * @return decrypted entity
   */
  CustomerProfile decryptCustomerProfile(String encryptedCustomerProfile);
}
