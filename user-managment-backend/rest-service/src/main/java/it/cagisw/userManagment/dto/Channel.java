package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** The representation of a channel. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Channel {

  /** Unique channel identifier. */
  @NotNull private Long channelId;

  /** Default activation value of channel. */
  @NotNull private boolean active;

  /** Name of the channel. */
  private String channelName;

  /** Contact information for channel email address or phone number etc.. */
  private String contactInfo;
}
