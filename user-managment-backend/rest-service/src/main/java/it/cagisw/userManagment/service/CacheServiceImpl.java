package it.cagisw.userManagment.service;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.cagisw.userManagment.property.CacheProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.dto.RemoteInvocationContext;
import it.cagisw.userManagment.exceptions.BaseServiceException;
import it.cagisw.userManagment.models.CustomerProfile;
import it.cagisw.userManagment.service.interfaces.CacheService;
import it.cagisw.userManagment.service.interfaces.CryptoService;
import lombok.extern.slf4j.Slf4j;

/** Service to perform operations with cache server. */
@Service
@Slf4j
public class CacheServiceImpl implements CacheService {

  /** Double quotes char */
  private static final String DOUBLE_QUOTES_CHAR = "\"";

  /** Empty string */
  private static final String EMPTY_STRING = "";

  /** First index */
  private static final int FIRST_INDEX = 0;

  /** Object mapper */
  private final ObjectMapper objectMapper;

  /** Properties for cache operations */
  private final CacheProperties cacheProperties;

  /** Local properties for cache */
  private final LocalCacheProperties localCacheProperties;

  /** Service to encrypt and decrypt sensitive information */
  private final CryptoService cryptoService;

  /**
   * Create new CacheService.
   *
   * @param localCacheProperties local cache properties
   * @param remoteInvocationService Service to perform invocation between cyn services
   * @param objectMapper Object mapper
   * @param cacheProperties Properties for cache operations
   * @param cryptoService Service to encrypt and decrypt sensitive information
   */
  public CacheServiceImpl(
      final LocalCacheProperties localCacheProperties,
      final RemoteInvocationService remoteInvocationService,
      final ObjectMapper objectMapper,
      final CacheProperties cacheProperties,
      final CryptoService cryptoService) {
    this.localCacheProperties = localCacheProperties;
    this.remoteInvocationService = remoteInvocationService;
    this.objectMapper = objectMapper;
    this.cacheProperties = cacheProperties;
    this.cryptoService = cryptoService;
  }

  /**
   * Retrieve the customer profile from cache.
   *
   * @param cui customer unique identifier
   * @return an optional customer profile
   */
  @Override
  public Optional<CustomerProfile> getCustomerProfile(String cui) {
    try {
      GenericResponse<CacheEntryResponse> cacheResponse =
          get(
              String.format(localCacheProperties.getProfileKeyFormat(), cui),
              new TypeReference<>() {});
      var customerProfile =
          cryptoService.decryptCustomerProfile(
              cacheResponse.getBody().getValue().replace(DOUBLE_QUOTES_CHAR, EMPTY_STRING));
      return Optional.of(customerProfile);
    } catch (Exception e) {
      log.error("error when try to get customer profile", e);
      return Optional.empty();
    }
  }

  /**
   * Save the profile for a customer.
   *
   * @param customerProfile customer profile
   */
  @Override
  public void saveCustomerProfile(CustomerProfile customerProfile) {
    save(
        String.format(
            localCacheProperties.getProfileKeyFormat(),
            customerProfile
                .getCanonicalIdMapEntity()
                .getCustomerUniqueIdentifier()
                .get(FIRST_INDEX)),
        localCacheProperties.getProfileTtl(),
        cryptoService.encryptCustomerProfile(customerProfile));
  }

  /**
   * Saves an object with the key specified.
   *
   * @param key Set this key in cache
   * @param object To save in cache
   */
  private void save(String key, long ttl, Object object) {
    try {
      var request = new CacheEntryRequest();
      request.setGroupId(localCacheProperties.getGroupIdValue());
      request.setKey(key);
      request.setTtl(ttl);
      request.setValue(objectMapper.writeValueAsString(object));
      remoteInvocationService.executeRemoteService(
          RemoteInvocationContext.builder()
              .httpMethod(HttpMethod.POST)
              .includeAccessToken(true)
              .url(cacheProperties.getCreateUri())
              .returnType(Object.class)
              .cynEvent(cacheProperties.getCynEventCreate())
              .requestBody(request)
              .build());
    } catch (Exception e) {
      throw new BaseServiceException(CPR_ERR000009.name(), Collections.emptyList(), e);
    }
  }

  /**
   * Gets an object from cache
   *
   * @param key Key of object in cache
   * @param typeReference This is needed to infer the type during deserialization
   * @param <T> Type of object to return
   * @return Object in cache
   */
  private <T> T get(String key, TypeReference<T> typeReference) {
    ResponseEntity<Object> response;
    try {
      response =
          remoteInvocationService.executeRemoteService(
              RemoteInvocationContext.builder()
                  .headers(
                      Map.of(
                          cacheProperties.getHeaderNameGroupId(),
                          localCacheProperties.getGroupIdValue(),
                          cacheProperties.getHeaderNameKey(),
                          key))
                  .httpMethod(HttpMethod.GET)
                  .includeAccessToken(true)
                  .url(cacheProperties.getRetrieveUri())
                  .returnType(Object.class)
                  .cynEvent(cacheProperties.getCynEventRetrieve())
                  .build());

      return objectMapper.convertValue(response.getBody(), typeReference);
    } catch (Exception e) {
      throw new BaseServiceException(CPR_ERR000009.name(), Collections.emptyList(), e);
    }
  }
}
