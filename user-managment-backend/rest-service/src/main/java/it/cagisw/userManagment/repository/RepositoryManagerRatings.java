package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;

import it.cagisw.userManagment.models.ManagerRatings;

public interface RepositoryManagerRatings extends CrudRepository<ManagerRatings, Integer>{

}
