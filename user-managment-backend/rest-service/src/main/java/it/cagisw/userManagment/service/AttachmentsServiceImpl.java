package it.cagisw.userManagment.service;

import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cagisw.userManagment.commons.exception.BaseNotFoundException;
import com.cagisw.userManagment.commons.exception.ErrorCode;

import it.cagisw.userManagment.dto.AttachmentDTO;
import it.cagisw.userManagment.dto.AttachmentRequestDTO;
import it.cagisw.userManagment.models.AppointmentAttachments;
import it.cagisw.userManagment.models.Appointments;
import it.cagisw.userManagment.repository.AppointmentAttachmentsRepository;
import it.cagisw.userManagment.repository.AppointmentsRepository;
import it.cagisw.userManagment.service.interfaces.AttachmentsService;
import it.cagisw.userManagment.service.interfaces.StorageService;

/** Implementation class for {@link AttachmentsService} */
@Service
public class AttachmentsServiceImpl implements AttachmentsService {

  /** CYN Service user name */
  private static final String CYN_SERVICE_NAME = "cyn_service";

  /** Auto injected AppointmentAttachmentsRepository instance */
  @Autowired private AppointmentAttachmentsRepository attachmentsRepo;

  /** Auto injected AppointmentsRepository instance */
  @Autowired private AppointmentsRepository appointmentsRepo;

  /** Auto injected StorageService instance */
  @Autowired private StorageService storageService;

  /**
   * Create new appointment attachment
   *
   * @param appointmentId Id of the appointment uploaded attachments belong to
   * @param requestDTO request object containing attachment details
   * @param customerUniqueId the logged-in unique id
   * @return created attachment
   */
  @Override
  public AttachmentDTO createNewAttachment(
      long appointmentId, AttachmentRequestDTO requestDTO, String customerUniqueId) {
    /*Appointments appointments = validateAndGetAppointment(appointmentId, customerUniqueId);
    String fileName = requestDTO.getFileName();
    try {
      byte[] data = Base64.decodeBase64(requestDTO.getContent());
      String url = storageService.storeFile(appointmentId, fileName, data);
      AppointmentAttachments attachments =
          AppointmentAttachments.builder()
              .url(url)
              .fileName(fileName)
              .appointment(appointments)
              .cratedDate(ZonedDateTime.now())
              .build();
      attachmentsRepo.save(attachments);
      return getFromAttachment(attachments);
    } catch (Exception e) {
      throw new BaseServiceException(ErrorCode.RMA_ERR000011.name(), Collections.emptyList());
    }*/
	  return null;
  }

  /**
   * Find attachment by its ID
   *
   * @param attachmentId ID to be found
   * @return found Attachment
   */
  @Override
  public AttachmentDTO findById(long attachmentId, String customerUniqueId) {
    return getFromAttachment(getAttachmentById(attachmentId, customerUniqueId, false));
  }

  /**
   * Delete an attachment
   *
   * @param attachmentId ID of the attachment to be deleted
   * @param customerUniqueId the logged-in unique id
   */
  @Override
  public void deleteAttachment(long attachmentId, String customerUniqueId) {
    AppointmentAttachments attachment = getAttachmentById(attachmentId, customerUniqueId, true);
    storageService.deleteFile(attachment.getAppointment().getId(), attachment.getFileName());
    // attachmentsRepo.delete(attachment);
  }

  /**
   * Find Appointments by Id, and throw exception if not found or not authorised
   *
   * @param id ID of the row to be found
   * @return Found Appointments
   */
  private Appointments validateAndGetAppointment(Long id, String customerUniqueId) {
    Optional<Appointments> found = appointmentsRepo.findById(id);

    if (!found.isPresent()) {
      throw new BaseNotFoundException(ErrorCode.RMA_ERR000004.name(), Collections.emptyList());
    }
    Appointments appointments = found.get();

    // video service will call by creator or manager of appointments
    boolean isManagerOrService = CYN_SERVICE_NAME.equals(customerUniqueId);

    if (!isManagerOrService
        && !appointments.getCustomerUniqueId().equalsIgnoreCase(customerUniqueId)) {
      throw new BaseNotFoundException(ErrorCode.RMA_ERR000009.name(), Collections.emptyList());
    }

    return appointments;
  }

  /**
   * Get AttachmentDTO created out of AppointmentAttachments
   *
   * @param attachments the entity instance
   * @return the populated DTO
   */
  private AttachmentDTO getFromAttachment(AppointmentAttachments attachments) {
    return AttachmentDTO.builder()
        .id(attachments.getId())
        .appointmentId(attachments.getAppointment().getId())
        .fileName(attachments.getFileName())
        .url(attachments.getUrl())
        .build();
  }

  /**
   * Validate and get AppointmentAttachments by ID. Throws exception if object couldn't be found or
   * not authorised
   *
   * @param attachmentId ID of the AppointmentAttachments
   * @param customerUniqueId the logged-in unique id
   * @param checkOwnership whether to check if appointment is for the logged-in user or not
   * @return AppointmentAttachments found by ID
   */
  private AppointmentAttachments getAttachmentById(
      long attachmentId, String customerUniqueId, boolean checkOwnership) {
    /*Optional<AppointmentAttachments> found = attachmentsRepo.findById(attachmentId);
    if (found.isEmpty()) {
      throw new BaseNotFoundException(ErrorCode.RMA_ERR000004.name(), Collections.emptyList());
    }
    AppointmentAttachments attachments = found.get();
    if (checkOwnership
        && !attachments.getAppointment().getCustomerUniqueId().equalsIgnoreCase(customerUniqueId)) {
      throw new BaseNotFoundException(ErrorCode.RMA_ERR000009.name(), Collections.emptyList());
    }
    return attachments;*/
	  return null;
  }
}
