package it.cagisw.userManagment.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import it.cagisw.userManagment.models.ManagerRatings;

/** Repository for ManagerRatings table */
public interface ManagerRatingsRepository extends PagingAndSortingRepository<ManagerRatings, Long> {

  /**
   * Get all manager ratings for a manager
   *
   * @param managerId id of the manager
   * @param pageable pagination attributes
   * @return found list of Manager ratings
   */
  Page<ManagerRatings> findByManagerIdAndRatingIn(
      long managerId, Pageable pageable, List<Integer> ratingList);

  /**
   * Get all manager ratings for a manager
   *
   * @param managerId id of the manager
   * @param pageable pagination attributes
   * @return found list of Manager ratings
   */
  Page<ManagerRatings> findByManagerId(long managerId, Pageable pageable);

  /**
   * Get all manager ratings for a manager in a single response
   *
   * @param managerId id of the manager
   * @return found list of Manager ratings
   */
  List<ManagerRatings> findAllByManagerId(long managerId);

  /**
   * Get manager rating created by current user
   *
   * @param managerId id of the manager
   * @return found list of Manager ratings
   */
  Optional<ManagerRatings> findByCustomerUniqueIdAndManagerId(
      String customerUniqueId, long managerId);
}
