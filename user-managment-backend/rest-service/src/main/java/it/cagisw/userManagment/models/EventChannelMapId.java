package it.cagisw.userManagment.models;

import java.io.Serializable;
import lombok.Data;

/** Id structure of event channel map */
@Data
public class EventChannelMapId implements Serializable {

  /** Serial version of class. */
  private static final long serialVersionUID = -5520563849458069841L;

  /** Event name. */
  private long event;

  /** Channel name. */
  private long channel;
}
