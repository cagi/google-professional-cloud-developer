package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** DeviceInfo structure for generic events. */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DeviceInfo {

  /** Access token. */
  private String deviceId;

  /** Refresh token */
  private Object deviceOs;
}
