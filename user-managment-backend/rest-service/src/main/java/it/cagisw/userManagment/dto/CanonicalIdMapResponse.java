package it.cagisw.userManagment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Response with the canonical id mapping. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CanonicalIdMapResponse {

  /** Party id */
  private long partyId;

  /** CCDS party id */
  private String ccdsPartyId;

  /** Unique identifier of the customer */
  private String customerUniqueIdentifier;

  /** CIF id */
  @JsonProperty("cifid")
  private String cifId;

  /** OLB user id */
  @JsonProperty("olbuserId")
  private String olbUserId;

  /** EQ customer numbers */
  @JsonProperty("eqcustomerNumbers")
  private List<String> eqCustomerNumbers;
}
