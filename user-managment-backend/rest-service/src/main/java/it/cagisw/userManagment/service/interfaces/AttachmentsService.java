package it.cagisw.userManagment.service.interfaces;

import it.cagisw.userManagment.dto.AttachmentDTO;
import it.cagisw.userManagment.dto.AttachmentRequestDTO;

/** Service interface for AppointmentAttachments */
public interface AttachmentsService {

  /**
   * Create new appointment attachment
   *
   * @param appointmentId Id of the appointment uploaded attachments belong to
   * @param requestDTO request object containing attachment details
   * @param customerUniqueId the logged-in unique id
   * @return created attachment
   */
  AttachmentDTO createNewAttachment(
      long appointmentId, AttachmentRequestDTO requestDTO, String customerUniqueId);

  /**
   * Find attachment by its ID
   *
   * @param attachmentId ID to be found
   * @param customerUniqueId the logged-in unique id
   * @return found Attachment
   */
  AttachmentDTO findById(long attachmentId, String customerUniqueId);

  /**
   * Delete an attachment
   *
   * @param attachmentId ID of the attachment to be deleted
   * @param customerUniqueId the logged-in unique id
   */
  void deleteAttachment(long attachmentId, String customerUniqueId);
}
