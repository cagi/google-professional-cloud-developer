package it.cagisw.userManagment.dto;

import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Manager ratings DTO */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ManagerRatingDTO {

  /** Id of the row */
  private long id;

  /** Rating Manager Id */
  private long managerId;

  /** unique identifier of the user who is rating the manager */
  private String customerUniqueId;

  /** Rating value */
  private int rating;

  /** Rating feedback. */
  private String feedback;

  /** Rating created date */
  private ZonedDateTime createdDate;
}
