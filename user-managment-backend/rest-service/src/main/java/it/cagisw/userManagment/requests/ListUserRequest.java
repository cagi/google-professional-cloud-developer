package it.cagisw.userManagment.requests;

import java.util.List;

public class ListUserRequest {
	
	private List<UserRequest> userRequests;

	public List<UserRequest> getUserRequests() {
		return userRequests;
	}

	public void setUserRequests(List<UserRequest> userRequests) {
		this.userRequests = userRequests;
	}
}
