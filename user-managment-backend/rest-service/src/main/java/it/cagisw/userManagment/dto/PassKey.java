package it.cagisw.userManagment.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.text.MessageFormat;

/** Passkey enumeration for login. */
public enum PassKey {

  /** Login with fingerprint */
  FINGERPRINT("Fingerprint"),

  /** Login with faceid */
  FACEID("Face"),

  /** Login with pin */
  PIN("PIN"),

  /** No pin login type */
  NOPIN("NoPIN");

  /** Actual enum value */
  private static final String ERROR_PATTERN = "{0} value is not correct for PassKey";

  /** Actual enum value */
  private String value;

  /** Thread local formatters */
  private static ThreadLocal<MessageFormat> threadLocalMessageFormat =
      ThreadLocal.withInitial(() -> new MessageFormat(ERROR_PATTERN));

  /**
   * Constuctor for enum
   *
   * @param value enum value
   */
  PassKey(String value) {
    this.value = value;
  }

  /**
   * Get the actual value of the enum.
   *
   * @return
   */
  @JsonValue
  public String getValue() {
    return value;
  }

  /**
   * Convert enum to string
   *
   * @return string representation of enum
   */
  @Override
  public String toString() {
    return String.valueOf(value);
  }

  /**
   * Create enum from string value
   *
   * @param text string value
   * @return PassKey instance.
   */
  @JsonCreator
  public static PassKey fromValue(String text) {
    for (PassKey b : PassKey.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    throw new IllegalArgumentException(threadLocalMessageFormat.get().format(text));
  }
}
