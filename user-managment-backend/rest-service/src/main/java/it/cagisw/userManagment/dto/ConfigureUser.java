package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** The representation of a user onboarding request. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfigureUser {

  /** Unique channel identifier. */
  @NotNull private String customerUniqueIdentifier;

  /** Account number for the user. */
  private String accountNumber;
}
