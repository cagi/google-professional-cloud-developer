package it.cagisw.userManagment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.cagisw.userManagment.dto.CustomerEventConfiguration;
import it.cagisw.userManagment.dto.EventConfigChannelMap;
import it.cagisw.userManagment.models.Channel;

/** Repository for storing EventConfigChannelMaps. */
public interface EventConfigChannelMapRepository
    extends JpaRepository<EventConfigChannelMap, Long> {

  /**
   * Find the event config channels by channel and event config.
   *
   * @param channel the channel instance.
   * @param customerEventConfiguration customer configuration instance.
   * @return list of event configurations.
   */
  List<EventConfigChannelMap> findByChannelAndEventConf(
      Channel channel, CustomerEventConfiguration customerEventConfiguration);

  /**
   * Find event config channels with event config and active flag.
   *
   * @param customerEventConfiguration event config instance.
   * @param active active flag to use in filtration.
   * @return list of event configurations.
   */
  List<EventConfigChannelMap> findByEventConfAndActive(
      CustomerEventConfiguration customerEventConfiguration, boolean active);

  /**
   * Find all available channels for an customer event config.
   *
   * @param customerEventConfiguration customer event config instance.
   * @return list of event configs.
   */
  List<EventConfigChannelMap> findByEventConf(
      CustomerEventConfiguration customerEventConfiguration);
}
