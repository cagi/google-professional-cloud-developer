package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Request structure to retrieve registration status. */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationStatusCheck {

  /** User id to check status. */
  @NotEmpty private String userId;

  /** Device binding id. */
  private String bindingId;

  /** Device Identifier. */
  private String deviceIdentifier;

  /** Device operating system. */
  private String deviceOs;
}
