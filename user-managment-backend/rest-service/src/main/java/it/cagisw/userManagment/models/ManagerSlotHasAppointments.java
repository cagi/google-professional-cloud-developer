package it.cagisw.userManagment.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "manager_slot_has_appointments")
@Entity
public class ManagerSlotHasAppointments {

	@Id
	@GeneratedValue
	private int id;
	 
	@ManyToOne()
	@JoinColumn(name = "manager_slot_id", nullable = true)
	private ManagerSlot managerSlot;
	
	@ManyToOne()
	@JoinColumn(name = "appointments_id", nullable = true)
	private Appointments appointments;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Appointments getAppointments() {
		return appointments;
	}
	public void setAppointments(Appointments appointments) {
		this.appointments = appointments;
	}
	public ManagerSlot getManagerSlot() {
		return managerSlot;
	}
	public void setManagerSlot(ManagerSlot managerSlot) {
		this.managerSlot = managerSlot;
	}
}
