package it.cagisw.userManagment.service;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.cagisw.userManagment.property.AccountInformationProperties;
import com.fasterxml.jackson.core.type.TypeReference;

import it.cagisw.userManagment.api.GenericResponse;
import it.cagisw.userManagment.dto.Status;
import it.cagisw.userManagment.dto.AccountEvents;
import it.cagisw.userManagment.dto.CaptureNotification;
import it.cagisw.userManagment.dto.ConfigViewRequest;
import it.cagisw.userManagment.dto.ConfigureUser;
import it.cagisw.userManagment.dto.CustomerEventConfiguration;
import it.cagisw.userManagment.dto.CustomerEventHistory;
import it.cagisw.userManagment.dto.Event;
import it.cagisw.userManagment.dto.EventConfigChannelMap;
import it.cagisw.userManagment.dto.EventFiltrationResponse;
import it.cagisw.userManagment.dto.EventNotification;
import it.cagisw.userManagment.dto.EventNotificationConfigUpdate;
import it.cagisw.userManagment.dto.EventNotificationResponse;
import it.cagisw.userManagment.dto.NewEventInput;
import it.cagisw.userManagment.dto.Pagination;
import it.cagisw.userManagment.exceptions.EntityNotFoundException;
import it.cagisw.userManagment.exceptions.InvalidStatusRequestException;
import it.cagisw.userManagment.models.AlertThresholdValue;
import it.cagisw.userManagment.models.Channel;
import it.cagisw.userManagment.models.CustomerAccount;
import it.cagisw.userManagment.models.CustomerEventLogHistory;
import it.cagisw.userManagment.models.EventChannelMap;
import it.cagisw.userManagment.models.EventDefault;
import it.cagisw.userManagment.models.EventDefaultParam;
import it.cagisw.userManagment.models.EventMap;
import it.cagisw.userManagment.models.RequestType;
import it.cagisw.userManagment.repository.ChannelRepository;
import it.cagisw.userManagment.repository.CustomerAccountRepository;
import it.cagisw.userManagment.repository.CustomerEventConfigurationsRepository;
import it.cagisw.userManagment.repository.CustomerEventHistoryRepository;
import it.cagisw.userManagment.repository.EventChannelMapRepository;
import it.cagisw.userManagment.repository.EventConfigChannelMapRepository;
import it.cagisw.userManagment.repository.EventDefaultRepository;
import it.cagisw.userManagment.repository.EventMapRepository;
import it.cagisw.userManagment.repository.EventRepository;
import it.cagisw.userManagment.repository.HistorySpecification;
import it.cagisw.userManagment.repository.UserThresholdValueRepository;
import it.cagisw.userManagment.service.interfaces.EventSubscriptionService;
import lombok.RequiredArgsConstructor;

/** The main login that performs event subscription related logic.. */
@Service
@RequiredArgsConstructor
public class DatabaseEventSubscriptionService implements EventSubscriptionService {

  /** Return type of account service.. */
  private static final TypeReference<GenericResponse<AccountResponse>> GENERIC_ACCOUNTS_TYPE =
      new TypeReference<>() {};

  /** The type name for account events.. */
  private static final String ACCOUNT_EVENTS = "CUSTOMER_ACCOUNT_ALERT";

  /** The type name payment events.. */
  private static final String CUSTOMER_EVENTS = "CUSTOMER_CONFIG_ALERT";

  /** The type name security events.. */
  private static final String SECURITY_EVENTS = "SECURITY_ALERT_EVENT";

  /** The type name security events.. */
  private static final String MANDATORY_EVENTS = "MANDATORY_ALERT";

  /** JSON field key for account events.. */
  private static final String ACCOUNT_EVENTS_RESPONSE_KEY = "customer_account_alert";

  /** JSON field key for payment events.. */
  private static final String CUSTOMER_CONFIG_ALERT_RESPONSE_KEY = "customer_config_alert";

  /** Default days to query event history. */
  private static final int DEFAULT_HISTORY_QUERY_DAYS = 7;

  /** Delimiter to store channels for history entry. */
  private static final String CHANNELS_DELIMITER = ",";

  /** Name of the created date field for history entity. */
  private static final String CREATED_ON = "createdOn";

  /** Underscore separator. */
  private static final char UNDERSCORE_SEPARATOR = '_';

  /** Space separator. */
  private static final char SPACE_SEPARATOR = ' ';

  /** The repository for customer specific event configurations.. */
  private final CustomerEventConfigurationsRepository customerEventConfigurationsRepository;

  /** The repository for customer channel configurations.. */
  private final EventConfigChannelMapRepository eventConfigChannelMapRepository;

  /** The repository for customer threshold values for events.. */
  private final UserThresholdValueRepository userThresholdValueRepository;

  /** The repository for customer event histories.. */
  private final CustomerEventHistoryRepository customerEventHistoryRepository;

  /** The repository for event master definitions.. */
  private final EventRepository eventRepository;

  /** The repository for channel master definitions.. */
  private final ChannelRepository channelRepository;

  /** The repository for account event config mapping.. */
  private final CustomerAccountRepository customerAccountRepository;

  /** The repository for customer specific event configurations.. */
  private final EventDefaultRepository eventDefaultRepository;

  /** The repository for customer event map.. */
  private final EventMapRepository eventMapRepository;

  /** The repository for default event channel mapping. */
  private final EventChannelMapRepository eventChannelMapRepository;

  /** Account information properties */
  private final AccountInformationProperties accountInformationProperties;

  /** Account and customer events from master data */
  private final List<Event> accountAndCustomerEvents = new ArrayList<>();

  /** Security and mandatory events from master data */
  private final List<Event> securityAndMandatoryEvents = new ArrayList<>();

  /** Default events channel map from master data */
  private final List<EventChannelMap> defaultEventsChannelMap = new ArrayList<>();

  /** Event default configuration from master data */
  private final List<EventDefault> eventDefaults = new ArrayList<>();

  /** Channel from master data */
  private List<Channel> channels;

  /**
   * Initialize the collections with master data, the assumption is that the data will not change
   * during runtime, but during a migration of flyway.
   */
  @PostConstruct
  void init() {
    var events = eventRepository.findAll();

    var foundAccountAndCustomerEvents =
        events.stream()
            .filter(
                e ->
                    e.getEventGroup().getGroupName().equals(ACCOUNT_EVENTS)
                        || e.getEventGroup().getGroupName().equals(CUSTOMER_EVENTS))
            .collect(Collectors.toList());

    var foundSecurityAndMandatoryEvents =
        events.stream()
            .filter(
                e ->
                    e.getEventGroup().getGroupName().equals(SECURITY_EVENTS)
                        || e.getEventGroup().getGroupName().equals(MANDATORY_EVENTS))
            .collect(Collectors.toList());
    accountAndCustomerEvents.addAll(foundAccountAndCustomerEvents);
    securityAndMandatoryEvents.addAll(foundSecurityAndMandatoryEvents);

    channels = channelRepository.findAll();
    defaultEventsChannelMap.addAll(eventChannelMapRepository.findAll());
    eventDefaults.addAll(eventDefaultRepository.findAll());
  }

  /**
   * Validates the given event type.
   *
   * @param requestData RequestData the evaluate request
   * @return return operation result success or fail..
   */
  @Override
  public EventNotificationResponse evaluate(@Valid EventNotification requestData) {
	  
	/*  
    Optional<Event> eventOptional =
        eventRepository.findOneByEventNameAndActive(requestData.getEvent(), true);
    if (eventOptional.isPresent()) {
      Optional<CustomerEventConfiguration> customerEventConfigurationQuery =
          customerEventConfigurationsRepository
              .findAllByCustomerUniqueId(requestData.getCustomerUniqueIdentifier())
              .stream()
              .map(eventMapRepository::findByEventConf)
              .filter(Optional::isPresent)
              .filter(
                  eventMap ->
                      eventMap
                          .get()
                          .getEvent()
                          .getEventName()
                          .equals(eventOptional.get().getEventName()))
              .findFirst()
              .map(eventMap -> eventMap.get().getEventConf());

      if (isNotEmpty(requestData.getAccountNumber())) {
        List<Long> allByAccountId =
            customerAccountRepository
                .findAllByAccountNumber(requestData.getAccountNumber())
                .stream()
                .map(customerAccount -> customerAccount.getEventConf().getEventConfId())
                .collect(Collectors.toList());
        customerEventConfigurationQuery =
            customerEventConfigurationQuery.stream()
                .filter(
                    eventConfiguration ->
                        allByAccountId.contains(eventConfiguration.getEventConfId()))
                .findFirst();
      }

      if (customerEventConfigurationQuery.isEmpty()) {
        createEvaluateEventHistoryEntry(
            Collections.emptyList(),
            requestData.getCustomerUniqueIdentifier(),
            requestData.getEvent(),
            requestData.getAccountNumber(),
            null,
            true);
        return EventNotificationResponse.builder()
            .eventFiltrationResponse(EventFiltrationResponse.builder().allowed(false).build())
            .build();
      }
      CustomerEventConfiguration customerEventConfiguration = customerEventConfigurationQuery.get();
      if (isValueEditable(customerEventConfiguration)) {
        Optional<AlertThresholdValue> optionalUserThresholdValue;
        Optional<EventMap> eventMapOptional =
            eventMapRepository.findByEventConf(customerEventConfiguration);
        if (eventMapOptional.isEmpty()) {
          optionalUserThresholdValue = Optional.empty();
        } else {
          optionalUserThresholdValue =
              userThresholdValueRepository.findOneByEventMap(eventMapOptional.get());
        }
        if (optionalUserThresholdValue.isPresent()
            && optionalUserThresholdValue.get().getThresholdValue()
                > requestData.getEventTriggerValue()) {
          createEvaluateEventHistoryEntry(
              Collections.emptyList(),
              requestData.getCustomerUniqueIdentifier(),
              requestData.getEvent(),
              requestData.getAccountNumber(),
              customerEventConfiguration,
              true);
          return EventNotificationResponse.builder()
              .eventFiltrationResponse(EventFiltrationResponse.builder().allowed(false).build())
              .build();
        }
      }
      List<EventConfigChannelMap> configChannelMaps =
          getEventConfigChannelMaps(customerEventConfiguration);
      createEvaluateEventHistoryEntry(
          configChannelMaps,
          requestData.getCustomerUniqueIdentifier(),
          requestData.getEvent(),
          requestData.getAccountNumber(),
          customerEventConfiguration,
          false);
      return EventNotificationResponse.builder()
          .eventFiltrationResponse(
              EventFiltrationResponse.builder()
                  .allowedChannels(
                      configChannelMaps.stream()
                          .map(
                              eventConfigChannelMap ->
                                  com.cyn.ccds.event.subs.filter.client.Channel.builder()
                                      .channelName(
                                          eventConfigChannelMap.getChannel().getChannelName())
                                      .channelId(eventConfigChannelMap.getChannel().getChannelId())
                                      .active(eventConfigChannelMap.getChannel().isActive())
                                      .build())
                          .collect(Collectors.toList()))
                  .allowed(true)
                  .build())
          .build();
    } else {
      return EventNotificationResponse.builder()
          .eventFiltrationResponse(EventFiltrationResponse.builder().allowed(false).build())
          .build();
    }
    */
    
  }

  /**
   * Capture a notification that has been sent to customer.
   *
   * @param requestData structure containing notification details.
   * @return status of the capture operation..
   */
  @Override
  public Status capture(CaptureNotification requestData) {
	  
	  /*
    Optional<Event> eventOptional =
        eventRepository.findOneByEventNameAndActive(requestData.getEvent(), true);
    if (eventOptional.isEmpty()) {
      return Status.FAIL;
    }
    Optional<CustomerEventConfiguration> customerEventConfigurationQuery =
        customerEventConfigurationsRepository
            .findAllByCustomerUniqueId(requestData.getCustomerUniqueIdentifier())
            .stream()
            .map(eventMapRepository::findByEventConf)
            .filter(Optional::isPresent)
            .filter(
                eventMap ->
                    eventMap
                        .get()
                        .getEvent()
                        .getEventName()
                        .equals(eventOptional.get().getEventName()))
            .findFirst()
            .map(eventMap -> eventMap.get().getEventConf());

    if (isNotEmpty(requestData.getAccountNumber())) {
      List<Long> allByAccountId =
          customerAccountRepository.findAllByAccountNumber(requestData.getAccountNumber()).stream()
              .map(customerAccount -> customerAccount.getEventConf().getEventConfId())
              .collect(Collectors.toList());
      customerEventConfigurationQuery =
          customerEventConfigurationQuery.stream()
              .filter(
                  eventConfiguration ->
                      allByAccountId.contains(eventConfiguration.getEventConfId()))
              .findFirst();
    }
    if (customerEventConfigurationQuery.isEmpty()) {
      return Status.FAIL;
    }
    String[] channelIds = requestData.getChannel().split(CHANNELS_DELIMITER);
    List<String> channelNames = new ArrayList<>();
    for (String channelId : channelIds) {
      Optional<Channel> optionalChannel = channelRepository.findById(Long.valueOf(channelId));
      if (optionalChannel.isPresent()) {
        channelNames.add(optionalChannel.get().getChannelName());
      }
    }
    CustomerEventConfiguration customerEventConfiguration = customerEventConfigurationQuery.get();
    CustomerEventLogHistory customerEventLogHistory = new CustomerEventLogHistory();
    customerEventLogHistory.setEventConf(customerEventConfiguration);
    Optional<EventMap> eventMapOptional =
        eventMapRepository.findByEventConf(customerEventConfiguration);
    if (eventMapOptional.isPresent()) {
      EventMap eventMap = eventMapOptional.get();
      customerEventLogHistory.setEvent(eventMap.getEvent());
      customerEventLogHistory.setEventGroupType(
          eventMap.getEvent().getEventGroup().getEventGroupType());
      customerEventLogHistory.setEventGroup(eventMap.getEvent().getEventGroup());
      customerEventLogHistory.setEventGroupName(eventMap.getEvent().getEventGroup().getGroupName());
      customerEventLogHistory.setEventGroupTypeName(
          eventMap.getEvent().getEventGroup().getEventGroupType().getGroupTypeName());
      customerEventLogHistory.setEventName(eventMap.getEvent().getEventName());
    }
    customerEventLogHistory.setFiltered(false);
    customerEventLogHistory.setAccountNumber(requestData.getAccountNumber());
    customerEventLogHistory.setRequestType(RequestType.NOTIFICATION);
    customerEventLogHistory.setChannels(StringUtils.join(channelNames, CHANNELS_DELIMITER));
    customerEventLogHistory.setCreatedOn(ZonedDateTime.now());
    customerEventLogHistory.setCustomerUniqueId(requestData.getCustomerUniqueIdentifier());
    customerEventLogHistory.setShortMessage(requestData.getShortMessage());
    customerEventHistoryRepository.save(customerEventLogHistory);*/
    
    
    return Status.SUCCESS;
  }

  /**
   * Get available channels for configured events.
   *
   * @param customerEventConfiguration the query result of event configurations of customer
   * @return the event channel mapping.
   */
  private List<EventConfigChannelMap> getEventConfigChannelMaps(
      CustomerEventConfiguration customerEventConfiguration) {
    return eventConfigChannelMapRepository.findByEventConfAndActive(
        customerEventConfiguration, true);
  }

  /**
   * Create history entries in database for the identified channels and events.
   *
   * @param configChannelMaps active events and channels
   * @param customerId customer unique id
   * @param message the incoming message to be routed
   * @param customerEventConfiguration event configuration instance of customer
   * @param filtered if the notification filtered or notp\.
   */
  public void createEvaluateEventHistoryEntry(
      List<EventConfigChannelMap> configChannelMaps,
      String customerId,
      String message,
      String accountNumber,
      CustomerEventConfiguration customerEventConfiguration,
      boolean filtered) {
	  
/*
    if (customerEventConfiguration == null) {
      return;
    }
    Optional<EventMap> eventMapOptional =
        eventMapRepository.findByEventConf(customerEventConfiguration);

    CustomerEventLogHistory customerEventLogHistory = new CustomerEventLogHistory();
    customerEventLogHistory.setEventConf(customerEventConfiguration);
    if (eventMapOptional.isPresent()) {
      EventMap eventMap = eventMapOptional.get();
      customerEventLogHistory.setEvent(eventMap.getEvent());
      customerEventLogHistory.setEventGroup(eventMap.getEvent().getEventGroup());
      customerEventLogHistory.setEventName(eventMap.getEvent().getEventName());
      customerEventLogHistory.setEventGroupName(eventMap.getEvent().getEventGroup().getGroupName());
      customerEventLogHistory.setEventGroupTypeName(
          eventMap.getEvent().getEventGroup().getEventGroupType().getGroupTypeName());
    }
    customerEventLogHistory.setAccountNumber(accountNumber);
    customerEventLogHistory.setFiltered(filtered);
    customerEventLogHistory.setRequestType(RequestType.EVALUATE);
    customerEventLogHistory.setChannels(
        configChannelMaps.stream()
            .map(eventConfigChannelMap -> eventConfigChannelMap.getChannel().getChannelName())
            .collect(Collectors.joining(CHANNELS_DELIMITER)));
    customerEventLogHistory.setCreatedOn(ZonedDateTime.now());
    customerEventLogHistory.setCustomerUniqueId(customerId);
    customerEventLogHistory.setShortMessage(message);
    customerEventHistoryRepository.save(customerEventLogHistory);
    */
    
  }

  /**
   * Return the number of history records based on given pagination info.
   *
   * @param requestData the pagination data
   * @return response containing the count.
   */
  @Override
  public int historyCount(Pagination requestData, String customerId) {
    Pageable sortedBy =
        PageRequest.of(
            requestData.getPageNo(), requestData.getLimit(), Sort.by(requestData.getSortBy()));
    List<CustomerEventLogHistory> customerEventHistories =
        customerEventHistoryRepository.findByCustomerUniqueId(customerId, sortedBy);
    return customerEventHistories.size();
  }

  /**
   * Internal helper method to create a specification out of date ranges.
   *
   * @param from from datetime.
   * @param to to datetime.
   * @return specification instance.
   */
  private static Specification<CustomerEventLogHistory> createDateBetween(
      ZonedDateTime from, ZonedDateTime to) {
    return (root, criteriaQuery, criteriaBuilder) -> {
      Predicate startingFrom =
          criteriaBuilder.greaterThanOrEqualTo(
              root.get(CREATED_ON),
              from.minusHours(from.getHour())
                  .minusMinutes(from.getMinute())
                  .minusSeconds(from.getSecond()));
      Predicate endingAt = criteriaBuilder.lessThanOrEqualTo(root.get(CREATED_ON), to);
      return criteriaBuilder.and(startingFrom, endingAt);
    };
  }

  /**
   * Return history records based on given pagination info.
   *
   * @param requestData the pagination data
   * @return response containing the history records.
   */
  @Override
  public List<CustomerEventHistory> historyData(Pagination requestData, String customerId) {
    Pageable sortedBy =
        PageRequest.of(
            requestData.getPageNo(), requestData.getLimit(), Sort.by(requestData.getSortBy()));
    if (requestData.getStartDate() == null) {
      requestData.setStartDate(ZonedDateTime.now().minusDays(DEFAULT_HISTORY_QUERY_DAYS));
    }
    List<CustomerEventLogHistory> customerEventHistories;

    if (requestData.getFilter() != null && !requestData.getFilter().isEmpty()) {
      List<Specification<CustomerEventLogHistory>> specs =
          requestData.getFilter().stream()
              .map(HistorySpecification::new)
              .collect(Collectors.toList());
      Specification<CustomerEventLogHistory> specification = specs.get(0);

      for (int i = 1; i < requestData.getFilter().size(); i++) {
        Specification<CustomerEventLogHistory> customerEventLogHistorySpecification =
            Specification.where(specification);
        if (customerEventLogHistorySpecification != null) {
          specification = customerEventLogHistorySpecification.and(specs.get(i));
        }
      }

      // Add start date to specification
      Specification<CustomerEventLogHistory> startDateSpecification =
          Specification.where(specification);
      if (startDateSpecification != null) {
        specification =
            startDateSpecification.and(
                createDateBetween(requestData.getStartDate(), ZonedDateTime.now()));
      }
      customerEventHistories =
          customerEventHistoryRepository.findAll(specification, sortedBy).toList();
    } else {
      customerEventHistories =
          customerEventHistoryRepository
              .findByCustomerUniqueIdAndCreatedOnAfterOrderByHistoryIdDesc(
                  customerId, requestData.getStartDate(), sortedBy);
    }

    return customerEventHistories.stream()
        .map(
            customerEventLogHistory ->
                CustomerEventHistory.builder()
                    .event(
                        com.cyn.ccds.event.subs.filter.client.Event.builder()
                            .eventDescription(
                                customerEventLogHistory.getEvent().getEventDescription())
                            .eventName(customerEventLogHistory.getEvent().getEventName())
                            .build())
                    .eventGroup(
                        com.cyn.ccds.event.subs.filter.client.EventGroup.builder()
                            .description(customerEventLogHistory.getEventGroup().getDescription())
                            .eventGroupName(customerEventLogHistory.getEventGroup().getGroupName())
                            .eventGroupId(customerEventLogHistory.getEventGroup().getEventGroupId())
                            .build())
                    .accountNumber(customerEventLogHistory.getAccountNumber())
                    .eventGroupType(customerEventLogHistory.getEventGroupTypeName())
                    .historyId(customerEventLogHistory.getHistoryId())
                    .message(customerEventLogHistory.getShortMessage())
                    .timeOfTransmission(customerEventLogHistory.getCreatedOn())
                    .createdOn(customerEventLogHistory.getCreatedOn())
                    .requestType(customerEventLogHistory.getRequestType().toString())
                    .shortMessage(customerEventLogHistory.getShortMessage())
                    .channel(
                        com.cyn.ccds.event.subs.filter.client.Channel.builder()
                            .channelName(customerEventLogHistory.getChannels())
                            .build())
                    .build())
        .collect(Collectors.toList());
  }

  /**
   * Return the default available events.
   *
   * @return all of the events in the master tables.
   */
  public Map<Object, Object> viewEvents(String customerId, @Valid ConfigViewRequest requestData) {
    List<CustomerEventConfiguration> customerEvents;
    if (isEmpty(requestData.getAccountNumber())) {
      customerEvents = customerEventConfigurationsRepository.findAllByCustomerUniqueId(customerId);
    } else {
      List<CustomerAccount> accountConfigs =
          customerAccountRepository.findAllByAccountNumber(requestData.getAccountNumber());
      customerEvents =
          customerEventConfigurationsRepository.findAllById(
              accountConfigs.stream()
                  .map(customerAccount -> customerAccount.getEventConf().getEventConfId())
                  .collect(Collectors.toSet()));
    }

    Map<Object, List<com.cyn.ccds.event.subs.filter.client.CustomerEventConfiguration>>
        eventConfigurations =
            customerEvents.stream()
                .map(
                    customerEventConfiguration -> {
                      Optional<AlertThresholdValue> userThresholdValueQuery;
                      Optional<EventMap> eventMapOptional =
                          eventMapRepository.findByEventConf(customerEventConfiguration);
                      if (eventMapOptional.isEmpty()) {
                        userThresholdValueQuery = Optional.empty();
                      } else {
                        userThresholdValueQuery =
                            userThresholdValueRepository.findOneByEventMap(eventMapOptional.get());
                      }

                      AlertThresholdValue userThresholdValue = null;
                      if (!userThresholdValueQuery.isEmpty()) {
                        userThresholdValue = userThresholdValueQuery.get();
                      }
                      List<EventConfigChannelMap> channelMaps =
                          eventConfigChannelMapRepository.findByEventConf(
                              customerEventConfiguration);
                      List<com.cyn.ccds.event.subs.filter.client.EventConfigChannelMap> channels =
                          new ArrayList<>();

                      com.cyn.ccds.event.subs.filter.client.CustomerEventConfiguration
                              .CustomerEventConfigurationBuilder
                          configurationBuilder =
                              com.cyn.ccds.event.subs.filter.client.CustomerEventConfiguration
                                  .builder()
                                  .active(eventMapOptional.get().isActive())
                                  .accountId(
                                      eventMapOptional.isPresent()
                                              && eventMapOptional.get().getCustomerAccount() != null
                                          ? eventMapOptional
                                              .get()
                                              .getCustomerAccount()
                                              .getAccountNumber()
                                          : null)
                                  .event(
                                      com.cyn.ccds.event.subs.filter.client.Event.builder()
                                          .active(
                                              eventMapOptional.isPresent()
                                                  ? eventMapOptional.get().getEvent().isActive()
                                                  : null)
                                          .eventName(
                                              eventMapOptional.isPresent()
                                                  ? eventMapOptional.get().getEvent().getEventName()
                                                  : null)
                                          .eventDescription(
                                              eventMapOptional.isPresent()
                                                  ? eventMapOptional
                                                      .get()
                                                      .getEvent()
                                                      .getEventDescription()
                                                  : null)
                                          .eventGroup(
                                              com.cyn.ccds.event.subs.filter.client.EventGroup
                                                  .builder()
                                                  .eventGroupId(
                                                      eventMapOptional.isPresent()
                                                          ? eventMapOptional
                                                              .get()
                                                              .getEvent()
                                                              .getEventGroup()
                                                              .getEventGroupId()
                                                          : null)
                                                  .description(
                                                      eventMapOptional.isPresent()
                                                          ? eventMapOptional
                                                              .get()
                                                              .getEvent()
                                                              .getEventGroup()
                                                              .getDescription()
                                                          : null)
                                                  .eventGroupName(
                                                      eventMapOptional.isPresent()
                                                          ? eventMapOptional
                                                              .get()
                                                              .getEvent()
                                                              .getEventGroup()
                                                              .getGroupName()
                                                          : null)
                                                  .groupName(
                                                      eventMapOptional.isPresent()
                                                          ? eventMapOptional
                                                              .get()
                                                              .getEvent()
                                                              .getEventGroup()
                                                              .getGroupName()
                                                          : null)
                                                  .build())
                                          .eventId(
                                              eventMapOptional.isPresent()
                                                  ? eventMapOptional.get().getEvent().getEventId()
                                                  : null)
                                          .eventName(
                                              eventMapOptional.isPresent()
                                                  ? eventMapOptional.get().getEvent().getEventName()
                                                  : null)
                                          .imageUrl(
                                              eventMapOptional.isPresent()
                                                  ? eventMapOptional.get().getEvent().getImageUrl()
                                                  : null)
                                          .valueEditable(
                                              isValueEditable(customerEventConfiguration))
                                          .build())
                                  .eventConfId(customerEventConfiguration.getEventConfId())
                                  .createdOn(customerEventConfiguration.getCreatedOn())
                                  .customerUniqueId(
                                      customerEventConfiguration.getCustomerUniqueId())
                                  .updatedOn(customerEventConfiguration.getUpdatedOn());
                      if (userThresholdValue != null) {
                        configurationBuilder.userThresholdValue(
                            com.cyn.ccds.event.subs.filter.client.UserThresholdValue.builder()
                                .thresholdId(userThresholdValue.getThresholdId())
                                .thresholdValue(userThresholdValue.getThresholdValue())
                                .build());
                      }

                      channelMaps.forEach(
                          eventConfigChannelMap -> {
                            com.cyn.ccds.event.subs.filter.client.EventConfigChannelMap channelMap =
                                com.cyn.ccds.event.subs.filter.client.EventConfigChannelMap
                                    .builder()
                                    .active(eventConfigChannelMap.isActive())
                                    .channel(
                                        com.cyn.ccds.event.subs.filter.client.Channel.builder()
                                            .channelId(
                                                eventConfigChannelMap.getChannel().getChannelId())
                                            .active(eventConfigChannelMap.getChannel().isActive())
                                            .channelName(
                                                eventConfigChannelMap.getChannel().getChannelName())
                                            .build())
                                    .defaultValue(isValueEditable(customerEventConfiguration))
                                    .createdOn(eventConfigChannelMap.getCreatedOn())
                                    .updatedOn(eventConfigChannelMap.getUpdatedOn())
                                    .channelMapId(eventConfigChannelMap.getChannelMapId())
                                    .build();
                            channels.add(channelMap);
                          });
                      configurationBuilder.channels(channels);
                      return configurationBuilder.build();
                    })
                .collect(Collectors.groupingBy(this::getCustomerEventConfigurationGroupKey));
    List<com.cyn.ccds.event.subs.filter.client.CustomerEventConfiguration> accountEvents =
        eventConfigurations.get(ACCOUNT_EVENTS_RESPONSE_KEY) == null
            ? Collections.emptyList()
            : eventConfigurations.get(ACCOUNT_EVENTS_RESPONSE_KEY);
    Map<String, List<com.cyn.ccds.event.subs.filter.client.CustomerEventConfiguration>>
        accountEventsAggregated =
            accountEvents.stream()
                .collect(
                    Collectors.groupingBy(
                        com.cyn.ccds.event.subs.filter.client.CustomerEventConfiguration
                            ::getAccountId));
    List<AccountEvents> formattedAccountEvents =
        accountEventsAggregated.entrySet().stream()
            .map(
                event ->
                    AccountEvents.builder()
                        .accountId(event.getKey())
                        .accountType(event.getValue().get(0).getAccountType())
                        .customerUniqueId(event.getValue().get(0).getCustomerUniqueId())
                        .events(
                            event.setValue(
                                event.getValue().stream()
                                    .map(
                                        customerEventConfiguration -> {
                                          customerEventConfiguration.setAccountId(null);
                                          customerEventConfiguration.setAccountType(null);
                                          customerEventConfiguration.setCustomerUniqueId(null);
                                          return customerEventConfiguration;
                                        })
                                    .collect(Collectors.toList())))
                        .build())
            .collect(Collectors.toList());
    Map<Object, Object> resultMap = new HashMap<>();
    resultMap.put(ACCOUNT_EVENTS_RESPONSE_KEY, formattedAccountEvents);
    resultMap.put(
        CUSTOMER_CONFIG_ALERT_RESPONSE_KEY,
        eventConfigurations.get(CUSTOMER_CONFIG_ALERT_RESPONSE_KEY));
    return resultMap;
  }

  /**
   * Check if a configuration is value editable.
   *
   * @param customerEventConfiguration customerEventConfiguration instance to check.
   * @return flag to indicate if a configuration is value editable.
   */
  private boolean isValueEditable(CustomerEventConfiguration customerEventConfiguration) {
    Optional<EventMap> eventMapOptional =
        eventMapRepository.findByEventConf(customerEventConfiguration);
    if (eventMapOptional.isEmpty()) {
      return false;
    }
    List<EventDefault> defaultEventConfigs =
        eventDefaults.stream()
            .filter(
                ed -> ed.getEvent().getEventId() == eventMapOptional.get().getEvent().getEventId())
            .collect(Collectors.toList());
    Optional<EventDefault> valueEditableEventOptional =
        defaultEventConfigs.stream()
            .filter(
                eventDefault -> EventDefaultParam.DEFAULT_THRESHOLD == eventDefault.getEventParam())
            .findFirst();
    return valueEditableEventOptional.isPresent();
  }

  /**
   * Return default threshold value of event.
   *
   * @param customerEventConfiguration customerEventConfiguration instance to check.
   * @return flag to indicate if a configuration is value editable.
   */
  private int getDefaultThreshold(CustomerEventConfiguration customerEventConfiguration) {
    Optional<EventMap> eventMapOptional =
        eventMapRepository.findByEventConf(customerEventConfiguration);
    if (eventMapOptional.isEmpty()) {
      return 0;
    }
    List<EventDefault> defaultEventConfigs =
        eventDefaults.stream()
            .filter(ed -> ed.getEvent().equals(eventMapOptional.get().getEvent()))
            .collect(Collectors.toList());
    Optional<EventDefault> valueEditableEventOptional =
        defaultEventConfigs.stream()
            .filter(
                eventDefault -> EventDefaultParam.DEFAULT_THRESHOLD == eventDefault.getEventParam())
            .findFirst();
    return valueEditableEventOptional.isPresent()
        ? Integer.parseInt(valueEditableEventOptional.get().getEventParamValue())
        : 0;
  }

  /**
   * Reformat event configuration naming convention for grouping events.
   *
   * @param customerEventConfiguration configuration containing events.
   * @return the group key name.
   */
  private Object getCustomerEventConfigurationGroupKey(
      com.cyn.ccds.event.subs.filter.client.CustomerEventConfiguration customerEventConfiguration) {
    return customerEventConfiguration
        .getEvent()
        .getEventGroup()
        .getGroupName()
        .replace(SPACE_SEPARATOR, UNDERSCORE_SEPARATOR)
        .toLowerCase();
  }

  /**
   * Update the event and channel preferences of the user.
   *
   * @return empty generic response.
   */
  @Transactional
  public Status updateEvent(EventNotificationConfigUpdate requestData) {
    Optional<CustomerEventConfiguration> eventConfigChannelMapOptional =
        customerEventConfigurationsRepository.findById(requestData.getEventConfId());

    if (eventConfigChannelMapOptional.isEmpty()) {
      throw new EntityNotFoundException(ESB_ERR000003.toString(), Collections.emptyList());
    }

    CustomerEventConfiguration customerEventConfiguration = eventConfigChannelMapOptional.get();

    if (requestData.getActive() != null && requestData.getActive()) {
      activateEventConfig(requestData, customerEventConfiguration);
    } else if (requestData.getActive() != null && !requestData.getActive()) {
      deactivateEventConfig(requestData, customerEventConfiguration);
    }
    if (isValueEditable(customerEventConfiguration) && requestData.getThresholdValue() != null) {

      Optional<AlertThresholdValue> userThresholdValues;
      Optional<EventMap> eventMapOptional =
          eventMapRepository.findByEventConf(customerEventConfiguration);
      if (eventMapOptional.isEmpty()) {
        userThresholdValues = Optional.empty();
      } else {
        userThresholdValues =
            userThresholdValueRepository.findOneByEventMap(eventMapOptional.get());
      }

      if (userThresholdValues.isEmpty() && requestData.getActive() == null) {
        throw new EntityNotFoundException(ESB_ERR000004.toString(), Collections.emptyList());
      } else if (!userThresholdValues.isEmpty()) {
        userThresholdValues.stream()
            .forEach(
                userThresholdValue -> {
                  userThresholdValue.setThresholdValue(requestData.getThresholdValue());
                  userThresholdValueRepository.save(userThresholdValue);
                });
      } else if (Boolean.TRUE.equals(requestData.getActive())
          && isValueEditable(customerEventConfiguration)) {
        createUserThreshold(customerEventConfiguration, requestData.getThresholdValue());
      }
    }

    activateOrDeactivateChannels(requestData, customerEventConfiguration);
    return Status.SUCCESS;
  }

  /**
   * Create threshold definition for event config.
   *
   * @param customerEventConfiguration the event config to link for threshold.
   * @param threshold the initial threshold value..
   */
  private void createUserThreshold(
      CustomerEventConfiguration customerEventConfiguration, int threshold) {
    Optional<EventMap> optionalEventMap =
        eventMapRepository.findByEventConf(customerEventConfiguration);
    AlertThresholdValue userThresholdValue = new AlertThresholdValue();
    userThresholdValue.setEventMap(optionalEventMap.orElse(null));
    userThresholdValue.setThresholdValue(threshold);
    userThresholdValueRepository.save(userThresholdValue);
  }

  /**
   * Activates or deactivates channels based on given request for an event configuration.
   *
   * @param requestData the actual request data to get channel status definitions.
   * @param customerEventConfiguration the stored customer event definition..
   */
  private void activateOrDeactivateChannels(
      EventNotificationConfigUpdate requestData,
      CustomerEventConfiguration customerEventConfiguration) {
    if (requestData.getChannelConfig() != null) {
      requestData.getChannelConfig().stream()
          .forEach(
              channel -> {
                Optional<Channel> channelOptional =
                    channelRepository.findById(channel.getChannelId());
                if (channelOptional.isPresent()) {
                  List<EventConfigChannelMap> configChannelMaps =
                      eventConfigChannelMapRepository.findByChannelAndEventConf(
                          channelOptional.get(), customerEventConfiguration);
                  if (!configChannelMaps.isEmpty()) {
                    EventConfigChannelMap eventConfigChannelMap = configChannelMaps.get(0);
                    eventConfigChannelMap.setActive(channel.isActive());
                    eventConfigChannelMapRepository.save(eventConfigChannelMap);
                  }
                }
              });
    }
  }

  /**
   * The deactivation logic for an event config, deactivation mutes all channels as well.
   *
   * @param requestData The actual request comes from client.
   * @param customerEventConfiguration the event configuration definition..
   */
  private void deactivateEventConfig(
      EventNotificationConfigUpdate requestData,
      CustomerEventConfiguration customerEventConfiguration) {
    if (requestData.getThresholdValue() != null || requestData.getChannelConfig() != null) {
      throw new InvalidStatusRequestException(ESB_ERR000008.toString(), Collections.emptyList());
    }
    List<EventConfigChannelMap> configChannelMaps =
        eventConfigChannelMapRepository.findByEventConf(customerEventConfiguration);
    configChannelMaps.forEach(
        eventConfigChannelMap -> {
          eventConfigChannelMap.setActive(false);
          eventConfigChannelMapRepository.save(eventConfigChannelMap);
        });
    Optional<EventMap> optionalEventMap =
        eventMapRepository.findByEventConf(customerEventConfiguration);
    if (optionalEventMap.isPresent()) {
      EventMap eventMap = optionalEventMap.get();
      userThresholdValueRepository.deleteByEventMap(eventMap);
      eventMap.setActive(false);
      eventMapRepository.save(eventMap);
    }
  }

  /**
   * Activate the event config by checking all the required fields.
   *
   * @param requestData the actual request to activate event.
   * @param customerEventConfiguration the customer event definition stored in database..
   */
  private void activateEventConfig(
      EventNotificationConfigUpdate requestData,
      CustomerEventConfiguration customerEventConfiguration) {
    if (requestData.getChannelConfig() == null || requestData.getChannelConfig().isEmpty()) {
      throw new InvalidStatusRequestException(ESB_ERR000006.toString(), Collections.emptyList());
    }
    long activeChannelCount =
        requestData.getChannelConfig().stream()
            .filter(com.cyn.ccds.event.subs.filter.client.Channel::isActive)
            .count();
    if (activeChannelCount == 0) {
      throw new InvalidStatusRequestException(ESB_ERR000006.toString(), Collections.emptyList());
    }

    if (requestData.getThresholdValue() == null && isValueEditable(customerEventConfiguration)) {
      throw new InvalidStatusRequestException(ESB_ERR000007.toString(), Collections.emptyList());
    }
    Optional<EventMap> eventMapOptional =
        eventMapRepository.findByEventConf(customerEventConfiguration);
    if (eventMapOptional.isPresent()) {
      EventMap eventMap = eventMapOptional.get();
      eventMap.setActive(true);
      eventMapRepository.save(eventMap);
    }
  }

  /**
   * Configure events for a new user. This will copy all the master data from master tables and move
   * them to corresponding user tables
   *
   * @param configureUser the unique customer id
   * @return empty generic response.
   */
  @org.springframework.transaction.annotation.Transactional
  public Status configureEvents(ConfigureUser configureUser) {
	  /*
	  
    if ((isEmpty(configureUser.getAccountNumber())
            && !customerEventConfigurationsRepository
                .findAllByCustomerUniqueId(configureUser.getCustomerUniqueIdentifier())
                .isEmpty())
        || (isNotEmpty(configureUser.getAccountNumber())
            && !customerAccountRepository
                .findAllByAccountNumber(configureUser.getAccountNumber())
                .isEmpty())) {
      throw new BaseServiceException(ESB_ERR000010.toString(), HttpStatus.CONFLICT);
    }
    AccountRequest accountRequest = new AccountRequest();
    accountRequest.setCustomerUniqueIdentifier(configureUser.getCustomerUniqueIdentifier());

    List<Account> accounts = getCustomerAccounts(accountRequest);
    List<List<Object>> eventsAndAccounts =
        Lists.cartesianProduct(
            accountAndCustomerEvents,
            isEmpty(configureUser.getAccountNumber())
                ? accounts
                : accounts.stream()
                    .filter(
                        account ->
                            account
                                .getExternalAccountNumber()
                                .equals(configureUser.getAccountNumber()))
                    .collect(Collectors.toList()));
    eventsAndAccounts.forEach(
        tuple -> {
          Event event = (Event) tuple.get(0);
          Account account = (Account) tuple.get(1);
          processEventForRegistration(
              new NewEventInput(
                  configureUser.getCustomerUniqueIdentifier(), channels, event, account));
        });
    if (isEmpty(configureUser.getAccountNumber())) {
      securityAndMandatoryEvents.forEach(
          event ->
              processEventForRegistration(
                  new NewEventInput(
                      configureUser.getCustomerUniqueIdentifier(), channels, event, null)));
    }
    */
    return Status.SUCCESS;
  }

  /**
   * Internal helper method to retrieve all accounts for a customer.
   *
   * @param accountRequest AccountRequest instance.
   * @return AccountResponse that contains list of accounts.
   */
  private List<Account> getCustomerAccounts(AccountRequest accountRequest) {
    /*ResponseEntity<Object> responseEntity =
        remoteInvocationService.executeRemoteService(
            RemoteInvocationContext.builder()
                .returnType(Object.class)
                .requestBody(accountRequest)
                .httpMethod(HttpMethod.POST)
                .url(
                    UriComponentsBuilder.fromHttpUrl(accountInformationProperties.getAccountsUri())
                        .buildAndExpand(
                            Collections.singletonMap(
                                accountInformationProperties.getContextPlaceholder(),
                                accountInformationProperties.getContextBusiness()))
                        .toUriString())
                .includeAccessToken(true)
                .build());
    AccountResponse businessAccountResponse =
        JsonUtils.OBJECT_MAPPER
            .convertValue(responseEntity.getBody(), GENERIC_ACCOUNTS_TYPE)
            .getBody();

    ResponseEntity<Object> personalResponseEntity =
        remoteInvocationService.executeRemoteService(
            RemoteInvocationContext.builder()
                .returnType(Object.class)
                .requestBody(accountRequest)
                .httpMethod(HttpMethod.POST)
                .url(
                    UriComponentsBuilder.fromHttpUrl(accountInformationProperties.getAccountsUri())
                        .buildAndExpand(
                            Collections.singletonMap(
                                accountInformationProperties.getContextPlaceholder(),
                                accountInformationProperties.getContextPersonal()))
                        .toUriString())
                .includeAccessToken(true)
                .build());
    AccountResponse personalAccountResponse =
        JsonUtils.OBJECT_MAPPER
            .convertValue(personalResponseEntity.getBody(), GENERIC_ACCOUNTS_TYPE)
            .getBody();
    businessAccountResponse.getAccounts().addAll(personalAccountResponse.getAccounts());
    return businessAccountResponse.getAccounts().stream().collect(Collectors.toList());*/
	  return null;
  }

  /**
   * Create an event config for customer out of master definition.
   *
   * @param eventInput structure needed for creating a new event for customer..
   */
  private void processEventForRegistration(NewEventInput eventInput) {
	  /*
    CustomerEventConfiguration customerEventConfiguration = new CustomerEventConfiguration();
    CustomerAccount customerAccount = null;
    if (eventInput.getAccount() != null) {
      customerAccount = new CustomerAccount();
      customerAccount.setAccountNumber(eventInput.getAccount().getExternalAccountNumber());
      customerAccount.setEventConf(customerEventConfiguration);
    }

    customerEventConfiguration.setCreatedOn(ZonedDateTime.now());
    customerEventConfiguration.setCustomerUniqueId(eventInput.getCustomerId());
    customerEventConfigurationsRepository.save(customerEventConfiguration);
    if (customerAccount != null) {
      customerAccount.setEventConf(customerEventConfiguration);
      customerAccountRepository.save(customerAccount);
    }
    EventMap eventMap = new EventMap();
    eventMap.setActive(eventInput.getEvent().isActive());
    eventMap.setCustomerAccount(customerAccount);
    eventMap.setEvent(eventInput.getEvent());
    eventMap.setEventConf(customerEventConfiguration);
    eventMapRepository.save(eventMap);
    eventInput
        .getChannels()
        .forEach(
            channel -> {
              Optional<EventChannelMap> optionalChannelAndEvent =
                  defaultEventsChannelMap.stream()
                      .filter(isEventChannelMap(eventInput, channel))
                      .findFirst();
              if (optionalChannelAndEvent.isPresent()) {
                EventConfigChannelMap eventConfigChannelMap = new EventConfigChannelMap();
                eventConfigChannelMap.setActive(eventInput.getEvent().isActive());
                eventConfigChannelMap.setEventConf(customerEventConfiguration);
                eventConfigChannelMap.setChannel(channel);
                eventConfigChannelMap.setCreatedOn(ZonedDateTime.now());
                eventConfigChannelMapRepository.save(eventConfigChannelMap);
              }
            });
    if (isValueEditable(customerEventConfiguration)) {
      createUserThreshold(
          customerEventConfiguration, getDefaultThreshold(customerEventConfiguration));
    }
    */
  }

  /**
   * Compare if the event channel map correspond to the event input and channel.
   *
   * @param eventInput event input
   * @param channel channel
   * @return true if it matches
   */
  private java.util.function.Predicate<EventChannelMap> isEventChannelMap(
      NewEventInput eventInput, Channel channel) {
    return map ->
        map.getChannel().getChannelId() == channel.getChannelId()
            && map.getEvent().getEventId() == eventInput.getEvent().getEventId();
  }
}
