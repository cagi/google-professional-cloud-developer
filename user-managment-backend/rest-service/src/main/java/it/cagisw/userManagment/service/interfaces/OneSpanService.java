package it.cagisw.userManagment.service.interfaces;

import java.util.Map;

import com.cagisw.userManagment.commons.authentication.LoginRequest;
import com.cagisw.userManagment.commons.authentication.LoginResponse;

import it.cagisw.userManagment.api.ResponseCode;
import it.cagisw.userManagment.dto.BindingIdRequest;
import it.cagisw.userManagment.dto.BindingIdResponse;
import it.cagisw.userManagment.dto.BiometricRegistrationRequest;
import it.cagisw.userManagment.dto.BiometricRegistrationResponse;
import it.cagisw.userManagment.dto.CheckActivationStatusResponse;
import it.cagisw.userManagment.dto.LoginSessionResponse;
import it.cagisw.userManagment.dto.RegisterDeviceInfo;
import it.cagisw.userManagment.dto.RegistrationRequest;
import it.cagisw.userManagment.dto.RegistrationStatusCheck;
import it.cagisw.userManagment.dto.SessionValidationRequest;
import it.cagisw.userManagment.dto.TransactionValidationRequest;
import it.cagisw.userManagment.dto.UnlockRequest;
import it.cagisw.userManagment.dto.UserRegistrationResponse;
import it.cagisw.userManagment.dto.UserStatusResponse;

/** Interface for onespan services. */
public interface OneSpanService {

  /**
   * Register a new user to onespan domain.
   *
   * @param requestData the actual entity to hold information about user to be registered.
   * @return UserRegistrationResponse entity with activation code and cronto image.
   */
  UserRegistrationResponse registerUser(RegistrationRequest requestData);

  /**
   * Checks registration status from database
   *
   * @param requestData the actual status check request.
   * @return CheckActivationStatusResponse structure that holds the latest status.
   */
  CheckActivationStatusResponse registrationStatus(RegistrationStatusCheck requestData);

  /**
   * Login to onespan using online passkey.
   *
   * @param loginRequest the actual login request with passkey.
   * @param userId user that logins.
   * @param customerUniqueIdentifier customer unique identifier that makes the request.
   * @param bindingId registration binding id
   * @return LoginResponse structure with session status.
   */
  LoginResponse login(
      LoginRequest loginRequest, String userId, String customerUniqueIdentifier, String bindingId);

  /**
   * Check if onespan session is established
   *
   * @param requestId onespan request id.
   * @param username username that logs in
   * @return LoginSessionResponse instance including session status.
   */
  LoginSessionResponse loginSession(String requestId, String username);

  /**
   * Execute an orchestration command on a mobile device.
   *
   * @param inputData onespan sdk compatible input data.
   * @param username user that runs a device command
   * @return return from onespan api as is.
   */
  Object orchestrationCommand(Map<String, Object> inputData, String username);

  /**
   * Register biometrics flag for a user.
   *
   * @param requestData request content including biometrics type.
   * @param bindingId the binding id for registration
   * @return BiometricRegistrationResponse structure for a successful registration.
   */
  BiometricRegistrationResponse registerBiometrics(
      BiometricRegistrationRequest requestData, String bindingId);

  /**
   * Checks if a user registered biometrics and returns the details.
   *
   * @param userId userId to check in the system.
   * @param bindingID the binding id for registration
   * @return BiometricRegistrationResponse structure for a successful check.
   */
  BiometricRegistrationResponse checkBiometrics(String userId, String bindingID);

  /**
   * Returns the status of user by invoking onespan api.
   *
   * @param userId user unique identifier
   * @return UserStatusResponse structure with status flags.
   */
  UserStatusResponse userStatus(String userId);

  /**
   * Unlock a user using onespan api.
   *
   * @param requestData unlock request model instance.
   * @return status of the unlock operation.
   */
  ResponseCode unlockUser(UnlockRequest requestData);

  /**
   * Register a device to onespan user.
   *
   * @param requestData RegistrationStatusCheck with device details
   * @return status of the operation.
   */
  ResponseCode registerDeviceInfo(RegisterDeviceInfo requestData);

  /**
   * Retrieve a binding id using activation password.
   *
   * @param requestData BindingIdRequest structure.
   * @return BindingIdResponse instance that contains binding id.
   */
  BindingIdResponse retrieveBindingId(BindingIdRequest requestData);

  /**
   * Validate a session
   *
   * @param sessionValidationRequest session validation actual request.
   * @return SessionValidationResponse instance.
   */
  LoginSessionResponse loginSessionV2(SessionValidationRequest sessionValidationRequest);

  /**
   * Validate a digipass transaction.
   *
   * @param inputData TransactionValidationRequest structure.
   * @param userId
   * @return LoginResponse instance.
   */
  LoginResponse transactionValidation(TransactionValidationRequest inputData, String userId);
}
