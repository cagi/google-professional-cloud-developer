package it.cagisw.userManagment.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Response with the canonical id mapping by identifier. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CanonicalIdMapIdentifierResponse {

  /** Party id */
  private long partyId;

  /** Type of identifier */
  private IdentifierType identifierType;

  /** Identifiers */
  private List<String> identifiers;
}
