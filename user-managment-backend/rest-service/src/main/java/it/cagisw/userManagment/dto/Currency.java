package it.cagisw.userManagment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Representation of a currency definition. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Currency {

  /** Currency identifier. */
  private long currencyId;

  /** Name of the currency. */
  @JsonProperty("currency")
  private String currencyName;

  /** Symbol of the currency USD, GBP etc... */
  private String currencySymbol;

  /** Is currency active. */
  private boolean active;
}
