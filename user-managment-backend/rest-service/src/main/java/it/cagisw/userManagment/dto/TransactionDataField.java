package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Structure for transaction data field. */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDataField {

  /** Key of the data field. */
  @NotBlank private TransactionValidationKey key;

  /** Value of the data field */
  @NotBlank private TransactionValidationValue value;
}
