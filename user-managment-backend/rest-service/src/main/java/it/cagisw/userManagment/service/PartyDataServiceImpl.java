package it.cagisw.userManagment.service;


import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.cagisw.userManagment.property.PartyDataMgmtProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cagisw.userManagment.dto.BaseLocation;
import it.cagisw.userManagment.dto.LocationEntity;
import it.cagisw.userManagment.dto.PartyLocation;
import it.cagisw.userManagment.models.CanonicalIdMapEntity;
import it.cagisw.userManagment.models.CustomerProfile;
import it.cagisw.userManagment.service.interfaces.PartyDataService;

/** Service to perform operations with party data management component service. */
@Service
public class PartyDataServiceImpl
    implements PartyDataService {

  /** List with nationalities considered part of UK */
  private static final List<String> UK_NATIONALITIES =
      List.of("British", "English", "Northern Irish", "Scottish", "Welsh");

  /** First index */
  private static final int FIRST_INDEX = 0;

  /** Jackson object mapper */
  private final ObjectMapper objectMapper;

  /** Model object mapper */
  private final ModelMapper modelMapper;

  /** Properties used in operations with party data management component service */
  private final PartyDataMgmtProperties partyDataProperties;

  /**
   * Create new PartyDataServiceImpl instance.
   *
   * @param remoteInvocationService service to perform cyn service to service operations
   * @param objectMapper jackson object mapper
   * @param partyDataProperties properties used in operations with party data service
   */
  public PartyDataServiceImpl(
      final ObjectMapper objectMapper,
      final PartyDataMgmtProperties partyDataProperties) {
    this.objectMapper = objectMapper;
    this.partyDataProperties = partyDataProperties;
    this.modelMapper = new ModelMapper();
  }

  /**
   * Retrieve canonical identifiers mapping.
   *
   * @param customerUniqueIdentifier unique identifier of customer
   * @return the canonical id mapping
   */
  /*public CanonicalMapResponse getCanonicalIdMap(String customerUniqueIdentifier) {
    try {
      var response =
          remoteInvocationService.executeRemoteService(
              RemoteInvocationContext.builder()
                  .httpMethod(HttpMethod.GET)
                  .url(
                      UriComponentsBuilder.fromHttpUrl(partyDataProperties.getCanonicalMapUri())
                          .queryParam(
                              partyDataProperties.getCanonicalMapIdentifierTypeQueryParam(),
                              partyDataProperties.getCanonicalMapIdentifierTypeCui())
                          .queryParam(
                              partyDataProperties.getCanonicalMapIdentifierQueryParam(),
                              customerUniqueIdentifier)
                          .toUriString())
                  .includeAccessToken(true)
                  .cynEvent(partyDataProperties.getCanonicalMapCynEvent())
                  .returnType(Object.class)
                  .build());

      return objectMapper
          .convertValue(
              response.getBody(), new TypeReference<GenericResponse<CanonicalMapResponse>>() {})
          .getBody();
    } catch (Exception e) {
      throw new BaseServiceException(CPR_ERR000002.name(), Collections.emptyList(), e);
    }
  }*/

  /**
   * Map canonical map response to customer profile entity.
   *
   * @param response party data canonical map response
   * @return customer profile canonical map entity
   */
  private CanonicalIdMapEntity mapCanonicalIdMapEntity(/*final CanonicalMapResponse response*/) {
    var entity = new CanonicalIdMapEntity();
   /* var mappings = response.getMappings();
    entity.setPartyId(
        mappings.stream()
            .map(CanonicalMap::getPartyId)
            .filter(Objects::nonNull)

            // TODO change type to Long in customer-profile client and update all dependent services

            .map(Math::toIntExact)
            .distinct()
            .collect(Collectors.toList()));
    entity.setCifId(
        mappings.stream()
            .map(CanonicalMap::getCifId)
            .filter(StringUtils::hasText)
            .distinct()
            .collect(Collectors.toList()));
    entity.setCustomerRelationshipAgreementId(
        mappings.stream()
            .map(CanonicalMap::getCustomerRelationshipAgreementId)
            .filter(Objects::nonNull)

            // TODO change type to Long in customer-profile client and update all dependent services

            .map(Math::toIntExact)
            .distinct()
            .collect(Collectors.toList()));
    entity.setEqCustomerNumbers(
        mappings.stream()
            .map(CanonicalMap::getEqcustomernumber)
            .filter(StringUtils::hasText)
            .distinct()
            .collect(Collectors.toList()));
    entity.setOlbUserId(
        mappings.stream()
            .map(CanonicalMap::getOlbuserId)
            .filter(StringUtils::hasText)
            .distinct()
            .collect(Collectors.toList()));
    entity.setCustomerUniqueIdentifier(
        mappings.stream()
            .map(CanonicalMap::getCustomerUniqueIdentifier)
            .filter(StringUtils::hasText)
            .distinct()
            .collect(Collectors.toList()));*/
    return entity;
  }

  /**
   * Retrieve the party for a given CCDS party id.
   *
   * @param ccdsPartyId the CCDS party id
   * @return the party response
   */
  /*public PartyRetrieveResponse getParty(String ccdsPartyId) {
    try {
      var response =
          remoteInvocationService.executeRemoteService(
              RemoteInvocationContext.builder()
                  .httpMethod(HttpMethod.GET)
                  .url(
                      UriComponentsBuilder.fromHttpUrl(partyDataProperties.getGetPartyUri())
                          .queryParam(partyDataProperties.getCcdsPartyIdQueryParam(), ccdsPartyId)
                          .toUriString())
                  .includeAccessToken(true)
                  .cynEvent(partyDataProperties.getGetPartyCynEvent())
                  .returnType(Object.class)
                  .build());

      return objectMapper
          .convertValue(
              response.getBody(), new TypeReference<GenericResponse<PartyRetrieveResponse>>() {})
          .getBody();
    } catch (Exception e) {
      throw new BaseServiceException(CPR_ERR000002.name(), Collections.emptyList(), e);
    }
  }*/

  /**
   * Get party contact point locations.
   *
   * @param ccdsPartyId the customer unique identifier
   * @return list with party locations
   */
  List<PartyLocation>
      getContactPointLocations(final String ccdsPartyId) {
    /*try {
      var response =
          remoteInvocationService.executeRemoteService(
              RemoteInvocationContext.builder()
                  .httpMethod(HttpMethod.GET)
                  .url(
                      UriComponentsBuilder.fromHttpUrl(
                              partyDataProperties.getGetPartyContactPointsUri())
                          .queryParam(partyDataProperties.getCcdsPartyIdQueryParam(), ccdsPartyId)
                          .toUriString())
                  .includeAccessToken(true)
                  .cynEvent(partyDataProperties.getGetPartyContactPointsCynEvent())
                  .returnType(Object.class)
                  .build());

      return objectMapper
          .convertValue(
              response.getBody(),
              new TypeReference<GenericResponse<PartyContactPointsResponse>>() {})
          .getBody()
          .getContactPoints();
    } catch (Exception e) {
      throw new BaseServiceException(CPR_ERR000002.name(), Collections.emptyList(), e);
    }*/
	  return null;
  }

  /**
   * Retrieve the CustomerProfile entity by calling the different endpoints needed from party data
   * management component service.
   *
   * @param customerUniqueIdentifier unique identifier of customer
   * @return the CustomerProfile entity
   */
  @Override
  public CustomerProfile getCustomerProfileEntity(String customerUniqueIdentifier) {
//    var canonicalIdMap = getCanonicalIdMap(customerUniqueIdentifier);
//    var party = getParty(customerUniqueIdentifier);
//    var contactPointLocations = getContactPointLocations(customerUniqueIdentifier);

    var customerProfile = new CustomerProfile();
    /*var profileEntity = new ProfileEntity();
    customerProfile.setProfileEntity(profileEntity);
    customerProfile.setCanonicalIdMapEntity(mapCanonicalIdMapEntity(canonicalIdMap));

    var person = (Person) party.getPartyEntity();
    var customerName = new CustomerNameEntity();
    customerName.setTitle(person.getTitle());
    customerName.setFirstName(person.getFName());
    customerName.setMiddleName(person.getMName());
    customerName.setLastName(person.getLName());
    profileEntity.setCustomerName(customerName);
    profileEntity.setDob(person.getBirthDate().toString());

    profileEntity.setUkCitizen(isUkCitizen((Person) party.getPartyEntity()));

    profileEntity.setContactPoints(
        party.getContactPoints().stream()
            .map(ContactPointProperty::getContactPoint)
            .filter(Objects::nonNull)
            .map(cp -> ContactPoint.valueOf(cp.toString()))
            .collect(Collectors.toList()));

    try {
      var contactPoints =
          contactPointLocations.stream()
              .map(
                  pl ->
                      PartyLocation.builder()
                          .partyLocationType(ContactPoint.valueOf(pl.getPartyLocationType().name()))
                          .partyLocationRole(
                              pl.getPartyLocationRole() != null
                                  ? PartyLocationRole.valueOf(pl.getPartyLocationRole().name())
                                  : null)
                          .location(
                              mapLocationFromPartyLocation(
                                  pl.getPartyLocationType(), pl.getLocation()))
                          .build())
              .collect(Collectors.toList());
      profileEntity.setPartyLocations(contactPoints);
    } catch (Exception e) {
      throw new BaseServiceException(CPR_ERR000003.name(), Collections.emptyList(), e);
    }
*/
    return customerProfile;
  }

  /**
   * Map a party data location to customer profile party location representation.
   *
   * @param partyLocationType type of party location
   * @param locationEntity location entity
   * @return customer profile representation of party location
   */
  LocationEntity mapLocationFromPartyLocation(
      //final ContactPointValue partyLocationType,
      final LocationEntity
          locationEntity) {
    /*var baseLocation = (BaseLocation) locationEntity;
    switch (partyLocationType) {
      case ELECTRONIC_ADDRESS:
        var emails = ((LocationElectronicAddress) locationEntity).getLocationAddress();
        if (emails.isEmpty()) {
          return com.cyn.ccds.customer.profile.client.location.LocationElectronicAddress.builder()
              .build();
        }
        var emailAddress =
            modelMapper.map(
                emails.get(FIRST_INDEX),
                com.cyn.ccds.customer.profile.client.location.ElectronicAddress.class);
        var locationElectronicAddress =
            com.cyn.ccds.customer.profile.client.location.LocationElectronicAddress.builder()
                .locationAddress(emailAddress)
                .build();
        mapCustomerProfileBaseLocationFromPartyBaseLocation(
            baseLocation, locationElectronicAddress);
        return locationElectronicAddress;
      case PHONE_ADDRESS:
      case MOBILE_ADDRESS:
        var phones = ((LocationPhoneAddress) locationEntity).getLocationAddress();
        if (phones.isEmpty()) {
          return com.cyn.ccds.customer.profile.client.location.LocationPhoneAddress.builder()
              .build();
        }
        var phoneAddress =
            modelMapper.map(
                phones.get(FIRST_INDEX),
                com.cyn.ccds.customer.profile.client.location.PhoneAddress.class);
        var locationPhoneAddress =
            com.cyn.ccds.customer.profile.client.location.LocationPhoneAddress.builder()
                .locationAddress(phoneAddress)
                .build();
        mapCustomerProfileBaseLocationFromPartyBaseLocation(baseLocation, locationPhoneAddress);
        return locationPhoneAddress;
      case POSTAL_ADDRESS:
      case TRADING_ADDRESS:
        var postals = ((LocationPostalAddress) locationEntity).getLocationAddress();
        if (postals.isEmpty()) {
          return com.cyn.ccds.customer.profile.client.location.LocationPostalAddress.builder()
              .build();
        }
        var postalAddress =
            modelMapper.map(
                postals.get(FIRST_INDEX),
                com.cyn.ccds.customer.profile.client.location.PostalAddress.class);
        postalAddress
            .getLegacyEqPostalAddress()
            .setPostCode(postals.get(FIRST_INDEX).getPostOfficeBox());
        var locationPostalAddress =
            com.cyn.ccds.customer.profile.client.location.LocationPostalAddress.builder()
                .locationAddress(postalAddress)
                .build();
        mapCustomerProfileBaseLocationFromPartyBaseLocation(baseLocation, locationPostalAddress);
        return locationPostalAddress;
      default:
        throw new BaseServiceException(CPR_ERR000008.name());
    }*/
	  return null;
  }

  /**
   * Map a party data base location to a customer profile base location.
   *
   * @param source party data base location
   * @param target customer profile base location
   */
  private void mapCustomerProfileBaseLocationFromPartyBaseLocation(
      final BaseLocation source) {
//    target.setLocationStatus(source.getLocationStatus());
//    target.setLocationType(source.getLocationType());
//    target.setLocationValue(source.getLocationValue());
  }

}
