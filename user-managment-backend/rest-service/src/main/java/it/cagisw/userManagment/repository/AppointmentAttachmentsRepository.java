package it.cagisw.userManagment.repository;

import org.springframework.data.repository.CrudRepository;

import it.cagisw.userManagment.models.AppointmentAttachments;

/** Repository class for AppointmentAttachments */
public interface AppointmentAttachmentsRepository
    extends CrudRepository<AppointmentAttachments, Long> {}
