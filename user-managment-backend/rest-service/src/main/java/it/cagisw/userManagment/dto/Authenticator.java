package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Authenticator for otp login flow. */
@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class Authenticator {

  /** One time password */
  private String otp;
}
