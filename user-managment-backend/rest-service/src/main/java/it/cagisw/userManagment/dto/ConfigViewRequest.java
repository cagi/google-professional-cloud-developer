package it.cagisw.userManagment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Structure to request event configs. Customer unique id is not present here because it can be read
 * from security context.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfigViewRequest {

  /** Account number for the customer. */
  private String accountNumber;
}
