package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Request structure to get binding-id. */
@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class BindingIdRequest {

  /** Activation password. */
  @NotBlank private String actp;
}
