package it.cagisw.userManagment.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Request structure for session validation. */
@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class SessionValidationRequest {

  /** User id. */
  @NotBlank private String userId;

  /** Request id. */
  @NotBlank private String requestId;

  /** Device details for session validation */
  @NotNull @Valid private DeviceDetails deviceDetails;
}
