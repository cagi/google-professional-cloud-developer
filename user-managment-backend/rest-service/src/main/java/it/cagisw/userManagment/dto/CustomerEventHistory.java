package it.cagisw.userManagment.dto;

import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** An event history entry for a customer. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerEventHistory {

  /** Identifier of the history entry. */
  private long historyId;

  /** Identifier of the customer. */
  private String customerUniqueId;

  /** Request type of the history item. */
  private String requestType;

  /** Short message of the item. */
  private String shortMessage;

  /** Parent event configuration. */
  private CustomerEventConfiguration eventConf;

  /** Group of the event. */
  private EventGroup eventGroup;

  /** Master event definition. */
  private Event event;

  /** Master channel definition. */
  private Channel channel;

  /** Message that has been transmitted. */
  private String message;

  /** Event group type. */
  private String eventGroupType;

  /** Account number that event belongs to. */
  private String accountNumber;

  /** The actual time of transmission. */
  private ZonedDateTime timeOfTransmission;

  /** Time of event creation. */
  private ZonedDateTime createdOn;

  /** Time of event update. */
  private ZonedDateTime updatedOn;
}
