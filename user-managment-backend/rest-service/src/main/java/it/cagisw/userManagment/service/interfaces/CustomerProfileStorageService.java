package it.cagisw.userManagment.service.interfaces;

import it.cagisw.userManagment.models.CustomerProfile;

/** Service to perform storage operations for the profile of a customer. */
public interface CustomerProfileStorageService {

  /**
   * Retrieve the CustomerProfile entity from DB if not available will fetch from party data
   * component service, sensitive data will be encrypted for storage and decrypted at retrieval.
   *
   * @param customerUniqueIdentifier the unique identifier for a customer
   * @return the CustomerProfile entity
   */
  CustomerProfile getCustomerProfile(String customerUniqueIdentifier);

  /**
   * Retrieve latest information from party data component service, encrypts sensitive data before
   * storing to DB.
   *
   * @param customerUniqueIdentifier the unique identifier for a customer
   */
  void createOrUpdateCustomerProfile(String customerUniqueIdentifier);

  /**
   * Updates the relationship of the customer with the bank.
   *
   * @param customerUniqueIdentifier the unique identifier for a customer
   * @param state state to be saved for this customer relationship
   */
  void updateCustomerProfileRelationshipState(String customerUniqueIdentifier, boolean state);
}
