package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Base class for profile requests. */
@Data
@NoArgsConstructor
public abstract class BaseProfileRequest {

  /** The unique identifier of a customer. */
  @NotNull private String customerUniqueIdentifier;
}
