package it.cagisw.userManagment.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Response for the status of a profile. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProfileStatusResponse {

  /** Status of the profile */
  @NotNull private boolean status;
}
