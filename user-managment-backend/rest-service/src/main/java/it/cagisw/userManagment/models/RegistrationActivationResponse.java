package it.cagisw.userManagment.models;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/** A Pojo representing a registration activation response for a user. */
@Entity
@Data
@Table(name = "REGISTRATION_ACTIVATION_RESPONSE")
public class RegistrationActivationResponse {

  /** Primary Key */
  @Column(name = "ID")
  @Id
  @GeneratedValue
  private long id;

  /** Reference to the activation request. */
  @ManyToOne
  @JoinColumn(name = "REG_REG_ID")
  private RegistrationActivationRequest registrationActivationRequest;

  /** Http code of the activation response. */
  @Column(name = "HTTP_CODE")
  private String httpCode;

  /** Activation code returned by Onespan */
  @Column(name = "ACTIVATION_CODE")
  private String activationCode;

  /** Serial number of activator device. */
  @Column(name = "SERIALNUMBER")
  private String serialNumber;

  /** Exception json for error responses. */
  @Column(name = "EXCEPTION_JSON")
  private String exceptionJson;

  /** Created on audit field. */
  @Column(name = "CREATED_ON")
  private ZonedDateTime createdOn;
}
