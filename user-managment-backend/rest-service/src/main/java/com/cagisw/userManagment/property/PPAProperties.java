package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.UriComponentsBuilder;

/** Properties to operate with privacy policy affirmation service. */
@Configuration
@ConfigurationProperties(prefix = "ppa")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PPAProperties {

  /** URI to read privacy policy */
  private String readPolicyUri;

  /** Cyn event used when reading privacy policy */
  private String readPolicyCynEvent;

  /** URI to validate a decision */
  private String validateDecisionUri;

  /** Cyn event used when validating a decision */
  private String validateDecisionCynEvent;

  /** URI to create a decision */
  private String createDecisionUri;

  /** Cyn event used when creating a decision */
  private String createDecisionCynEvent;

  /** The query parameter name for a page URI */
  private String pageUriQueryParam;

  /** The query parameter name for channel code */
  private String channelCodeQueryParam;

  /** URI used for registration */
  private String registrationUri;

  /**
   * Construct a read privacy policy URI string from a Page URI and channel code.
   *
   * @param namespaceUri namespace page URI
   * @param channelCode channel code (for example web/mobile)
   * @return string URI with query parameters set
   */
  public String getReadPolicyUriFor(final String namespaceUri, final String channelCode) {
    return UriComponentsBuilder.fromHttpUrl(readPolicyUri)
        .queryParam(getPageUriQueryParam(), namespaceUri)
        .queryParam(getChannelCodeQueryParam(), channelCode)
        .toUriString();
  }
}
