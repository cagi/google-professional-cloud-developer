package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** Properties to operate with auth migration helper service. */
@Configuration
@ConfigurationProperties(prefix = "auth.migration")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AuthMigrationProperties {

  /** evaluate URI */
  private String evaluateUri;

  /** Cyn event used for evaluation */
  private String evaluateCynEvent;

  /** MFA token evaluate URI */
  private String mfaEvaluateUri;

  /** Cyn event used for MFA token evaluation */
  private String mfaEvaluateCynEvent;
}
