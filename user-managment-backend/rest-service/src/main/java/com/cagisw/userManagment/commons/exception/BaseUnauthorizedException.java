package com.cagisw.userManagment.commons.exception;

import java.util.List;

/** Exception to be thrown when request is unauthorized */
public class BaseUnauthorizedException extends BaseServiceException {

  /** Serial version of class. */
  private static final long serialVersionUID = 6738568843135124904L;

  /**
   * Constructor for unauthorized exceptions
   *
   * @param errorCode error code that will be used for looking up messages from bundle
   * @param apiSubErrorList error sub errors causing the problem
   */
  public BaseUnauthorizedException(String errorCode, List<SubError> apiSubErrorList) {
    super(errorCode, apiSubErrorList);
  }

  /**
   * Constructor with error code and a root cause
   *
   * @param errorCode unique error code for failure
   * @param apiSubErrorList sub errors if exists
   * @param cause a cause exception
   */
  public BaseUnauthorizedException(
      String errorCode, List<SubError> apiSubErrorList, Throwable cause) {
    super(errorCode, apiSubErrorList, cause);
  }
}
