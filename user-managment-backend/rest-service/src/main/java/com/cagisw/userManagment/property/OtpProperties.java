package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to operate with OTP service. */
@Component
@ConfigurationProperties(prefix = "otp")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OtpProperties {

  /** Create URI */
  private String createUri;

  /** Create CYN event */
  private String createCynEvent;

  /** Resend URI */
  private String resendUri;

  /** Resend CYN event */
  private String resendCynEvent;

  /** Evaluate URI */
  private String evaluateUri;

  /** Evaluate CYN event */
  private String evaluateCynEvent;

  /** Introspect URI */
  private String introspectUri;

  /** Introspect CYN event */
  private String introspectCynEvent;

  /** Header name for MFA token */
  private String headerNameMfaToken;
}
