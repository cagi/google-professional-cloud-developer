package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to operate with customer profile service. */
@Component
@ConfigurationProperties(prefix = "customer.profile")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerProfileProperties {

  /** Uri to retrieve the customer profile */
  private String retrieveUri;

  /** Cyn event to be used for retrieve endpoint */
  private String retrieveCynEvent;

  /** Canonical map URI */
  private String canonicalMapUri;

  /** Cyn event canonical map */
  private String canonicalMapCynEvent;

  /** Evaluate URI */
  private String evaluateUri;

  /** Evaluate CYN event */
  private String evaluateCynEvent;
}
