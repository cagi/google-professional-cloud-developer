package com.cagisw.userManagment.security;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Configuration options for cyn authorization */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CynAuthorisation {

  /** List of locations that customerUniqueId might exist. */
  private List<AuthorisationLocation> authorisationLocations;

  /** Permitted roles to bypass cyn authorization check. */
  private List<String> permittedRoles;
}
