package com.cagisw.userManagment.commons.exception;

import java.util.Collections;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/** Exception thrown by another CYN service. */
@Getter
public class RemoteCynServiceException extends RuntimeException {

  /** Serial version of class */
  private static final long serialVersionUID = 9042073288578069635L;

  /** Message */
  private final String message;

  /** Error code */
  private final String errorCode;

  /** Http status */
  private final HttpStatus httpStatus;

  /**
   * Private constructor to create new RemoteCynServiceException instance.
   *
   * @param message message
   * @param errorCode error code
   * @param httpStatus http status
   * @param cause error that caused it
   */
  private RemoteCynServiceException(
      String message, String errorCode, HttpStatus httpStatus, Throwable cause) {
    super(message, cause);
    this.message = message;
    this.errorCode = errorCode;
    this.httpStatus = httpStatus;
  }

  /**
   * Private constructor to create new RemoteCynServiceException instance.
   *
   * @param message message
   * @param errorCode error code
   * @param httpStatus http status
   */
  private RemoteCynServiceException(String message, String errorCode, HttpStatus httpStatus) {
    super(message);
    this.message = message;
    this.errorCode = errorCode;
    this.httpStatus = httpStatus;
  }

  /**
   * Private method to create new RemoteCynServiceException instance.
   *
   * @param message message
   * @param errorCode error code
   * @param httpStatus http status
   * @param cause error that caused it
   * @return error instance
   */
  private static RemoteCynServiceException _createRemoteServiceException(
      String message, String errorCode, HttpStatus httpStatus, Throwable cause) {
    if (cause == null) {
      return new RemoteCynServiceException(message, errorCode, httpStatus);
    } else {
      return new RemoteCynServiceException(message, errorCode, httpStatus, cause);
    }
  }

  /**
   * Create new RemoteCynServiceException instance, this method is public ONLY for testing purposes,
   * otherwise instances should be created only in cyn-commons.
   *
   * @param message message
   * @param errorCode error code
   * @param httpStatus http status
   * @return error instance
   */
  public static RemoteCynServiceException createRemoteServiceException(
      String message, String errorCode, HttpStatus httpStatus) {
    return _createRemoteServiceException(message, errorCode, httpStatus, null);
  }

  /**
   * Create new RemoteCynServiceException instance, this method is public ONLY for testing purposes,
   * otherwise instances should be created only in cyn-commons.
   *
   * @param message message
   * @param errorCode error code
   * @param httpStatus http status
   * @param cause error that caused it
   * @return error instance
   */
  public static RemoteCynServiceException createRemoteServiceException(
      String message, String errorCode, HttpStatus httpStatus, Throwable cause) {
    return _createRemoteServiceException(message, errorCode, httpStatus, cause);
  }

  /**
   * Convert to BaseServiceException if you want to manually propagate it.
   *
   * @return converted BaseServiceException
   */
  public BaseServiceException convertToBaseServiceException() {
    return new BaseServiceException(
        this.message,
        this.getCause(),
        this.errorCode,
        this.message,
        this.httpStatus,
        Collections.emptyList());
  }
}
