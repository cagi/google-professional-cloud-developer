package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to operate with device administration service. */
@Component
@ConfigurationProperties(prefix = "device.administration")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeviceAdminProperties {

  /** URI to get devices */
  private String retrieveDevicesUri;

  /** Get devices CYN event */
  private String retrieveDevicesCynEvent;

  /** URI to unregister devices */
  private String unregisterDeviceUri;

  /** Unregister devices CYN event */
  private String unregisterDeviceCynEvent;

  /** Header name CUI */
  private String headerNameCui;
}
