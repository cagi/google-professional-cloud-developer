package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to operate with mfa orchestrator service. */
@Component
@ConfigurationProperties(prefix = "mfa.orchestrator")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MfaOrchestratorProperties {

  /** validate token URI */
  private String validateTokenUri;

  /** validate token CYN event */
  private String validateTokenCynEvent;

  /** header name */
  private String headerNameScaToken;
}
