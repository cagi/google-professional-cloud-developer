package com.cagisw.userManagment.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Encryption flags for a path */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PayloadEncryptionMetadata {
  /** Flag to indicate request is encrypted. */
  private boolean request;

  /** Flag to indicate response should be encrypted. */
  private boolean response;
}
