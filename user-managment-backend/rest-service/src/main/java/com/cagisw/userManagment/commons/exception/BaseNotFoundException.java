package com.cagisw.userManagment.commons.exception;

import java.util.List;

/** A base class for entity not found exceptions to be thrown in business layer. */
public class BaseNotFoundException extends BaseServiceException {

  /** Serial version of class */
  private static final long serialVersionUID = -3121001328120088222L;

  /**
   * Crete an error with an error code and sub errors if exist
   *
   * @param errorCode the error key that will be used for reading message from message.properties
   * @param apiSubErrorList the list of sub errors if exist
   */
  public BaseNotFoundException(String errorCode, List<SubError> apiSubErrorList) {
    super(errorCode, apiSubErrorList);
  }

  /**
   * Constructor with error code and a root cause
   *
   * @param errorCode unique error code for failure
   * @param apiSubErrorList sub errors if exists
   * @param cause a cause exception
   */
  public BaseNotFoundException(String errorCode, List<SubError> apiSubErrorList, Throwable cause) {
    super(errorCode, apiSubErrorList, cause);
  }
}
