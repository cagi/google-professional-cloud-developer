package com.cagisw.userManagment.commons.exception;

import java.util.List;

/** A base class for exceptions that are caused by security errors occurs during api to to call */
public class BaseClientEncryptionException extends BaseServiceException {

  /** Serial version of the class. */
  private static final long serialVersionUID = 5170353090364745005L;

  /**
   * @param errorCode
   * @param apiSubErrorList
   */
  public BaseClientEncryptionException(String errorCode, List<SubError> apiSubErrorList) {
    super(errorCode, apiSubErrorList);
  }

  /**
   * @param errorCode
   * @param apiSubErrorList
   * @param cause
   */
  public BaseClientEncryptionException(
      String errorCode, List<SubError> apiSubErrorList, Throwable cause) {
    super(errorCode, apiSubErrorList, cause);
  }
}
