package com.cagisw.userManagment.security;

/** Enumeration for the cui parameter for authorization. */
public enum AuthorizationLocationType {

  /** Location is request body. */
  BODY,

  /** Location is query parameter. */
  QUERY
}
