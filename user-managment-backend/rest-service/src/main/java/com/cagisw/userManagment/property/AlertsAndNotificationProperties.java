package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** Properties to perform operations with alerts and notification component service. */
@Configuration
@ConfigurationProperties(prefix = "alerts.and.notification")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AlertsAndNotificationProperties {

  /** Notify URI */
  private String notifyUri;

  /** Notify CYN event */
  private String notifyCynEvent;
}
