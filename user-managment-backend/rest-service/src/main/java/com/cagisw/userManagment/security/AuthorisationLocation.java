package com.cagisw.userManagment.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** The location configuration for cui parameter for authorization. */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class AuthorisationLocation {

  /** The type of the location. */
  private AuthorizationLocationType type;

  /** The property name of the field. */
  private String propertyName;
}
