package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** Properties to perform operations with Digipass adapter service. */
@Configuration
@ConfigurationProperties(prefix = "digipass.adapter")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DigipassAdapterProperties {

  /** User Registration URI */
  private String userRegistrationUri;

  /** Login challenge URI */
  private String loginChallengeUri;

  /** User Registration Id header */
  private String registrationIdHeader;

  /** User User Id header */
  private String userIdHeader;

  /** User registration CYN event */
  private String userRegistrationCynEvent;

  /** Login challenge CYN event */
  private String loginChallengeCynEvent;

  /** Device provision URI */
  private String deviceProvisionUri;

  /** Device provision CYN event */
  private String deviceProvisionCynEvent;

  /** Device acivation URI */
  private String deviceActivationUri;

  /** Device activation CYN event */
  private String deviceActivationCynEvent;

  /** Registered user URI */
  private String registeredUserUri;

  /** Registered user CYN event */
  private String registeredUserCynEvent;

  /** Login URI */
  private String loginUri;

  /** Login CYN event */
  private String loginCynEvent;

  /** Unassign URI */
  private String unassignUri;

  /** Unassign CYN event */
  private String unassignCynEvent;

  /** User unlock URI */
  private String userUnlockUri;

  /** User unlock CYN event */
  private String userUnlockCynEvent;
}
