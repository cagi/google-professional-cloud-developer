package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to operate with email service. */
@Component
@ConfigurationProperties(prefix = "email")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailProperties {

  /** Notify URI */
  private String notifyUri;

  /** Notify CYN event */
  private String notifyCynEvent;
}
