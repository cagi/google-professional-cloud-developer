package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to operate with event subscription filter service. */
@Component
@ConfigurationProperties(prefix = "event.filtration")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EventFiltrationProperties {

  /** Config URI */
  private String configUri;

  /** Config CYN event */
  private String configCynEvent;

  /** History capture URI */
  private String historyCaptureUri;

  /** History capture CYN event */
  private String historyCaptureCynEvent;

  /** Subscription evaluate URI */
  private String subscriptionEvaluateUri;
}
