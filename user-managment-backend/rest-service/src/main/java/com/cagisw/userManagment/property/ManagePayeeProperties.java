package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** Properties to perform operations with manage payee component service. */
@Configuration
@ConfigurationProperties(prefix = "manage.payee")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ManagePayeeProperties {

  /** Retrieve debit accounts URI */
  private String retrieveDebitAccountsUri;

  /** Retrieve all URI */
  private String retrieveAllPayeesUri;

  /** Retrieve all CYN event */
  private String retrieveAllPayeesCynEvent;

  /** Count URI */
  private String countUri;

  /** Count CYN event */
  private String countCynEvent;

  /** Add URI */
  private String addUri;

  /** Add CYN event */
  private String addCynEvent;

  /** Update URI */
  private String updateUri;

  /** Update CYN event */
  private String updateCynEvent;

  /** Delete URI */
  private String deleteUri;

  /** Delete CYN event */
  private String deleteCynEvent;

  /** Retrieve payee URI */
  private String retrievePayeeUri;

  /** Retrieve payee CYN event */
  private String retrievePayeeCynEvent;

  /** Pending approval URI */
  private String pendingApprovalUri;

  /** Pending approval CYN event */
  private String pendingApprovalCynEvent;

  /** Pending approval count URI */
  private String pendingApprovalCountUri;

  /** Pending approval count CYN event */
  private String pendingApprovalCountCynEvent;

  /** Pending approval review URI */
  private String pendingApprovalReviewUri;

  /** Pending approval review CYN event */
  private String pendingApprovalReviewCynEvent;
}
