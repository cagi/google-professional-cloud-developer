package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** Properties to operate with bank val adapter service. */
@Configuration
@ConfigurationProperties(prefix = "bank.val")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BankValProperties {

  /** biccode validate uri */
  private String biccodeValidateUri;

  /** biccode validate cyn event */
  private String biccodeValidateCynEvent;

  /** iban validate uri */
  private String ibanValidateUri;

  /** iban validate cyn event */
  private String ibanValidateCynEvent;

  /** acc validate uri */
  private String accValidateUri;

  /** acc validate cyn event */
  private String accValidateCynEvent;

  /** enhanced validate uri */
  private String enhancedValidateUri;

  /** enhanced validate cyn event */
  private String enhancedValidateCynEvent;
}
