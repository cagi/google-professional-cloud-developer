package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to operate with channel router service. */
@Component
@ConfigurationProperties(prefix = "channel.router")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChannelRouterProperties {

  /** Route URI */
  private String routeUri;

  /** Route CYN event */
  private String routeCynEvent;
}
