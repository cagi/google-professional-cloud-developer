package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to operate with ibav service. */
@Component
@ConfigurationProperties(prefix = "ibav")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IbavProperties {

  /** Evaluate URI */
  private String evaluateUri;

  /** Evaluate CYN event */
  private String evaluateCynEvent;
}
