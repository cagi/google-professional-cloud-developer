package com.cagisw.userManagment.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @deprecated since 1.2.0 Please use security.yaml to configure decryption and encryption.An
 *     annotation to enable body based authorization.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Deprecated(since = "1.2.0")
public @interface CynBodyAuthorization {

  /**
   * The path to the customer unique id location within a request body. For example use
   * 'customerUniqueId' for { "field" : :"value", "customerUniqueId" : "value" }
   *
   * @return path to be used by CynBodyAuthorizer
   */
  String customerUniqueIdPath() default "";
}
