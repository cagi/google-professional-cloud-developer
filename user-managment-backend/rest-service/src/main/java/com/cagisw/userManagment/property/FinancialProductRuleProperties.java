package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to operate with financial product rule service. */
@Component
@ConfigurationProperties(prefix = "financial.product.rule")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FinancialProductRuleProperties {

  /** Account type codes URI */
  private String accountTypeCodesUri;

  /** Account type codes CYN event */
  private String accountTypeCodesCynEvent;

  /** Retrieve rules URI */
  private String retrieveRulesUri;

  /** Retrieve rules CYN event */
  private String retrieveRulesCynEvent;

  /** Transaction from account URI */
  private String transactionFromAccountUri;

  /** Transaction from account CYN event */
  private String transactionFromAccountCynEvent;

  /** Transaction menu URI */
  private String transactionMenuUri;

  /** Transaction menu CYN event */
  private String transactionMenuCynEvent;
}
