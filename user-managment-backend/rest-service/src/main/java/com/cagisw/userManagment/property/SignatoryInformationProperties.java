package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to operate with signatory information service. */
@Component
@ConfigurationProperties(prefix = "signatory.information")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignatoryInformationProperties {

  /** approval retrieve URI */
  private String approvalRetrieveUri;

  /** approval retrieve CYN event */
  private String approvalRetrieveCynEvent;

  /** Signatory access retrieve URI */
  private String signatoryAccessRetrieveUri;

  /** Signatory access retrieve CYN event */
  private String signatoryAccessRetrieveCynEvent;
}
