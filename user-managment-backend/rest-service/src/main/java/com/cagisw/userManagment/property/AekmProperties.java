package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** Properties to operate with aekm service. */
@Configuration
@ConfigurationProperties(prefix = "aekm")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AekmProperties {

  /** Private key URI */
  private String getPrivateKeyUri;

  /** Public key URI */
  private String getPublicKeyUri;

  /** App id query parameter */
  private String appIdQueryParam;
}
