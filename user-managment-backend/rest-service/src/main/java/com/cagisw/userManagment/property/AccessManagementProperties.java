package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to operate with access management service. */
@Component
@ConfigurationProperties(prefix = "access.management")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccessManagementProperties {

  /** Access level URI */
  private String accessLevelUri;

  /** Access level CYN event */
  private String accessLevelCynEvent;

  /** Evaluate URI */
  private String evaluateUri;

  /** Evaluate CYN event */
  private String evaluateCynEvent;

  /** Access types URI */
  private String accessTypesUri;

  /** Access types CYN event */
  private String accessTypesCynEvent;
}
