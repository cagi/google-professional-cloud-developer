package com.cagisw.userManagment.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.support.DefaultPropertySourceFactory;

/** Properties for CYN service configurations. */
@Configuration
@ConfigurationProperties
@PropertySource(
    value = "${service.config.file.path:classpath:service.properties}",
    factory = DefaultPropertySourceFactory.class)
public class ServicesConfigProperties {}
