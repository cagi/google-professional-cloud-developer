package com.cagisw.userManagment.security;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** The main structure to represent permissions and security configurations for a path. */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Permission {

  /** Url of the endpoint */
  private String url;

  /** Flag to indicate if authentication is enabled. */
  private boolean authenticationEnabled;

  /** Optional HTTP method to apply rule for a specific endpoint with http method. */
  private String httpMethod;

  /** Encryption metadata flags. */
  private PayloadEncryptionMetadata payloadEncryption;

  /** List of roles that are allowed to call this endpoint. */
  private List<String> permittedRoles = new ArrayList<>();

  /** Cyn authorization structure. */
  private CynAuthorisation cynAuthorisation;
}
