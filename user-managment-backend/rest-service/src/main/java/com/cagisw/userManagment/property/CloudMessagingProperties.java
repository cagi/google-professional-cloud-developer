package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to operate with cloud messaging service. */
@Component
@ConfigurationProperties(prefix = "cloud.messaging")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CloudMessagingProperties {

  /** Notify URI */
  private String notifyUri;

  /** Notify CYN event */
  private String notifyCynEvent;
}
