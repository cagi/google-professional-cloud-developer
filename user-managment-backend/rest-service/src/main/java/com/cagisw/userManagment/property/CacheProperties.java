package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to be used for operations performed against cache service. */
@Component
@ConfigurationProperties(prefix = "cache")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CacheProperties {

  /** Header name for cache key */
  private String headerNameKey;

  /** Header name for cache group id */
  private String headerNameGroupId;

  /** Retrieve URI */
  private String retrieveUri;

  /** Create URI */
  private String createUri;

  /** Delete URI */
  private String deleteUri;

  /** CYN event to be passed to fetch operations */
  private String cynEventRetrieve;

  /** CYN event to be passed to create operations */
  private String cynEventCreate;

  /** CYN event to be passed to delete operations */
  private String cynEventDelete;
}
