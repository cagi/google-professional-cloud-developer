package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.UriComponentsBuilder;

/** Properties to operate with terms and conditions service. */
@Configuration
@ConfigurationProperties(prefix = "tc")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class TncProperties {

  /** URI to read terms and conditions */
  private String readTncUri;

  /** Cyn event used when reading terms and conditions */
  private String readTncCynEvent;

  /** URI to validate a decision */
  private String validateDecisionUri;

  /** Cyn event used when validating a decision */
  private String validateDecisionCynEvent;

  /** URI to create a decision */
  private String createDecisionUri;

  /** Cyn event used when creating a decision */
  private String createDecisionCynEvent;

  /** The query parameter name for a page URI */
  private String pageUriQueryParam;

  /** The query parameter name for channel code */
  private String channelCodeQueryParam;

  /** URI used for registration */
  private String registrationUri;

  /**
   * Construct a URI string from a Page URI and channel code.
   *
   * @param endpointUri URI of the endpoint to be called
   * @param namespaceUri namespace page URI
   * @param channelCode channel code (for example web/mobile)
   * @return string URI with query parameters set
   */
  private String constructUriFor(
      final String endpointUri, final String namespaceUri, final String channelCode) {
    return UriComponentsBuilder.fromHttpUrl(endpointUri)
        .queryParam(getPageUriQueryParam(), namespaceUri)
        .queryParam(getChannelCodeQueryParam(), channelCode)
        .toUriString();
  }

  /**
   * Construct a read terms and conditions URI string from a Page URI and channel code.
   *
   * @param namespaceUri namespace page URI
   * @param channelCode channel code (for example web/mobile)
   * @return string URI with query parameters set
   */
  public String getReadTncUriFor(final String namespaceUri, final String channelCode) {
    return constructUriFor(getReadTncUri(), namespaceUri, channelCode);
  }

  /**
   * Construct a validation terms and conditions URI string from a Page URI and channel code.
   *
   * @param namespaceUri namespace page URI
   * @param channelCode channel code (for example web/mobile)
   * @return string URI with query parameters set
   */
  public String getValidateDecisionUriFor(final String namespaceUri, final String channelCode) {
    return constructUriFor(getValidateDecisionUri(), namespaceUri, channelCode);
  }
}
