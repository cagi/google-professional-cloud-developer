package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to perform operations with customer data management component service. */
@Component
@ConfigurationProperties(prefix = "customer.data.mgmt")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDataMgmtProperties {

  /** URL to retrieve customer agreement */
  private String customerAgreementUrl;

  /** Name of the placeholder variable in path */
  private String customerAgreementPathVariable;
}
