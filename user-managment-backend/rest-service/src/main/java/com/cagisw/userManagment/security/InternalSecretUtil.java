package com.cagisw.userManagment.security;

import lombok.extern.slf4j.Slf4j;

/** Internal secret util. */
@Slf4j
public class InternalSecretUtil {

  /** Encryption algorithm. */
  private static final String ALGORITHM = "PBEWithHMACSHA512AndAES_256";

  /** Separator for string representation of identifiers. */
  private static final String SEPARATOR = ":";

  /** Internal secret to be used for encryption. */
  private static String internalSecretKey = System.getenv("AEKM_INTERNAL_SECRET");

}
