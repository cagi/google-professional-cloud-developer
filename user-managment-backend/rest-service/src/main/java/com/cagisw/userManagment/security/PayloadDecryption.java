package com.cagisw.userManagment.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @deprecated since 1.2.0 Please use security.yaml to configure decryption and encryption. An
 *     annotation mark the a request body been encrypted.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Deprecated(since = "1.2.0")
public @interface PayloadDecryption {}
