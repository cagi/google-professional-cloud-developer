package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** Properties to operate with authentication service. */
@Configuration
@ConfigurationProperties(prefix = "authentication")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AuthenticationProperties {

  /** System login URI */
  private String systemLoginUri;

  /** Introspect URI */
  private String introspectUri;
}
