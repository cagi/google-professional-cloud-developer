package com.cagisw.userManagment.commons.exception;

/** Enumeration for error codes */
public enum ErrorCode {
	/** An error occurred during profile retrieve */
	CPR_ERR000001,

	/** Failed to fetch data from party data management service */
	CPR_ERR000002,

	/** No active location found in party data management service for customer */
	CPR_ERR000003,

	/** No postal address found in party data management service */
	CPR_ERR000004,

	/**
	 * Customer does not have any existing relationship as a party in party data
	 * management
	 */
	CPR_ERR000005,

	/** Failed to encrypt entity */
	CPR_ERR000006,

	/** Failed to decrypt entity */
	CPR_ERR000007,

	/** Party location type not supported */
	CPR_ERR000008,

	/** Failed to access storage */
	CPR_ERR000009,

	/** Failed to fetch data form customer data management service */
	CPR_ERR000010,

	/** Failed to retrieve party id from canonical mapping */
	CPR_ERR000011,

	/** Identifier type not supported */
	CPR_ERR000012, RMA_ERR000006, RMA_ERR000007, RMA_ERR000021, RMA_ERR000004, RMA_ERR000009, RMA_ERR000001,
	RMA_ERR000023, ENC_ERR000002, DEC_ERR000002, RMA_ERR000026, RMA_ERR000013, RMA_ERR000020, RMA_ERR000003,
	RMA_ERR000019, RMA_ERR000018, RMA_ERR000017, RMA_ERR000016, RMA_ERR000015, RMA_ERR000028, RMA_ERR000027, RMA_ERR000024, RMA_ERR000005, RMA_ERR000025
}
