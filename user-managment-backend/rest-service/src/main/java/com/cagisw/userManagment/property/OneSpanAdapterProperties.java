package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** Properties to perform operations with OneSpan adapter service. */
@Configuration
@ConfigurationProperties(prefix = "onespan.adapter")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class OneSpanAdapterProperties {

  /** User login URI */
  private String userLoginUri;

  /** User login CYN event */
  private String userLoginCynEvent;

  /** Biometric registration check URI */
  private String biometricRegistrationCheckUri;

  /** Biometric registration check CYN event */
  private String biometricRegistrationCheckCynEvent;

  /** Registration check status URI */
  private String registrationCheckStatusUri;

  /** Registration check status CYN event */
  private String registrationCheckStatusCynEvent;

  /** Registration URI */
  private String registrationUri;

  /** Registration CYN event */
  private String registrationCynEvent;

  /** User unlock URI */
  private String userUnlockUri;

  /** User unlock CYN event */
  private String userUnlockCynEvent;

  /** User status URI */
  private String userStatusUri;

  /** User status CYN event */
  private String userStatusCynEvent;

  /** Header name for User id */
  private String headerNameUserId;

  /** device registration uri */
  private String deviceRegistrationUri;

  /** device registration cyn event */
  private String deviceRegistrationCynEvent;

  /** transaction validate uri */
  private String transactionValidateUri;

  /** transaction validate cyn event */
  private String transactionValidateCynEvent;
}
