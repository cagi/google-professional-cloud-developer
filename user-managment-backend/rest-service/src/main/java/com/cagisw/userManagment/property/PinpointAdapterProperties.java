package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** Properties to perform operations with pinpoint adapter component service. */
@Configuration
@ConfigurationProperties(prefix = "pinpoint.adapter")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PinpointAdapterProperties {

  /** Eval info URI */
  private String evalInfoUri;

  /** Eval transaction URI */
  private String evalTransactionUri;

  /** Eval payee URI */
  private String evalPayeeUri;
}
