package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.UriComponentsBuilder;

/** Properties to perform operations with party data management component service. */
@Configuration
@ConfigurationProperties(prefix = "party.data.mgmt")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PartyDataMgmtProperties {

  /** The URI to retrieve the canonical id mapping */
  private String canonicalMapUri;

  /** CYN event to be used for canonical id mapping endpoint */
  private String canonicalMapCynEvent;

  /** Query param for identifier value */
  private String canonicalMapIdentifierQueryParam;

  /** Query param for identifier type */
  private String canonicalMapIdentifierTypeQueryParam;

  /** Type to be used as the identifier in the query parameters for customer unique identifier */
  private String canonicalMapIdentifierTypeCui;

  /** Type to be used as the identifier in the query parameters for OLB user ID */
  private String canonicalMapIdentifierTypeOlbuserid;

  /** The URI to retrieve the party information */
  private String getPartyUri;

  /** CYN event to be used for party endpoint */
  private String getPartyCynEvent;

  /** The URI to retrieve the party contact points */
  private String getPartyContactPointsUri;

  /** CYN event to be used for party contact points endpoint */
  private String getPartyContactPointsCynEvent;

  /** The URI to retrieve the status of the relationship of the customer with the bank */
  private String getRelationshipUri;

  /** CYN event to be used for relationship endpoint */
  private String getRelationshipCynEvent;

  /** Query parameter of CCDS Party Id */
  private String ccdsPartyIdQueryParam;

  /**
   * Get the canonical map URI for a given identifier type and identifier.
   *
   * @param identifierType type of identifier (for example OLBUSERID)
   * @param identifier the value for the identifier
   * @return URI with the query parameters set
   */
  private String constructCanonicalMapUriFor(final String identifierType, final String identifier) {
    return UriComponentsBuilder.fromHttpUrl(getCanonicalMapUri())
        .queryParam(getCanonicalMapIdentifierTypeQueryParam(), identifierType)
        .queryParam(getCanonicalMapIdentifierQueryParam(), identifier)
        .toUriString();
  }

  /**
   * Get the canonical map URI for a CUI.
   *
   * @param cui customer unique identifier
   * @return URI with the query parameters set
   */
  public String getCanonicalMapUriForCui(final String cui) {
    return constructCanonicalMapUriFor(getCanonicalMapIdentifierTypeCui(), cui);
  }

  /**
   * Get the canonical map URI for a OLB user ID.
   *
   * @param olbUserId OLB user ID
   * @return URI with the query parameters set
   */
  public String getCanonicalMapUriForOlbUserId(final String olbUserId) {
    return constructCanonicalMapUriFor(getCanonicalMapIdentifierTypeOlbuserid(), olbUserId);
  }
}
