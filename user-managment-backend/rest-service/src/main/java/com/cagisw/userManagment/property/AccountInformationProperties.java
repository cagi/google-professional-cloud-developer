package com.cagisw.userManagment.property;

import java.util.Collections;
import java.util.Locale;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import it.cagisw.userManagment.api.CustomerContext;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Properties to operate with account information service. */
@Component
@ConfigurationProperties(prefix = "account.information")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountInformationProperties {

  /** Empty string */
  private static final String EMPTY = "";

  /** Whitespace string */
  private static final String WHITESPACE = " ";

  /** URI to fetch all accounts of customer */
  private String accountsUri;

  /** The placeholder value for the context path variable in the accounts URI */
  private String contextPlaceholder;

  /** Value for placeholder */
  private String contextPersonal;

  /** Value for placeholder */
  private String contextBusiness;

  /** Evaluate URI */
  private String evaluateUri;

  /** Evaluate CYN event */
  private String evaluateCynEvent;

  /** Account details URI */
  private String accountDetailsUri;

  /** Account details CYN event */
  private String accountDetailsCynEvent;

  /** Nominee URI */
  private String nomineeUri;

  /** Nominee CYN event */
  private String nomineeCynEvent;

  /** Account id URI */
  private String accountIdUri;

  /** Account id CYN event */
  private String accountIdCynEvent;

  /**
   * Create URI to retrieve all accounts for a given context, lower case and remove whitespaces.
   *
   * @param context context as string
   * @return the URI with context set in path
   */
  private String constructAccountsUriFor(String context) {
    var safeContext = context.toLowerCase(Locale.ROOT).replace(WHITESPACE, EMPTY);
    return UriComponentsBuilder.fromHttpUrl(getAccountsUri())
        .buildAndExpand(Collections.singletonMap(getContextPlaceholder(), safeContext))
        .toUriString();
  }

  /**
   * Create URI to retrieve all accounts for a given context, the context string is processed to
   * lower case and remove whitespaces.
   *
   * @param context context as string
   * @return the URI with context set in path
   */
  public String getAccountsUriFor(String context) {
    return constructAccountsUriFor(context);
  }

  /**
   * Create URI to retrieve all accounts for a given context.
   *
   * @param context context as string
   * @return the URI with context set in path
   */
  public String getAccountsUriFor(CustomerContext context) {
    return constructAccountsUriFor(context.name());
  }
}
