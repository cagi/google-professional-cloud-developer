package com.cagisw.userManagment.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Properties to perform operations with channel activity history component service. */
@Component
@ConfigurationProperties(prefix = "channel.history")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChannelActivityHistoryProperties {

  /** Record Activity URI */
  private String recordActivityUri;

  /** Record Activity CYN event */
  private String recordActivityCynEvent;

  /** Record event URI */
  private String recordEventUri;

  /** Record event CYN event */
  private String recordEventCynEvent;

  /** Update event URI */
  private String updateEventUri;

  /** Update event CYN event */
  private String updateEventCynEvent;
}
