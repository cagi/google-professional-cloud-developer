Harry finally has his POC and the Back-end API of his million-dollar idea Thrive-a-lyzer. He is pretty excited about it and now he pitches the idea to his rich friend. His rich friend decides to invest in his idea, if Harry can first develop a front-end for the app.

Hard Part

In this Hard challenge, you will build a front-end to interact with some of the functionality developed in the previous challenges. Hard: Publish a HTML page with the following components:

1) A div with id="title" with the content <h1>Thrive-a-lyzer</h1>

2) A div with id="resultsContainer"

3) A text input with label text "Search by date (YYYY-MM-DD): "

3.1) A button input with text "Search by date!"

3.2) When the button is clicked, the resultsContainer should contain for top 3 upvoted results:

3.2a) a <h4> element with text as the article title
        
3.2b) a div with id "upvotes" with the text
	         "Upvotes count:" followed by the number of upvotes
       
3.2c) a div with id "articleId" with the text 
	         "Article ID:  " followed by the article ID
4) A text input with label text: "Search by starting date (YYYY-MM-DD): "

4.1) A text input with label text: "Search by end date (YYYY-MM-DD): "

4.2) A button input with text "Search by range!"

4.3) When the button is clicked, the resultsContainer should contain for top 3 upvoted results:

       4.3a) a <h4> element with text as the article title
          
       4.3b) a div with id "upvotes" with the text 
				         "Upvotes count: " followed by the number of upvotes
          
       4.3c) a div with id "articleId" with the text
				          "Article ID: " followed by the article ID
5) A text input with label text "Search by ID: "

5.1) A button input with text "Search by ID!"

5.2) When the button is clicked, the resultsContainer should contain:

  5.2a) a <h4> element with text as the article title
      
  5.2b) a div with id "upvotes" with the text 
		          "Upvotes count: " followed by the number of upvotes
       
 5.2c) a div with id "downvotes" with the text 
	            "Downvotes count: " followed by the number of downvotes
        
 5.2d) a div with id "contents" with the text contents of the 
	           article (verbatim, do not render any markdown or images)
Notes:

If an article has no up- or downvotes, the Thrive API output might not contain these fields. Be sure to replace by 0 in these cases.
When sorting and two articles have the same upvote-count, solve the tie alphabetically by article title.
The resultsContainer must be empty unless a successful search has been made. If a search returns no results, it must remain empty.




