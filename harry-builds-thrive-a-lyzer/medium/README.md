Harry has received the CLI POC for his Thrive-a-lyzer and is very happy with the results , he now thinks to build the Backend for his million dollar idea Thrive-a-lyzer. He wants to add a few new functionalities to the existing POC that he got from the Easy challenge.

Medium Part

In this Medium challenge, help Harry build a back-end API to support some of the same functionality we created in the Easy challenge, as well as add some new functionality.

Medium: Expose an API on port 8080 with the following endpoints:

1. /get/:id get article by ID

output format:

    {
    id: articleID,
    title: articleTitle,
    tags: [tag1,
    tag2, . . .],
    category: articleTrack
    content: articleContent,
    upvotes: articleUpvotes,
    downvotes: articleDownvotes
    }
2. /get-range/:startDate/:endDate get article IDs by date range

output format:

  {
   response: [id1,
       id2,
       . . .]
   }
		 
3. /get-date/:date get article IDs by date

output format:

 {
  response: [id1,
       id2,
       . . .]
    }
			
4. /get-stats/:id : get article metadata

output format:

  {
  id: articleID,
  title: articleTitle,
  tags: [tag1,
         tag2, . . .],
  category: articleTrack,
  subCategories: [subCategory1,
            subCategory2,. . .],
 upvotes: articleUpvotes,
 downvotes: articleDownvotes,
 recommendedArticles: [recommendedArticleID1,
                  recommendedArticleID2, . . .]

 }
	 
	 
Notes:

Output format should be JSON.
If the article contains multiple entries for its trackCategory, you only need to consider the first entry
For subCategory (fields.contentCategory), you must list the name of each subcategory (fields.name)














