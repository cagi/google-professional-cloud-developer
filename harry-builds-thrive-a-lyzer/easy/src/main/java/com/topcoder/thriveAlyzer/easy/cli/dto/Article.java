package com.topcoder.thriveAlyzer.easy.cli.dto;

import java.io.Serializable;

public class Article implements Serializable {

	private static final long serialVersionUID = 4426290737242200396L;

	private String contentAuthor;
	 
	 private String upvotes;
	 
	 private String trackCategory;
	 
	 private String readTime;
	 
	 private String title;
	 
	 private String type;
	 
	 private String creationDate;
	 
	 private String downvotes;
	 
	 private String content;
	 
	 private String contentCategory;
	 
	 private String tags;
	 
	 private String recommended;
	 
	 private String featuredImage;
	 
	 private String journeyNext;
	 
	 private String Category;
	 
	 private String openExternalLinksInNewTab;
	 
	 private String slug;
	 
	 private String trackParent;
	 
	 private String name;
	 
	 private String Author;
	 
	 private String journeyPrevious;
	 
	 private String tcHandle;
	 
	public String getContentAuthor() {
		return contentAuthor;
	}
	public void setContentAuthor(String contentAuthor) {
		this.contentAuthor = contentAuthor;
	}
	public String getUpvotes() {
		return upvotes;
	}
	public void setUpvotes(String upvotes) {
		this.upvotes = upvotes;
	}
	public String getTrackCategory() {
		return trackCategory;
	}
	public void setTrackCategory(String trackCategory) {
		this.trackCategory = trackCategory;
	}
	public String getReadTime() {
		return readTime;
	}
	public void setReadTime(String readTime) {
		this.readTime = readTime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getDownvotes() {
		return downvotes;
	}
	public void setDownvotes(String downvotes) {
		this.downvotes = downvotes;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getContentCategory() {
		return contentCategory;
	}
	public void setContentCategory(String contentCategory) {
		this.contentCategory = contentCategory;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getRecommended() {
		return recommended;
	}
	public void setRecommended(String recommended) {
		this.recommended = recommended;
	}
	public String getFeaturedImage() {
		return featuredImage;
	}
	public void setFeaturedImage(String featuredImage) {
		this.featuredImage = featuredImage;
	}
	public String getJourneyNext() {
		return journeyNext;
	}
	public void setJourneyNext(String journeyNext) {
		this.journeyNext = journeyNext;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public String getOpenExternalLinksInNewTab() {
		return openExternalLinksInNewTab;
	}
	public void setOpenExternalLinksInNewTab(String openExternalLinksInNewTab) {
		this.openExternalLinksInNewTab = openExternalLinksInNewTab;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getTrackParent() {
		return trackParent;
	}
	public void setTrackParent(String trackParent) {
		this.trackParent = trackParent;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return Author;
	}
	public void setAuthor(String author) {
		Author = author;
	}
	public String getJourneyPrevious() {
		return journeyPrevious;
	}
	public void setJourneyPrevious(String journeyPrevious) {
		this.journeyPrevious = journeyPrevious;
	}
	public String getTcHandle() {
		return tcHandle;
	}
	public void setTcHandle(String tcHandle) {
		this.tcHandle = tcHandle;
	}
}
