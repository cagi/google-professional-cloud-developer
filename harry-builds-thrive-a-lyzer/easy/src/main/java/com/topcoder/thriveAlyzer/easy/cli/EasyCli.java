package com.topcoder.thriveAlyzer.easy.cli;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class EasyCli {

	public static void main(String[] args) {
		EasyCli main = new EasyCli();
		// main.getTotalNumberOfArticles();
		main.getAllArticleIDsPerDate("2022-03-04");
	}

	public void prova() {
		String url = "https://www.topcoder.com/api/cdn/public/contentful/EDU/master/published/entries?content_type=article&limit=5&skip=0&fields.title%5C%5Bmatch%5C%5D&order=-fields.creationDate";
		try {
			String data = getData(url);
			JSONObject obj = new JSONObject(data);
			// System.out.println(data);
			JSONObject jsonObject = obj.getJSONObject("sys");
			System.out.println(obj.get("total"));

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void getTotalNumberOfArticles() {
		try {
			String url = "https://www.topcoder.com/api/cdn/public/contentful/EDU/master/published/entries?content_type=article&limit=5";
			String data = getData(url);
			JSONObject obj = new JSONObject(data);
			System.out.println(obj.get("total"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Outputs the IDs of articles with creationDate matching this day. One per each
	 * line.
	 * 
	 * @param date
	 */
	public void getAllArticleIDsPerDate(String date) {
		try {
			String url = "https://www.topcoder.com/api/cdn/public/contentful/EDU/master/published/entries?content_type=article&fields.creationDate="
					+ date;
			String data = getData(url);
			JSONObject obj = new JSONObject(data);
			Util util = new Util();
			util.getArticles(obj);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	/*
	 * day-level accuracy
	 */
	public void getAllArticleIDsPerDateInterval(String dayStart, String dayEnd) {

	}

	public void getArticleByID(String id) {
		try {
			String url = "https://www.topcoder.com/api/cdn/public/contentful/EDU/master/published/entries?sys.id=" + id;
			String data = getData(url);
			System.out.println(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void getArticleUpvotesByID() {

	}

	private static String getData(String url) throws IOException {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		String result = null;
		try {
			HttpGet request = new HttpGet(url);
			CloseableHttpResponse response = httpClient.execute(request);
			try {
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					// return it as a String
					result = EntityUtils.toString(entity);
				}

			} finally {
				response.close();
			}
		} finally {
			httpClient.close();
		}
		return result;
	}

}
