package com.topcoder.thriveAlyzer.easy.cli;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.topcoder.thriveAlyzer.easy.cli.dto.Article;

public class Util {
	
	private static final String ARRAY_LIST = "public class java.util.ArrayList<E>";
	private static final String INTEGER_VALUE = "public final class java.lang.Integer";
	private static final String STRING_VALUE = "public final class java.lang.String";
	private static final String HASH_MAP = "public class java.util.HashMap<K,V>";
	private static final String BOOLEAN = "public final class java.lang.Boolean";
	

	@SuppressWarnings("unchecked")
	public List<Article> getArticles(JSONObject obj) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		List<String> idList = new ArrayList<String>();
		List<Article> articoli = new ArrayList<Article>();
		
		
		JSONArray items = obj.getJSONArray("items");
		for (int i = 0; i < items.length(); i++) {
			JSONObject jsonObj = (JSONObject) items.get(i);
			JSONObject sys = (JSONObject) jsonObj.get("sys");
			String id = (String) sys.get("id");
			idList.add(id);
		}

		JSONObject jsonObject = obj.getJSONObject("includes");
		ArrayList<HashMap<String, Object>> object = (ArrayList<HashMap<String, Object>>) jsonObject.toMap()
				.get("Entry");
		HashMap<String, Object> sys = null;
		HashMap<String, Object> fields = null;
		String id = null;
		Article article = null;
		Object entryValue = null;
		boolean booleanValue = false;
		String stringValue = null;
		Integer integerValue = null;
		ArrayList arrayListValue = null;
		HashMap hashMapValue = null;
		for (HashMap<String, Object> entry : object) {
			sys = (HashMap<String, Object>) entry.get("sys");
			id = (String) sys.get("id");
			idList.add(id);
			fields = (HashMap<String, Object>) entry.get("fields");
			// Set<String> keySet = fields.keySet();
			article = new Article();
			Keys[] values = Keys.values();
			for (int i = 0; i < values.length; i++) {
				Keys chiave = values[i];
				entryValue = fields.get(chiave.toString());
				
				if (entryValue != null) {
					// System.out.println(entryValue.getClass().toGenericString()); 
					switch (entryValue.getClass().toGenericString()) {
					case ARRAY_LIST:
						arrayListValue = (ArrayList)entryValue;
						// System.out.println(arrayListValue);
						
						break;
					case INTEGER_VALUE:
						integerValue = (Integer)entryValue;
						setValueByReflection(article,integerValue,values[i].toString());
						break;
					case STRING_VALUE:
						stringValue = (String) entryValue;
						System.out.println(stringValue);
						setValueByReflection(article,stringValue,values[i].toString());
						break;
					case HASH_MAP:
						hashMapValue = (HashMap)entryValue;
						
						break;	
					case BOOLEAN:
						booleanValue = (Boolean) entryValue;
						setValueByReflection(article,booleanValue,values[i].toString());
						break;
					default:
						break;
					}
				}
			}
			articoli.add(article);

			/*
			 * article = new Article(); article.setContentAuthor(id);
			 * article.setUpvotes(id); article.setTrackCategory(id);
			 * article.setReadTime(id); article.setTitle(id); article.setType(id);
			 * article.setCreationDate(id); article.setDownvotes(id);
			 * article.setContent(id); article.setContentCategory(id); article.setTags(id);
			 * article.setRecommended(id); article.setFeaturedImage(id);
			 * article.setJourneyNext(id); article.setCategory(id);
			 * article.setOpenExternalLinksInNewTab(id); article.setSlug(id);
			 * article.setTrackParent(id); article.setName(id); article.setAuthor(id);
			 * article.setJourneyPrevious(id); article.setTcHandle(id);
			 */
		}
		return articoli;
	}

	public static void setValueByReflection(Article article, Object value, String property) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method[] declaredMethods = article.getClass().getDeclaredMethods();
		Method declaredMethod = null;
		property = property.substring(0, 1).toUpperCase() + property.substring(1);
		String suffix = "get" + property + "()";
		for (int i = 0; i < declaredMethods.length; i++) {
			if (declaredMethods[i].toString().endsWith(suffix)) {
				if (value instanceof String) {
					declaredMethod = article.getClass().getDeclaredMethod("set" + property,String.class);
				}
				if (value instanceof Boolean) {
					declaredMethod = article.getClass().getDeclaredMethod("set" + property,String.class);
				}
				if (value instanceof Integer) {
					declaredMethod = article.getClass().getDeclaredMethod("set" + property,String.class);
				}
				// System.out.println(property);
				declaredMethod.invoke(article, value + "");
				break;
			}
		}
	}
	
	public static void main(String[] args) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		setValueByReflection(new Article(), "simpleString", "name");
	}

	public List<String> getValueByIncludes(JSONObject obj, String attribute) {
		ArrayList list = null;
		List<String> ids = new ArrayList<String>();
		JSONObject includes = obj.getJSONObject("includes");

		Map<String, Object> map = includes.toMap();
		Map<String, Object> innerMap = null;
		Map<String, Object> valueMap = null;
		for (String key : map.keySet()) {
			list = (ArrayList) map.get(key);
			for (Object entry : list) {
				innerMap = (Map<String, Object>) entry;
				valueMap = (Map<String, Object>) innerMap.get("sys");
				ids.add((String) valueMap.get(attribute));
			}
		}
		return ids;
	}

	public List<String> getValueByItems(JSONObject obj, String attribute) {
		List<String> ids = new ArrayList<String>();
		JSONArray jsonArray = obj.getJSONArray("items");
		List<Object> list = jsonArray.toList();
		Map<String, Object> innerMap = null;
		Map<String, Object> sysMap = null;
		for (Object entry : list) {
			innerMap = (Map<String, Object>) entry;
			sysMap = (Map<String, Object>) innerMap.get("sys");
			ids.add((String) sysMap.get(attribute));
			sysMap = (Map<String, Object>) innerMap.get("fields");
			// ids.add((String) sysMap.get("contentAuthor"));
		}
		return ids;
	}

}
