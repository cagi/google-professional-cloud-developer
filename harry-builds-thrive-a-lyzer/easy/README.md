Problem Detail

Harry is an ambitious Topcoder member and he comes across Thrive and he is thrilled with the amount of learning content and articles in there and the entire idea and he decides to build something like Thrive from scratch.

To explore his idea he needs to begin writing a console application to interface with the Thrive API.

Easy Part:

In this Easy challenge, help Harry build a command-line POC (proof-of-concept) app for interfacing with the Topcoder Thrive library of educational material created by the community.

The Topcoder Thrive API is a proxy for the Contentful API. Familiarize yourself with the Contentful API for Thrive at 
https://www.topcoder.com/api/cdn/public/contentful/EDU/master/published/entries?content_type=article&limit=5&skip=0&fields.title%5C%5Bmatch%5C%5D&order=-fields.creationDate to see the format of the JSON data.

You can check https://www.contentful.com/developers/docs/references/content-delivery-api/ for more information on using the API.

In this challenge you are tasked with creating a console application with the following functionality. All output should be sent to console standard output.

Supported queries:

get total number of articles
get all article IDs per date
get all article IDs per date interval (day-level accuracy)
get article by ID
get article upvotes by ID


Follow the example in the sample submission for your language of choice. The command-line usage of the app should be as follows:

1. ./solution get-all

Outputs a single number

2. ./solution get-date "2022-03-04"

Outputs the IDs of articles with creationDate matching this day. One per each line.

3. ./solution get-interval "2022-03-02 2022-03-04"

Outputs the IDs of articles with creationDate matching this interval. One per each line.

4. ./solution get-id 6a7YQp5mDEF6tFxP2h2bnJ

Outputs the content field (fields.content) for the article matching this ID.

5. ./solution get-upvotes 6a7YQp5mDEF6tFxP2h2bnJ

Outputs a single number, the number of upvotes for the article matching this ID

NOTE: If the article has not either up- or downvotes, the Contentful API output might not contain these fields. Be sure to substitute 0 in this case.

Dates are expected to be in "%Y-%m-%d" format (Year-Month-Day).
Consider only the fields.creationDate field as the relevant timestamp here.


Test case input can be assumed to be valid, i.e. error handling is not considered for the scoring of the submissions.




























